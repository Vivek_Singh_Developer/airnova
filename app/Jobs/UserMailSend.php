<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use SebastianBergmann\ObjectEnumerator\Exception;

class UserMailSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $details;
    public $timeout = 7200; // 2 hours
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userlist = [];
        if($this->details['send_user']=='Web')
        {
            $user = Customer::select('email')->get();
            foreach($user as $item)
            {
                array_push($userlist,$item->email);
            }
        }
        if($this->details['send_user']=='Mobile')
        {
            $user = User::select('email')->get();
            foreach($user as $item)
            {
                array_push($userlist,$item->email);
            }
        }
    
        if($this->details['send_user'] =='Both')
        {
            $userlist1 = Customer::select('email')->get();
            $userlist2 = User::select('email')->get();
           
            foreach($userlist1 as $item)
            {
                array_push($userlist,$item->email);
            }
            foreach($userlist2 as $item)
            {
                array_push($userlist,$item->email);
            }

        }

        if(count($userlist)>0)
        {
            $newuser = array_unique($userlist);
            $data = [
                'subject' =>$this->details['subject'],
                'message' =>$this->details['message'],
            ];

            foreach($newuser as $items)
            {
                try
                {
                    Mail::to($items)->send(new \App\Mail\NewsLetter($data));
                }
                catch(Exception $e)
                {

                }
            }
        }
    
    }
}
