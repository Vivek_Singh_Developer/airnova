<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserOTP;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }
    protected function guard()
    {
        return Auth::guard('customer');
    }

    public function login(Request $request)
    {
        $user=User::where('email',$request->user)
        ->orWhere('mobile',$request->user)->first();
        $errors=[];
        if($user==null)
        {
            $errors['user'] = ['User does not exists.'];
            throw ValidationException::withMessages($errors);
        } 
        elseif($user!=null && $user->block==0)
        {
            $errors['user'] = ['Invalid User.'];
            throw ValidationException::withMessages($errors);
        }  
        else
        {
            if($request->logintype == 'password')
            {
                $password = $request->password;
                if($password==$user->send_password)
                {
                    $this->guard()->login($user);
                    return $request->wantsJson()
                            ? new JsonResponse([], 201)
                            : redirect($this->redirectPath());
                }
                else
                {
                    $errors['password'] = ['The password does not match.'];
                    throw ValidationException::withMessages($errors);
                }
            }
            elseif($request->logintype == 'otp')
            {
                if($request->otp==null)
                {
                    return redirect()->back()->with('failed','Please sent otp first.');
                }
                $userotp = UserOTP::where('mobile',$request->user)->first();
                if($userotp!=null && $userotp->otp == $request->otp)
                {
                    $userotp->delete();
                    $this->guard()->login($user);
                    return $request->wantsJson()
                            ? new JsonResponse([], 201)
                            : redirect($this->redirectPath());
                }
                else
                {
                    return redirect()->back()->with('failed','Login Failed');
                }
                
            }
        }
    }

    public function logout(Request $request)
    {
        $this->guard('customer')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

        // if ($response = $this->loggedOut($request)) {
        //     return $response;
        // }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect(route('login'));
    }

    protected function loggedOut(Request $request)
    {
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect(route('login'));
    }
}
