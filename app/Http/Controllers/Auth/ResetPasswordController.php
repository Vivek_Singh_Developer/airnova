<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserOTP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request)
    {
        $Validator = Validator::make($request->all(), [
            'password' => ['required', 'min:6'],
            'otp' => ['required', 'digits:4'],
        ]); 
        if($Validator->fails())
        {
            return redirect()->back()->withErrors($Validator->errors())->withInput();
        }
        $user = User::where('mobile',$request->mobile)->first();
        $user->password = Hash::make($request->password);
        $user->send_password = $request->password;
        $user->save();
        UserOTP::where('mobile',$request->mobile)->delete();
        return redirect()->route('login')->with('success','Your Password Reset Successfully!');
    }
}