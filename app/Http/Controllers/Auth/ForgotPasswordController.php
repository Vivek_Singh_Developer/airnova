<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserOTP;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    public function sendResetLinkEmail(Request $request)
    {
        $Validator = Validator::make($request->all(), [
            'mobile' => ['required', 'between:9,11', 'exists:users'],
        ]); 
        if($Validator->fails())
        {
            return redirect()->back()->withErrors($Validator->errors())->withInput();
        }
        
        if(sendOtp($request->mobile)==true)
        {
            $userotp = UserOTP::where('mobile',$request->mobile)->first();
            $data['mobile'] = $request->mobile;
            return view('auth.passwords.reset',$data);
        }
        else
        {
            return redirect()->back()->with('failed','OTP Not Sent');
        }
    }
}
