<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserOTP;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer');
    }
    protected function guard()
    {
        return Auth::guard('customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
   
    public function register(Request $request)
    {
        $Validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'otp' => ['required', 'digits:4'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'mobile' => ['required', 'between:9,11', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]); 
        if($Validator->fails())
        {
            return redirect()->back()->withErrors($Validator->errors())->withInput();
        }

        $data = $request->all();
        $userOtp = UserOTP::where('mobile', $data['mobile'])->first();
        if($userOtp->mobile_verified !=null)
        {
            $str =  Str::random(4);
            $user = User::create([
                'my_refer_code' => $str,
                'name' => Str::words($data['name']),
                'email' => $data['email'],
                'mobile' => $data['mobile'],
                'user_type' => 'User',
                'password' => Hash::make($data['password']),
                'send_password'=>$data['password'],
            ]);
            $myvalue = $data['name'];
            $arr = explode(' ',trim($myvalue));
            $user->my_refer_code = Str::upper($arr[0]).$user->id;
            $user->save();

            UserOTP::where('mobile', $data['mobile'])->delete();
            $this->guard()->login($user);
            return $request->wantsJson()
                        ? new JsonResponse([], 201)
                        : redirect($this->redirectPath());
        }
        else
        {
            return redirect()->back()->with('failed','Registration Failed! Please try again.');
        }
    }
    
}
