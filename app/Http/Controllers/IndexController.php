<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use Razorpay\Api\Api;
use GuzzleHttp\Client;
use App\Models\Content;
use App\Models\Product;
use App\Models\Service;
use App\Models\UserOTP;
use App\Models\FeedBack;
use App\Models\Newsletter;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class IndexController extends Controller
{

    public function serviceById($id)
    {
        $data["servicedata"] = Service::find($id);
        $data["allservice"] = Service::where('id','!=',$id)->get();
        return view('frontend.service-detail', $data);
    }

    public function productByCategory($cat_id)
    {
        $data["catdata"] = ProductCategory::find($cat_id);
        $data["productdata"] = Product::where('category_id',$cat_id)->paginate(12);
        return view('frontend.category-detail', $data);
    }
    public function productDetail($product_id)
    {
        $data["product"] = Product::find($product_id);
        $data["productdata"] = Product::where('id',"!=",$product_id)->inrandomorder()->take(3)->get();
        return view('frontend.product-detail', $data);
    }
    
    public function contactUs()
    {
        return view('frontend.contact-us');
    }

    public function career()
    {
        return view('frontend.career');
    }


    public function index()
    {
        return view('index');
    }
  
    public function feedback(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'       => 'required',
            'email'      => 'sometimes|nullable|email',
            'mobile'      => 'sometimes|nullable',
            'subject'    => 'required',
            'message'    => 'required',
        ],[
           'name.required' => 'The name field is required.',
           'email.sometimes' => 'The email field is sometimes required.',
           'mobile.sometimes' => 'The mobile field is sometimes required.',
           'email.email' => 'The email must be a valid email address.',
           'subject.required' => 'The subject field is required.',
           'message.required' => 'The message field is required.',
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        if($request->email==null && $request->mobile==null)
        {
            throw ValidationException::withMessages([
                'email' => 'The email field is sometimes required.',
                'mobile' => 'The mobile field is sometimes required.',
            ]);
        }
        $feedback = new FeedBack();
        $feedback->name  = $request->name;
        $feedback->email  = $request->email?$request->email:null;
        $feedback->mobile  = $request->mobile?$request->mobile:null;
        $feedback->subject  = $request->subject;
        $feedback->message  = $request->message;
        $feedback->save();
        return redirect()->back()->with('success','Thank You '.$request->name.'. We will contact you soon!');

    }

    public function newsletter(Request $request)
    {
        $validator=Validator::make($request->all(),[ 
            'nemail'      => 'required|email',
        ],[
           'nemail.required' => 'The email field is required.',
           'nemail.email' => 'The email must be a valid email address.',
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $Newsletter = new Newsletter();
        $Newsletter->email  = $request->nemail;
        $Newsletter->save();
        return redirect()->back()->with('success','Thank You . We will contact you soon!');

    }

    public function aboutUs()
    {
        return view('frontend.about-us');
    }

    public function privacypolicy()
    {
        return view('frontend.privacypolicy');
    }

    public function termcondition()
    {
        $data = [];
        return view('frontend.termcondition',$data);
    }

    public function services()
    {
        return view('frontend.services');
    }

    public function who_we_are()
    {
        return view('frontend.who_we_are');
    }

    public function our_values()
    {
        return view('frontend.our_values');
    }

    public function our_history()
    {
        return view('frontend.our_history');
    }

    public function board_of_director()
    {
        return view('frontend.board_of_director');
    }

    public function management_team()
    {
        return view('frontend.management_team');
    }

}
