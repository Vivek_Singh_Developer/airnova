<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $data['name']=Setting::where('key','name')->first();
        $data['logo']=Setting::where('key','logo')->first();
        $data['copyright']=Setting::where('key','copyright')->first();
        $data['admin_data']=Admin::where('id',auth()->user()->id)->first();
        $data['app_address']=Setting::where('key','app_address')->first();
        $data['app_email']=Setting::where('key','app_email')->first();
        $data['app_mobile']=Setting::where('key','app_mobile')->first();
        $data['notifications']=0;

        return view('admin.setting.setting_list',$data);
    }

    public function updateappname(Request $request)
    {
            $validator = Validator::make($request->all(),[
                'id'=>'required|exists:settings',
                'key' => 'required|string|max:255',
                'appname' => 'required',
            ]);
            if($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors());
            }          
            
           Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->appname]);

            Session::flash('success', 'Setting Updated');
            return redirect()->route('admin.setting');
    }
    public function updatecopyright(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'key' => 'required|string|max:255',
            'copyright' => 'required',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }          
        
        Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->copyright]);
        Session::flash('success', 'Setting Update');
        return redirect()->route('admin.setting');
    }
    public function updatelogo(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'key' => 'required|string|max:255',
            'logo'=>'required|mimes:jpg,png,jpeg|max:8000',
        ]);
        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }  
        
        $editlogo= Setting::where('id',$request->id)->first();
        if($request->has('logo')) 
        {
            //Image delete
            $backupLoc='public/Logo/';
            if(!is_dir($backupLoc)) {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }

            $filePath = $editlogo->value;
            if($filePath != null)
            { 
               $filePath1 = storage_path('app/public/'. $filePath);
               if(is_file($filePath1))
               {
                  unlink($filePath1);
               } 
            } 
            $logofile = $request->file('logo');
            $logo_image = time().'_'.$logofile->getClientOriginalName();
            $upload_success1 = $request->file('logo')->storeAs('public/Logo',$logo_image);    
            $uploaded_logo_image = 'Logo/'.$logo_image; 
            $editlogo->value =  $uploaded_logo_image;
           
        }
        $editlogo->update();        
        
       // Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->content]);
        Session::flash('success', 'Setting Updated');
        return redirect()->route('admin.setting');
    }
    public function updateadminemail(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'email' => 'required|email',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }          
        Admin::where('id',$request->id)->update(['email' =>$request->email]);
        Session::flash('success', 'Setting Updated');
        return redirect()->route('admin.setting');
    }
    public function updateadminpassword(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'old_password' => 'required|min:6|max:16',
            'password' => 'required|min:6|max:16',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        } 
        $errors=[];
        $admin = Admin::where('id',$request->id)->first();         
        if(!Hash::check($request->old_password ,$admin->password) )
        {
            $errors['old_password'] = ['The Old Password does not match.'];
            throw ValidationException::withMessages($errors);
            return redirect()->back()->withErrors($errors);
        }
        else
        {
            Admin::where('id',$request->id)->update(['password' =>Hash::make($request->password)]);
            Session::flash('success', 'Setting Updated');
        }
        return redirect()->route('admin.setting');
    }
    public function updatecompanyemail(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'key' => 'required|string|max:255',
            'app_email' => 'required|email',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }          
        
        Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->app_email]);
        Session::flash('success', 'Setting Updated');
        return redirect()->route('admin.setting');
    }
    public function updatecompanyaddress(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'key' => 'required|string|max:255',
            'app_address' => 'required',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }          
        
        Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->app_address]);
        Session::flash('success', 'Setting Updated');
        return redirect()->route('admin.setting');
    }
    public function updatecompanymobile(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id'=>'required|exists:settings',
            'key' => 'required|string|max:255',
            'app_mobile' => 'required|min:7|numeric',
        ]);

        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }          
        
        Setting::where('id',$request->id)->update(['key' =>$request->key, 'value' =>$request->app_mobile]);
        Session::flash('success', 'Setting Updated');
        return redirect()->route('admin.setting');
    }

}
