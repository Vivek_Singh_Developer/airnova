<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(route('admin.procategory.destroy')+'/1');
        $data['productscategory']=ProductCategory::orderBy('id','desc')->get();
        return view('admin.products.category.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'cat_name'       => 'required|string',
            'cat_code'       => 'required|string|unique:product_categories,cat_code',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  

        $addproduct = new ProductCategory;
        $addproduct->cat_name  = $request->cat_name;
        $addproduct->cat_code  = $request->cat_code;
        $addproduct->save();

        return redirect()->route('admin.procategory.index')->with('success','Product Category Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['productscategory']=ProductCategory::get();
        $data['singlecategory']=ProductCategory::where('id',$id)->first();
        return view('admin.products.category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'cat_name'       => 'required|string',
            'cat_code'       => 'required|string|unique:product_categories,cat_code,'.$id.',id'
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  

        $editproduct = ProductCategory::where('id',$id)->first();
        $editproduct->cat_name  = $request->cat_name;
        $editproduct->cat_code  = $request->cat_code;
        $editproduct->save();

        return redirect()->route('admin.procategory.index')->with('success','Product Category Updated!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catpro=ProductCategory::find($id);
        if($catpro->delete())
        {
            Session::flash('success','Product Category Deleted!');
            return response()->json(['status' => '1']);
        }
        else
        {
            Session::flash('failed','Product Category Not Deleted!');
            return response()->json(['status' => '0']);
        }
    }
}
