<?php

namespace App\Http\Controllers\Admin;

use App\Models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function index()
    {
        $data['contents']=Content::orderBy('id','desc')->where('type','Website')->get();
        return view('admin.content.index',$data);
    }

    public function edit($id)
    {
        $data['contents']=Content::where('id',$id)->first();
        return view('admin.content.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'key'   => 'required',
            'Value'   => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
        $content = Content::where('id',$id)->first();
        $content->key  = $request->key;
        $content->Value  = $request->Value;
        if($request->has('image')) 
        {
            $backupLoc='public/content/';
            if(!is_dir($backupLoc)) 
            {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }

            if( $content->image != null)
            { 
                $filePath1 = storage_path('app/public/content/'.$content->image);
                if(is_file($filePath1))
                {
                    unlink($filePath1);
                } 
            } 

            $file = $request->file('image');
            $image = time().'_'.$file->getClientOriginalName();
            $upload_success1 = $request->file('image')->storeAs('public/content',$image); 
            $content->image =  $image; 
        }

        if($request->has('video')) 
        {
            $backupLoc='public/content/';
            if(!is_dir($backupLoc)) 
            {
                Storage::makeDirectory($backupLoc, 0755, true, true);
            }

            if( $content->video != null)
            { 
                $filePath2 = storage_path('app/public/content/'.$content->video);
                if(is_file($filePath2))
                {
                    unlink($filePath2);
                } 
            } 

            $file1 = $request->file('video');
            $video = time().'_'.$file1->getClientOriginalName();
            $upload_success2 = $request->file('video')->storeAs('public/content',$video); 
            $content->video =  $video; 
        }
        $content->save();
        if($content->type=='Mobile')
        {
            return redirect()->route('admin.content.mobile.index')->with('success','Content Updated!');
        }
        else
        {
            return redirect()->route('admin.content.index')->with('success','Content Updated!');
        }
    }

    public function deleteImage($id)
    {
        $Content=Content::find($id);
        if( $Content->image != null)
        { 
            $filePath1 = storage_path('app/public/content/'.$Content->image);
            if(is_file($filePath1))
            {
                unlink($filePath1);
                $Content->image = null;
                $Content->save();
            } 
        } 
        return redirect()->back()->with('success','Image Removed Successfully!');
    }
    public function deletevideo($id)
    {
        $Content=Content::find($id);
        if( $Content->video != null)
        { 
            $filePath1 = storage_path('app/public/content/'.$Content->video);
            if(is_file($filePath1))
            {
                unlink($filePath1);
                $Content->video = null;
                $Content->save();
            } 
        } 
        return redirect()->back()->with('success','Video Removed Successfully!');
    }
}
