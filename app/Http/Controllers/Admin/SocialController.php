<?php

namespace App\Http\Controllers\Admin;

use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['socials'] = Social::orderBy('id','desc')->get();
        return view('admin.social.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.social.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'icon_name'    => 'required',
            'link'        => 'required',
            'fa_class'         => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
        $Social = new Social();
        $Social->icon_name  = $request->icon_name;
        $Social->link  = $request->link;
        $Social->fa_class  = $request->fa_class;
        $Social->save();
        return redirect()->route('admin.socials.index')->with('success','Social Added!');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['social'] = Social::where('id',$id)->first();
        return view('admin.social.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'icon_name'    => 'required',
            'link'        => 'required',
            'fa_class'         => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
        $Social = Social::where('id',$id)->first();
        $Social->icon_name  = $request->icon_name;
        $Social->link  = $request->link;
        $Social->fa_class  = $request->fa_class;
        $Social->save();
        return redirect()->route('admin.socials.index')->with('success','Social Updated!');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Social=Social::find($id);
        if($Social->delete())
        {
            Session::flash('success','Social Deleted!');
            return response()->json(['status' => '1']);
        }
        else
        {
            Session::flash('failed','Social  Not Deleted!');
            return response()->json(['status' => '0']);
        }
    }
}
