<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\FeedBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FeedBackController extends Controller
{
    public function index()
    {
        $data['feedback']=FeedBack::orderBy('id','desc')->get();
        return view('admin.feedback.index',$data);
    }
    public function destroy($id)
    {
        $feedback = FeedBack::where('id',$id)->first();

        if($feedback->delete())
        {
            return response()->json(['status' => '1','massage'=>'FeedBack Status Deleted!']);
        }
        else
        {
            return response()->json(['status' => '0','massage'=>'FeedBack Status Not Deleted!']);
        }

        // return redirect()->route('admin.feedback.index')->with('success','Feedback Deleted!');
    }

    public function feedbackshow($id)
    {
        $data['feedback']=FeedBack::find($id);
        return view('admin.feedback.show',$data);
    }

    public function feedbackreply(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'reply'   => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $addevent = FeedBack::where('id',$request->id)->first();
        $addevent->reply  = $request->reply;
        $addevent->save();
        if($addevent->email!=null)
        {
            try
            {
                $data = [
                    'subject' =>'',
                    'message' =>$request->reply,
                ];
                Mail::to($addevent->email)->send(new \App\Mail\Feedback($data));
            }
            catch(Exception $e)
            {

            }
        }
        return redirect()->route('admin.feedback.index')->with('success','Feedback Sent!');
    }

}
