<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\ProductImages;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['products']=Service::orderBy('id','desc')->get();
        return view('admin.service.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'service_name'   => 'required',
            'image'          => 'required',
            'images'  => 'required|array',
            'description'  => 'required',
            'sub_heading'  => 'required',
            'sub_heading_desc'  => 'required',
        ]);
        if($validator->fails())
        {
           
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
        $addproduct = new Service();
        $addproduct->service_name  = $request->service_name;
        $addproduct->description  = $request->description;
        $addproduct->sub_heading  = $request->sub_heading;
        $addproduct->sub_heading_desc  = $request->sub_heading_desc;
      
        $backupLoc='public/service/';
        if(!is_dir($backupLoc)) 
        {
            Storage::makeDirectory($backupLoc, 0755, true, true);
        }

        if($request->has('image')) 
        {
            $file = $request->file('image');
            $image = time().'_'.$file->getClientOriginalName();
            $upload_success1 = $request->file('image')->storeAs('public/service',$image); 
            $addproduct->thumbnail_image =  $image; 
            
        }
      

        if(count($request->images)>0) 
        {
            if( $request->has('images')) 
            {
                if(count($request->images)>0)
                {
                    $abc=[];
                    foreach($request->images as $file)
                    {
                        $image = time().'_'.$file->getClientOriginalName();
                        $upload_success1 = $file->storeAs('public/service',$image);
                        array_push( $abc,$image);
                    }
                    $addproduct->images  = json_encode($abc);

                }
            }
        }

        $addproduct->save();
        return redirect()->route('admin.services.index')->with('success','Service Added!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['products']=Service::where('id',$id)->first();

        // dd( $data['products']->getImages);
        return view('admin.service.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $validator=Validator::make($request->all(),[
            'service_name'   => 'required',
            'description'  => 'required',
            'sub_heading'   => 'required',
            'sub_heading_desc'  => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
         
        $addproduct = Service::where('id',$id)->first();
        $addproduct->service_name  = $request->service_name;
        $addproduct->description  = $request->description;
        $addproduct->sub_heading  = $request->sub_heading;
        $addproduct->sub_heading_desc  = $request->sub_heading_desc;
        $backupLoc='public/service/';
        if(!is_dir($backupLoc)) 
        {
            Storage::makeDirectory($backupLoc, 0755, true, true);
        }
  
        if($request->has('image')) 
        {          
            if( $addproduct->thumbnail_image != null)
            { 
                $filePath1 = storage_path('app/public/service/'.$addproduct->thumbnail_image);
                if(is_file($filePath1))
                {
                    unlink($filePath1);
                } 
            } 

            $file = $request->file('image');
            $image = time().'_'.$file->getClientOriginalName();
            $upload_success1 = $request->file('image')->storeAs('public/service',$image); 
            $addproduct->thumbnail_image =  $image; 
            
        }


            if( $request->has('images')) 
            {
                if(count($request->images)>0)
                {
                    $abc=[];
                    foreach($request->images as $file)
                    {
                        $image = time().'_'.$file->getClientOriginalName();
                        $upload_success1 = $file->storeAs('public/service',$image);
                        array_push( $abc,$image);
                    }
                    $addproduct->images  = json_encode($abc);

                }
            }

        
        $addproduct->save();
      
      

        return redirect()->route('admin.services.index')->with('success','Service Updated!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Service::find($id);
        if( $product->thumbnail_image != null)
        { 
            $filePath1 = storage_path('app/public/service/'.$product->thumbnail_image);
            if(is_file($filePath1))
            {
                unlink($filePath1);
            } 
        } 
        $productimage = ProductImages::where('product_id',$id)->get();
        if(count($productimage)>0)
        {
            foreach($productimage as $file)
            {
                $product_image = storage_path('app/public/service/'.$file->product_image);
                if(is_file($product_image))
                {
                    unlink($product_image);
                } 
            }
        }
        if($product->delete())
        {
            Session::flash('success','Service Deleted!');
            return response()->json(['status' => '1']);
        }
        else
        {
            Session::flash('failed','Service  Not Deleted!');
            return response()->json(['status' => '0']);
        }
    }
    
    public function updatestatus(Request $request)
    {
        $Product = Service::find($request->id);
        $Product->status = $request->status;
        if($Product->save())
        {
            return response()->json(['status' => '1','massage'=>'Service Status Updated!']);
        }
        else
        {
            return response()->json(['status' => '0','massage'=>'Service Status Not Updated!']);
        }
    }
    
}
