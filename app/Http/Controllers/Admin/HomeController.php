<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $data['users']=User::count();
        $data['plans']=0;
        $data['subscription']=0;
        $data['content']=0;
        $data['enquiry']=0;
        $data['setting']=0;
        $data['promoter']=0;
        $data['notifications']=0;
        $data['notify']=[];
        return view('admin.home',$data);
    }
    public function newsletter()
    {
        $data['newsletters'] = Newsletter::get();
        return view('admin.newsletter.index',$data);
    }
    public function newsletterstore(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'subject'    => 'required',
            'message'    => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }   
        $userlist = [];
        if($request->send_user=='Web')
        {
            $user = User::select('email')->get();
            foreach($user as $item)
            {
                array_push($userlist,$item->email);
            }
        }
        if($request->send_user=='Mobile')
        {
            $user = User::select('email')->get();
            foreach($user as $item)
            {
                array_push($userlist,$item->email);
            }
        }
        if($request->send_user=='Both')
        {
            $userlist1 = User::select('email')->get();
            $userlist2 = User::select('email')->get();
           
            foreach($userlist1 as $item)
            {
                array_push($userlist,$item->email);
            }
            foreach($userlist2 as $item)
            {
                array_push($userlist,$item->email);
            }

        }

        if(count($userlist)>0)
        {
            $newuser = array_unique($userlist);
            $data = [
                'subject' =>$request->subject,
                'message' =>$request->message,
            ];

            foreach($newuser as $items)
            {
                try
                {
                    Mail::to($items)->send(new \App\Mail\NewsLetter($data));
                }
                catch(Exception $e)
                {

                }
            }
        }
        

        //  $details = [
    	 // 	'subject' => $request->subject,
          //    'message' => $request->message,
          //    'send_user' => $request->send_user
    	 // ];

    	// send all mail in the queue.
        // $job = (new \App\Jobs\UserMailSend($details))
        //     ->delay(
        //     	now()
        //     	->addSeconds(2)
        //     ); 

         // dispatch((new \App\Jobs\UserMailSend($details)));
        return redirect()->back();
       //   return redirect()->back()->with('success','Bulk mail send successfully in the background...');
    }
}
