<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use App\Models\ProductImages;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['products']=Product::orderBy('id','desc')->get();
        return view('admin.products.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['productscategory']=ProductCategory::get();
        return view('admin.products.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'category_id'    => 'required|not_in:0',
            'product_code'   => 'required|unique:products,product_code',
            'product_name'   => 'required',
            'image'          => 'required',
            // 'product_image'  => 'required|array',
            'product_price'  => 'required',
            'product_description'  => 'required',
        ]);
        if($validator->fails())
        {
           
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
        $addproduct = new Product();
        $addproduct->category_id  = $request->category_id;
        $addproduct->product_code  = $request->product_code;
        $addproduct->product_name  = $request->product_name;
        $addproduct->product_price  = $request->product_price;
        $addproduct->product_description  = $request->product_description;
        $backupLoc='public/product/';
        if(!is_dir($backupLoc)) 
        {
            Storage::makeDirectory($backupLoc, 0755, true, true);
        }

        if($request->has('image')) 
        {
            $file = $request->file('image');
            $image = time().'_'.$file->getClientOriginalName();
            $upload_success1 = $request->file('image')->storeAs('public/product',$image); 
            $addproduct->thumbnail_image =  $image; 
            
        }
        $addproduct->save();

   
        // if(count($request->product_image)>0) 
        // {
        //     if( $request->has('product_image')) 
        //     {
        //         if(count($request->product_image)>0)
        //         {
        //             foreach($request->product_image as $file)
        //             {
        //                 $image = time().'_'.$file->getClientOriginalName();
        //                 $upload_success1 = $file->storeAs('public/product',$image);
        //                 $productimages = new ProductImages(); 
        //                 $productimages->product_id = $addproduct->id;
        //                 $productimages->product_image = $image;
        //                 $productimages->save();
        //             }
        //         }
        //     }
        // }
      
        return redirect()->route('admin.products.index')->with('success','Product Added!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['productscategory']=ProductCategory::get();
        $data['products']=Product::where('id',$id)->first();

        // dd( $data['products']->getImages);
        return view('admin.products.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $validator=Validator::make($request->all(),[
            'category_id'    => 'required|not_in:0',
            'product_code'   => "required|unique:products,product_code,".$id,
            'product_name'   => 'required',
            'product_price'  => 'required',
            'product_description'  => 'required',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }  
         
        $addproduct = Product::where('id',$id)->first();
        $addproduct->category_id  = $request->category_id;
        $addproduct->product_code  = $request->product_code;
        $addproduct->product_name  = $request->product_name;
        $addproduct->product_price  = $request->product_price;
        $addproduct->product_description  = $request->product_description;
        $backupLoc='public/product/';
        if(!is_dir($backupLoc)) 
        {
            Storage::makeDirectory($backupLoc, 0755, true, true);
        }
  
        if($request->has('image')) 
        {          
            if( $addproduct->thumbnail_image != null)
            { 
                $filePath1 = storage_path('app/public/product/'.$addproduct->thumbnail_image);
                if(is_file($filePath1))
                {
                    unlink($filePath1);
                } 
            } 

            $file = $request->file('image');
            $image = time().'_'.$file->getClientOriginalName();
            $upload_success1 = $request->file('image')->storeAs('public/product',$image); 
            $addproduct->thumbnail_image =  $image; 
            
        }
        $addproduct->save();
        // if($request->has('product_image')) 
        // {
        //     if(count($request->product_image)>0)
        //     {
        //         foreach($request->product_image as $file)
        //         {
        //             $image = time().'_'.$file->getClientOriginalName();
        //             $upload_success1 = $file->storeAs('public/product',$image);
        //             $productimages = new ProductImages(); 
        //             $productimages->product_id = $addproduct->id;
        //             $productimages->product_image = $image;
        //             $productimages->save();
        //         }
        //     }
        // }
      

        return redirect()->route('admin.products.index')->with('success','Product Updated!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        if( $product->thumbnail_image != null)
        { 
            $filePath1 = storage_path('app/public/product/'.$product->thumbnail_image);
            if(is_file($filePath1))
            {
                unlink($filePath1);
            } 
        } 
        $productimage = ProductImages::where('product_id',$id)->get();
        if(count($productimage)>0)
        {
            foreach($productimage as $file)
            {
                $product_image = storage_path('app/public/product/'.$file->product_image);
                if(is_file($product_image))
                {
                    unlink($product_image);
                } 
            }
        }
        if($product->delete())
        {
            Session::flash('success','Product Deleted!');
            return response()->json(['status' => '1']);
        }
        else
        {
            Session::flash('failed','Product  Not Deleted!');
            return response()->json(['status' => '0']);
        }
    }
    public function produtimagedelete($id)
    {
        $product=ProductImages::find($id);
        if( $product->product_image != null)
        { 
            $filePath1 = storage_path('app/public/product/'.$product->product_image);
            if(is_file($filePath1))
            {
                unlink($filePath1);
            } 
        } 
        if($product->delete())
        {
            Session::flash('success','Product Image Deleted!');
            return redirect()->back();
        }
        else
        {
            Session::flash('failed','Product Image Not Deleted!');
            return redirect()->back();
        }
    }
    public function updatestatus(Request $request)
    {
        $Product = Product::find($request->id);
        $Product->status = $request->status;
        if($Product->save())
        {
            return response()->json(['status' => '1','massage'=>'Product Status Updated!']);
        }
        else
        {
            return response()->json(['status' => '0','massage'=>'Product Status Not Updated!']);
        }
    }
    
}
