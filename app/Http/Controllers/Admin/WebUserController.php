<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Customer;

use Illuminate\Http\Request;
use App\Exports\WebUsersExport;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class WebUserController extends Controller
{
    public function index()
    {
        $data = [];
        return view('admin.user.web_user_list',$data);
    }
   
    public function get(Request $request)
    {
        $count = $request->count ?? 10;
        $search = $request->search ?? '';
        $sort = $request->sort ?? '';
        $sortOrder = $request->sortOrder ?? 'ASC';

        $query = User::where('user_type', 'User')
        ->where(function($q) use ($search) {
            $q->where('name', 'LIKE', "%{$search}%")
            ->orWhere('email', 'LIKE', "%{$search}%")
            ->orWhere('mobile', 'LIKE', "%{$search}%")
            ->orWhere('created_at', 'LIKE', "%{$search}%");
        });

        if($sort)
        {
            $query = $query->orderBy($sort, $sortOrder);
        }

        $users = $query->paginate($count);
        
        return response()->json($users);
    }

    public function addUser()
    {
        $data = [];
        return view('admin.webuser.add_user',$data);
    }

    public function storeUser(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'         => 'required|string',
            'email'        => 'required|email|unique:users',
            'mobile'     => 'required|unique:users|numeric',
            'password'     => 'required|min:6',
        ]);
        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }  
        if($request->status=='on')
        {
            $status= 1;
        }
        else 
        {
            $status=0;
        }
        CreateUser:
        $username = $number = mt_rand(1000000000, 9999999999);

        if(User::where('username', $username)->count())
        {
            goto CreateUser;
        }

        $adduser = new User();
        $adduser->username = $username;
        $adduser->name = $request->name;
        $adduser->email = $request->email;
        $adduser->mobile = $request->mobile;
        $adduser->password = Hash::make($request->password);
        $adduser->type = 'user';
        $adduser->mobile_verified_at = Carbon::now();
        $adduser->email_verified_at = Carbon::now();
        $adduser->active = $status;
        $adduser->save();

        return redirect()->route('admin.user')->with('success','User Added!');
    }

    public function editUser($id)
    {
        $data['user'] = User::where('id',$id)->first();
        return view('admin.user.edit_web_user',$data);
    }
    public function showUser($id)
    {
        $data['user'] = User::where('id',$id)->first();
        return view('admin.user.show_web_user',$data);
    }
    public function updateUser(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'id'           =>'required|integer|exists:users',
            'name'         => 'required|string',
            'email'        => 'required|email',
            'mobile'       => 'required|numeric',
            'password'     => 'sometimes',
        ]);
        if($validator->fails())
        {
        	return redirect()->back()->withErrors($validator->errors());
        }  
        if($request->status=='on')
        {
            $status= 1;
        }
        else 
        {
            $status=0;
        }
        if($request->password)
        {
            $password=  Hash::make($request->password);
        }
        else 
        {
            $password=  Hash::make('password');
        }
        User::where('id',$request->id)->update([
            'name' => $request->name ,
            'email' => $request->email ,
            'mobile' => $request->mobile ,
            'block'  => $status,
            'password' => $password,
            'send_password' => $request->password!=null?$request->password:'password',
         ]);
    
        return redirect()->route('admin.webuser.index')->with('success','User Update!');
    }

    
    public function userdelete(Request $request,$id)
    {
        User::where('id',$id)->delete();
            Session::flash('success', 'User Deleted!');
            return redirect()->route('admin.webuser.index');
    }
    
    public function bulkAction(Request $request)
    {
        $request->validate([
            'action'    =>  'required|in:active,inactive,delete',
            'ids'       =>  'required|array|min:1|max:10000',
            'ids.*'     =>  'required|exists:users,id',
        ]);

        switch($request->action)
        {
            case 'active' :
                User::whereIn('id', $request->ids)->update(['block' => 1]);
                break;
            case 'inactive' :
                User::whereIn('id', $request->ids)->update(['block' => 0]);
                break;
            case 'delete' :
                User::whereIn('id', $request->ids)->delete();
                break;
            default :
                return response()->json(['error' => 'Invalid action!'], 422);
                break;
        }
        return response()->json(['success' => 'Success!']);
    }

    public function excel_export(Request $request) 
    {
        return Excel::download(new WebUsersExport($request->action), 'web-users.xlsx');
    }

    public function updatestatus(Request $request)
    {
        $User = User::find($request->id);
        if($request->forval=='web')
        {
            $User->block = $request->status;
        }
        if($User->save())
        {
            return response()->json(['status' => '1','massage'=>'User Status Updated!']);
        }
        else
        {
            return response()->json(['status' => '0','massage'=>'User Status Not Updated!']);
        }
    }
}
