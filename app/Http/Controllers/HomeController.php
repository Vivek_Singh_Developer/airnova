<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\Setting;
use App\Models\FeedBack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data['events'] = [];
        $data['about'] = Content::where('key','about_us')->first();
        $data['vision'] = Content::where('key','our_vision')->first();
        $data['refer'] = Content::where('key','refer')->first();
        return view('index',$data);
    }

    // public function chat()
    // {
    //     $data['topics']= Topic::orderBy('id','DESC')->paginate(10);
    //     return view('frontend.chat',$data);
    // }


    
   
}
