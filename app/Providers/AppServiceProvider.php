<?php

namespace App\Providers;

use Exception;
use App\Models\Social;
use App\Models\Service;
use App\Models\Setting;
use App\Models\ProductCategory;
use App\Models\AdminNotification;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try
        {
            $data = Setting::get();

            $settings = [];

            foreach ($data as $item)
            {
                $settings[$item->key] = $item->value;
            }

            $settings = (Object) $settings;
           
            View::share('settings', $settings);
        }
        catch(Exception $e) {}

        try
        {
            $socialsdata = Social::get();;
           
            View::share('socialsdata', $socialsdata);
        }
        catch(Exception $e) {}

        try
        {
            $services = Service::get();;
           
            View::share('services', $services);
        }
        catch(Exception $e) {}
        try
        {
            $categories = ProductCategory::get();
           
            View::share('categories', $categories);
        }
        catch(Exception $e) {}

    }
}
