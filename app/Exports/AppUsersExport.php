<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AppUsersExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents
{
    protected $action;

    function __construct($action) 
    {
            $this->action = $action;
    }

    public function headings():array{
        return[
            'Name',
            'Email',
            'Mobile',
            'DOB',
            'User Code',
            'User Level',
            'Registered Date',
            'Status'
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if($this->action=='active')
        {
            $data=User::where('block','1')->get(['name','email','mobile','dob','my_refer_code','user_level','created_at','block']);
        }
        elseif($this->action=='inactive')
        {
            $data=User::where('block','0')->get(['name','email','mobile','dob','my_refer_code','user_level','created_at','block']);
        }
        else
        {
            $data=User::get(['name','email','mobile','dob','my_refer_code','user_level','created_at','block']);
        }
        return $data;
    }
   

}
