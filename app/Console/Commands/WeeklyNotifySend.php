<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Task;
use App\Models\Content;
use App\Models\TaskGroup;
use App\Models\GroupTimer;
use App\Models\Notification;
use App\Models\ActiveTaskList;
use App\Models\PivotTaskGroup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\UserTypeTaskGroupList;

class WeeklyNotifySend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:weeknotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Weekly Notification to All Users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {      
        $taskgroup = TaskGroup::where('group_type','Weekly')->where('start_time','!=',null)
        ->where('end_time','!=',null)->get();
        if(count($taskgroup)>0)
        { 
            $tasks = [];
            $date['st'] = '';
            $date['et'] = '';
            foreach($taskgroup as $item)
            {
                $group = GroupTimer::where('id',$item->group_id)->first();
                $currentDate = Carbon::now()->format('Y-m-d H:i:s');  
                $newstartdate = $group!=null?Carbon::parse($group->start_time)->format('Y-m-d H:i:s'):'';
                $newenddate = $group!=null?Carbon::parse($group->end_time)->format('Y-m-d H:i:s'):'';
                if($newstartdate !='' && $newenddate !='' && ($newstartdate < $currentDate) && ($currentDate < $newenddate))
                {
                    $date['st'] = Carbon::parse($group->start_time)->format('Y-m-d H:i:s');
                    $date['et'] = Carbon::parse($group->end_time)->format('Y-m-d H:i:s');   
                }
              
                $startDate = Carbon::parse($item->start_time)->format('Y-m-d H:i:s'); 
                $endDate = Carbon::parse($item->end_time)->format('Y-m-d H:i:s');

                $procat = [];
                $tasknamearray = [];
                $status = false;
               
                if(($startDate < $currentDate) && ($currentDate < $endDate))
                {
                    $pivottaskgroup = PivotTaskGroup::where('task_group_id',$item->id)->get();
                    foreach($pivottaskgroup as $cateitem)
                    {
                        $taskdata = Task::where('task_type','Weekly')->where('id',$cateitem->task_id)->first();
                        $status = true;
                    }
                }
                
            }
            
            $currentDate2 = Carbon::now()->format('Y-m-d H:i:s');
            if($date['st']!='' && $date['et']!='' && ( $date['st'] < $currentDate2) && ($currentDate2 < $date['et'] ))
            {
                $startTimenoti = Carbon::parse($currentDate2);
                $finishTimenoti = Carbon::parse($date['et'] );
                $totalDuration = $finishTimenoti->diffInHours($startTimenoti);
             
                if($totalDuration!=0 && $totalDuration<=10)
                {
                    $notification = Notification::where('key','weekly_task')->first();
                    if($notification!=null && $notification->status==1)
                    {
                        $title = 'Only '.$totalDuration.' Hours Left To Complete Weekly Task';
                        $aaa['key'] = 'weekly_task';
                        $aaa['product_id'] = null;
                        $aaa['custom_image'] = $notification->image!=null?url('storage/notification/'.$notification->image):null;
                        $send = sendNotificationtoalluser('Hurry Up !',$title,$aaa,null);
                    }
                }
            }

            $this->info('Successfully Notification Send.');
        }
        
    }
}
