<?php

namespace App\Console\Commands;

use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class HourlyLiveRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively update database with live rate of Metals.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // $response = Http::withHeaders([
        //     'X-API-KEY'=>'f6412fdf260d55b6cb460cb4e5bad69cf6412fdf',
        //     'Content-Type'=>'application/json'
        // ])->get('http://goldpricez.com/api/rates/currency/inr/measure/gram/metal/all');
        // if($response->status()==200)
        // {
        //     $data = json_decode($response->json(),true);

        //     Setting::where('key','silverRateInrgm')->update(['value'=>$data['silver_gram_in_inr']]);
        //     Setting::where('key','goldRateInrgm')->update(['value'=> $data['gram_in_inr']]);
        //     Setting::where('key','goldRateUSD')->update(['value'=>$data['gram_in_usd']]);
        //     Setting::where('key','silverRateUSD')->update(['value'=>$data['silver_gram_in_usd']]);
        //     $this->info('Successfully update hourly rate.');

        // }

        // $silverINR = getRate('INR','XAG');
        // $goldINR = getRate('INR','XAU');
        // $silverUSD = getRate('USD','XAG');
        // $goldUSD = getRate('USD','XAU');
        $data = getRate();
        if($data!=null)
        {
            $sellvaue = $data['result']['data']['rates']['gSell'];
            Setting::where('key','silverRateInrgm')->update(['value'=> $data['result']['data']['rates']['sBuy']]);
            Setting::where('key','buygoldRateInrgm')->update(['value'=> $data['result']['data']['rates']['gBuy']]);
            Setting::where('key','goldRateInrgm')->update(['value'=>  $sellvaue -( ($sellvaue *0.5)/100)]);
            Setting::where('key','allrate')->update(['value'=> json_encode($data['result']['data']['rates'],true)]);

        }

        // if($silverINR!=null && $goldINR!=null && $silverUSD!=null && $goldUSD!=null)
        // {

        //     $silverPriceInrPerGram = ($silverINR['price']+$silverINR['ch']) / 28.3495;
        //     $goldPriceInrPerGram = ($goldINR['price']+$goldINR['ch']) / 28.3495;

        //     Setting::where('key','silverRateInrgm')->update(['value'=>$silverPriceInrPerGram]);
        //     Setting::where('key','goldRateInrgm')->update(['value'=>$goldPriceInrPerGram]);
        //     Setting::where('key','goldRateUSD')->update(['value'=>round($goldUSD['price'],2)]);
        //     Setting::where('key','silverRateUSD')->update(['value'=>round($silverUSD['price'],2)]);
        //     $this->info('Successfully update hourly rate.');
        // }

    }
}
