<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'my_refer_code',
        'user_type',
        'dob',
        'mobile',
        'address',
        'GST_number',
        'refferal_code',
        'send_password',
        'block',
        'latitude',
        'longitude',
        'fcm_token',
        'wallet_pin',
        'user_level',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'send_password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'user_level' => 'int',
        'block' => 'int',
    ];

    public function findForPassport($username)
    {
        return $this->where('email', $username)
        ->orWhere('mobile', $username)
        ->first();
    }

    public function getProfileImageAttribute($value)
    {
        return $value ? url(Storage::url('userprofile/'.$value)) : null;
    }

    public function getCreatedAtAttribute($value)
    {
        return $value ? \Carbon\Carbon::parse($value)->format('Y-m-d'): null;
    }

}
