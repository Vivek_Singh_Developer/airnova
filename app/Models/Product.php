<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'product_code',
        'product_name',
        'thumbnail_image',
        'product_price',
        'status',
    ];

    protected $casts = [
        'category_id' => 'int',
        'like_count' => 'int',
        'default_count' => 'int',
    ];

    public function getCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id', 'id');
    }
   
    public function getImages()
    {
        return $this->hasMany(ProductImages::class, 'product_id', 'id'); 
    }
}
