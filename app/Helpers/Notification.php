<?php

use App\Models\User;
use Illuminate\Support\Facades\Http;

if(!function_exists('notify'))
{
    function notify($user, $body, $title, $data = [1])
    {
        return fcm($user->fcm_token ?? 'avc',$user->type, $body, $title, $data);
    }
}

if(!function_exists('fcm'))
{
    function fcm($fcmToken,$type, $body, $title, $data)
    {

        $fcm=env('FCM_SERVER_KEY_USER');
        $data['priority']="high";
        $data['content_available']=true;
        try
        {
            $response = Http::withHeaders([
                "Content-Type" => "application/json",
                "Authorization" => "key=" . $fcm
            ])
            ->post('https://fcm.googleapis.com/fcm/send', [
                'to' => $fcmToken,
                'notification' => [
                    "title" => $title,
                    "body" => $body,
                    "mutable_content" => true,
                    "sound" => "Tri-tone",
                    "icon"=>url('images/ppcnotification.svg')
                ],
                'data' => $data
            ]);
            return $response->json();
        }
        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }
}


if(!function_exists('sendNotificationtoalluser'))
{
    function sendNotificationtoalluser( $body, $title, $data,$user)
    {
        if($user!=null)
        {
            $firebaseToken = User::select('fcm_token')->where('id',$user)->where('type','app')->get();
            $firebaseTokenos = User::select('fcm_token')->where('id',$user)->where('type','ios')->get();
        }
        else
        {
            $firebaseToken = User::whereNotNull('fcm_token')->where('type','app')->pluck('fcm_token')->all();
            $firebaseTokenos = User::whereNotNull('fcm_token')->where('type','ios')->pluck('fcm_token')->all();
        }

        $fcm='AAAAmke6sj4:APA91bEP05VCKUdwmpENUAaN9TRIp0EUZ7xInKgjsp6H5g1SrMBv-X2y48Z1kNvZwYdolA5xEK0u6Q0Vc_k2yX1gw73HPIK3j55QnJC_lNlPSpiob1oWqrTTLZT_8AZK0-9T_KiW5F_3';
        if(count( $firebaseToken )>0)
        {
            $data["title"] = $title;
            $data["body"] = $body;
            $data["mutable_content"] = true;
            $data["image"] = $data['custom_image'];
            $data["sound"] = url('images/notification.mp3');
            $data["icon"]= url('images/ppnotification.png') ;
            $data["android_channel_id"]="fcm_channel_id";
            $data['priority']="high";
            $data['content_available']=true;
            try
            {
                $response = Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Authorization" => "key=" . $fcm
                ])
                ->post('https://fcm.googleapis.com/fcm/send', [
                    'registration_ids' => $firebaseToken,
                    // 'notification' => [
                    //     "title" => $title,
                    //     "body" => $body,
                    //     "mutable_content" => true,
                    //     "image" => $data['custom_image'],
                    //     "sound" => url('images/notification.mp3'),
                    //     "icon"=>url('images/ppnotification.png'),
                    // ],
                    'data' => $data
                ]);
                // dd($response->json());
                $res['app'] = $response->json();
            }
            catch (Exception $e)
            {
                 $res['app'] = $e->getMessage();
            }
        }
        else
        {
             $res['app'] = [];
        }

        if(count( $firebaseTokenos )>0)
        {
            try
            {
                $data['priority']="high";
                $data['content_available']=true;
                $response = Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Authorization" => "key=" . $fcm
                ])
                ->post('https://fcm.googleapis.com/fcm/send', [
                    'registration_ids' => $firebaseTokenos,
                    'notification' => [
                        "title" => $title,
                        "body" => $body,
                        "mutable_content" => true,
                        "content_available" => true,
                        "image" => $data['custom_image'],
                        "sound" => url('images/notification.mp3'),
                        "icon"=>url('images/ppnotification.png'),
                        "priority"=>'high',
                        "android_channel_id"=>'fcm_channel_id',
                    ],
                    'data' => $data
                ]);

                $res['ios'] = $response->json();
            }
            catch (Exception $e)
            {
                 $res['ios'] = $e->getMessage();
            }
        }
        else
        {
             $res['ios'] = [];
        }
        // dd($res);
        return $res;

    }
}

if(!function_exists('sendNotificationtosingleuser'))
{
    function sendNotificationtosingleuser($body, $title, $data,$user)
    {
        $firebaseToken = User::select('fcm_token','type')->where('id',$user)->first();
        $fcm='AAAAmke6sj4:APA91bEP05VCKUdwmpENUAaN9TRIp0EUZ7xInKgjsp6H5g1SrMBv-X2y48Z1kNvZwYdolA5xEK0u6Q0Vc_k2yX1gw73HPIK3j55QnJC_lNlPSpiob1oWqrTTLZT_8AZK0-9T_KiW5F_3';

        if($firebaseToken !=null && $firebaseToken->type=='app')
        {
            $data["title"] = $title;
            $data["body"] = $body;
            $data["mutable_content"] = true;
            $data["content_available"] = true;
            $data["image"] = $data['custom_image'];
            $data["sound"] = url('images/notification.mp3');
            $data["icon"]= url('images/ppnotification.png') ;
            $data["priority"] = 'high';
            $data["android_channel_id"]="fcm_channel_id";

            try
            {
                $response = Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Authorization" => "key=" . $fcm
                ])
                ->post('https://fcm.googleapis.com/fcm/send', [
                    'to' => $firebaseToken->fcm_token,
                    'data' => $data
                ]);
                $res['app'] = $response->json();
                // dd($data);
            }
            catch (Exception $e)
            {
                 $res['app'] = $e->getMessage();
            }
        }

        if($firebaseToken !=null && $firebaseToken->type=='ios')
        {
            try
            {
                $data["content_available"] = true;
                $data["priority"] = 'high';

                $response = Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Authorization" => "key=" . $fcm
                ])
                ->post('https://fcm.googleapis.com/fcm/send', [
                    'to' => $firebaseToken->fcm_token,
                    'notification' => [
                        "title" => $title,
                        "body" => $body,
                        "mutable_content" => true,
                        "content_available" => true,
                        "image" => $data['custom_image'],
                        "sound" => url('images/notification.mp3'),
                        "icon"=>url('images/ppnotification.png'),
                        "priority"=>'high',
                        "android_channel_id"=>'fcm_channel_id',
                    ],
                    'data' => $data
                ]);

                $res['ios'] = $response->json();
            }
            catch (Exception $e)
            {
                 $res['ios'] = $e->getMessage();
            }
        }
        // dd($res);
        return $res;

    }
}
