<?php

use App\Models\User;
use App\Models\Setting;
use App\Models\UserOTP;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use PhpOffice\PhpSpreadsheet\Calculation\TextData\Replace;

if(!function_exists('otpsenddata'))
{
    function otpsenddata($mobile,$otp)
    {
        $textmsg = "<#>".$otp." is your OTP that you've requested to get registered on Mobile Gold Application /tPqDrCfJiQ";
        $sender = urlencode('mGOLD');
        
        // $textmsg =  $otp." is your OTP that you've requested to get registered on www.playpausecontinue.com";
        // $sender = urlencode('PLAYPC');

        $apiKey = urlencode('NmY1NzRmNGUzNzQ4NTY0NjQyMzYzNjZjNGQ1YTU1NjE=');
        $numbers = array($mobile);
        $numbers = implode(',',$numbers);
        $message = rawurlencode($textmsg);
        $data = array("apikey" => $apiKey, "numbers" => $numbers, "sender" => $sender, "message" => $message);

        $ch = curl_init("https://api.textlocal.in/send/");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // echo $response;

    }
}

if(!function_exists('sendOtp'))
{
    function sendOtp($mobile)
    {
        $otp = mt_rand(1000, 9999);
        // $otp = 1234;
        $textmsg = "<#>".$otp." is your OTP that you've requested to get registered on Mobile Gold Application /tPqDrCfJiQ";
        // $textmsg =  $otp." is your OTP that you've requested to get registered on www.playpausecontinue.com";
        $apiKey = urlencode('NmY1NzRmNGUzNzQ4NTY0NjQyMzYzNjZjNGQ1YTU1NjE=');
        $sender = urlencode('mGOLD');
        // $sender = urlencode('PLAYPC');
        $numbers = array('91'.$mobile);
        $numbers = implode(',',$numbers);
        $message = rawurlencode($textmsg);
        // dd($message);
        $data = array("apikey" => $apiKey, "numbers" => $numbers, "sender" => $sender, "message" => $message);

        $ch = curl_init("https://api.textlocal.in/send/");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // dd($response);

        UserOTP::where('mobile',$mobile)->delete();
        UserOTP::updateOrCreate([
            'mobile' => $mobile
        ], [
            'otp'       =>  $otp,
            'attempt'   =>  0,
        ]);

        return true;
    }
}

if(!function_exists('sendOtpWeb'))
{
    function sendOtpWeb($mobile)
    {
        $otp = mt_rand(1000, 9999);
        // $otp = 1234;
        // $textmsg = "<#>".$otp." is your OTP that you've requested to get registered on Mobile Gold Application /tPqDrCfJiQ";
        $textmsg =  $otp." is your OTP that you've requested to get registered on www.playpausecontinue.com";
        $apiKey = urlencode('NmY1NzRmNGUzNzQ4NTY0NjQyMzYzNjZjNGQ1YTU1NjE=');
        // $sender = urlencode('mGOLD');
        $sender = urlencode('PLAYPC');
        $numbers = array('91'.$mobile);
        $numbers = implode(',',$numbers);
        $message = rawurlencode($textmsg);
        // dd($message);
        $data = array("apikey" => $apiKey, "numbers" => $numbers, "sender" => $sender, "message" => $message);

        $ch = curl_init("https://api.textlocal.in/send/");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        // dd($response);

        UserOTP::where('mobile',$mobile)->delete();
        UserOTP::updateOrCreate([
            'mobile' => $mobile
        ], [
            'otp'       =>  $otp,
            'attempt'   =>  0,
        ]);

        return true;
    }
}
if(!function_exists('getRate'))
{
    function getRate()
    {
        // $currency,$type
        $apiURL = 'https://uat-api.augmontgold.com/api/merchant/v1/auth/login';
        $postInput = [
            'email' => 'pranav@playpausecontinue.com',
            'password' => 'Mj9*)f85jg53+L6Xvz',
        ];
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($apiURL, $postInput);
        if($response->status()==200)
        {
            $token =  $response['result']['data']['accessToken'];
            $response2 = Http::withHeaders([
                'Accept' =>'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$token
                ])->get('https://uat-api.augmontgold.com/api/merchant/v1/rates');
            return $response2;
        }
        else
        {
            return null ;
        }
        // $response = Http::withHeaders([
        //     'x-access-token'=>'goldapi-rfjs5tkx4b5lx0-io',
        //     'Content-Type'=>'application/json'
        // ])->get('https://www.goldapi.io/api/'.$type.'/'.$currency);
        // if($response->status()==200)
        // {
        //     return $data = $response->json();
        // }
        // else
        // {
        //     return null ;
        // }
    }
}





if(!function_exists('convertinhindi'))
{
    function convertinhindi( $string)
    {
        return strtr($string, array(
            '۰'=>'.',
            '0'=>'०',
            '1'=>'१',
            '2'=>'२',
            '3'=>'३',
            '4'=>'४',
            '5'=>'५',
            '6'=>'६',
            '7'=>'७',
            '8'=>'८',
            '9'=>'९'
        ));
    }
}



