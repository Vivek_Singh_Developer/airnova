<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    
    public function register()
    {

        $this->renderable(function (NotFoundHttpException $exception,$request) {
            if($request->is('api/*'))
            {
                $statusCode = $exception->getStatusCode($exception);
                if ($statusCode === 404)
                {
                    return res($statusCode, 'URL Not Found');
                }
            }
        });
    }

    protected function invalidJson($request, ValidationException $exception)
    {
        $errors = [];

        foreach($exception->errors() as $key => $error) 
        {
            $errors[$key] = $error[0];
        }

        return res_failed($exception->getMessage(), [
            'errors' => $errors,
        ]);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
                    ? res(401, $exception->getMessage())
                    : redirect()->guest($exception->redirectTo() ?? route('login'));
    }

    // public function render($request, Throwable $exception)
    // {
    //     $response = parent::render($request, $exception);
    //     if ($response->status() === 401) {
    //         return response(view('errors.401'), 401);
    //     }
    //     if ($response->status() === 403) {
    //         return response(view('errors.403'), 403);
    //     }
    //     if ($response->status() === 404) {
    //         return response(view('errors.404'), 404);
    //     }
    //     if ($response->status() === 419) {
    //         return response(view('errors.419'), 419);
    //     }
    //     if ($response->status() === 429) {
    //         return response(view('errors.429'), 429);
    //     }
    //     if ($response->status() === 500) {
    //         return response(view('errors.500'), 500);
    //     }
    //     if ($response->status() === 503) {
    //         return response(view('errors.503'), 503);
    //     }
    //     return $response; 
    // }
}
