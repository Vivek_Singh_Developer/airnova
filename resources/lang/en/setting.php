<?php

return [

    'AppName' => env('APP_NAME'),
    'AppTitle' => 'Welcome | Airnova',
    'AdminLogin' => 'Admin Login',

];