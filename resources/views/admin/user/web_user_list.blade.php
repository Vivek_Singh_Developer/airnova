@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Website User List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Manage User</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Website User List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5></h5>
                        <div class="ibox-tools">
                            <form action="{{ route('admin.webuser.export.excel') }}" method="post" hidden>
                                @csrf
                                <input type="hidden" name="action" id="fillexport"/>
                                <button id="exportsubmit">submit</button>
                            </form> 
                            {{-- <a class="btn btn-xs btn-dark" href="{{ route('admin.webuser.add') }}">Add user <i class="fa fa-plus"></i></a> --}}
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="row">

                            <div class="col-sm-3 m-b-xs ">
                                Show 
                                <select id="dataCount" onchange="loadData()" class="form-control-sm form-control input-s-sm inline mw-mc">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                entries
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <select id="selectedAction" class="form-control-sm form-control input-s-sm inline">
                                        <option value="">Select Action</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                        <option value="delete">Delete</option>
                                    </select>
                                    <span class="input-group-append"> 
                                        <button type="button" onclick="applyAction()" id="actionBtn" class="btn btn-sm btn-dark disabled">Apply</button> 
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <select id="selectedExport" class="form-control-sm form-control input-s-sm inline">
                                        <option value="">Select Export</option>
                                        <option value="all">All</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                    <span class="input-group-append"> 
                                        <button type="button" onclick="applyExport()" id="actionExportBtn" class="btn btn-sm btn-dark disabled">Export</button> 
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input placeholder="Search" id="search" type="text" class="form-control form-control-sm"> 
                                    <span class="input-group-append"> 
                                        <button type="button" onclick="loadData()" class="btn btn-sm btn-dark">Go!</button> 
                                    </span>
                                </div>
                            </div>
                            
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="chkCheckAll"/></th>
                                        <th>#</th>
                                        <th onclick="sortData(this)">Name <i data-name="name" class="fa fa-sort sortingElement"></i></th>
                                        <th onclick="sortData(this)">Email <i data-name="email" class="fa fa-sort sortingElement"></i></th>
                                        <th onclick="sortData(this)">Mobile <i data-name="mobile" class="fa fa-sort sortingElement"></i></th>
                                        <th onclick="sortData(this)">Registered Date <i data-name="created_at" class="fa fa-sort sortingElement"></i></th>
                                        {{-- <th onclick="sortData(this)">Status <i data-name="block" class="fa fa-sort sortingElement"></i></th> --}}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tableData">
                                    {{-- Table Data --}}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" id="dataInfo">{{-- Data Info --}}</td>
                                        <td colspan="4" class="footable-visible">
                                            <ul class="pagination float-right" id="dataPagination">
                                                {{-- Data Pagination --}}
                                            </ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@push('script')

    <script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script>

        var tableData = [];
        var currentLink = "{{ route('admin.webuser.get') }}";
        var sort = '';
        var sortOrder = 'ASC';
        var actions = ['active', 'inactive', 'delete'];
        var exportactions = ['active', 'inactive', 'all'];

        $(document).ready(function()
        {   
            loadData(currentLink);    
        });

        $("#chkCheckAll").click(function()
        {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        function applyAction()
        {
            let selectedAction = $('#selectedAction').val();

            if(actions.includes(selectedAction))
            {
                let userIds = [];

                $(".checkBoxClass:checked").each(function(){
                    userIds.push($(this).val());
                });

                if(userIds.length)
                {
                    swal({
                        title: "Are you sure?",
                        text: "Your may not be able to revert this.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Confirm",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false,
                        closeOnCancel: false 
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{ route('admin.webuser.bulkAction') }}",
                                type: 'post',
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    action: selectedAction,
                                    ids: userIds
                                },
                                success: (res) => {
                                    swal("Success!", "", "success");
                                    $('#selectedAction').val('');
                                    $('#actionBtn').addClass('disabled');
                                    loadData(currentLink);
                                },
                                error: (err) => {
                                    console.log(err);
                                    swal("Server Error!", "", "error");
                                },
                            });
                        }
                    });
                }
                else
                {
                    swal({
                        title: "Warning!",
                        text: "Please select records first.",
                        type: "warning",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        timer: 2000
                    });
                }
            }
        }

        function applyExport()
        {
            let selectedExportAction = $('#selectedExport').val();

            if(exportactions.includes(selectedExportAction))
            {
                    swal("Success!", "", "success");
                    $('#fillexport').val(selectedExportAction);
                    $('#exportsubmit').click();
                    $('#selectedExport').val('');
                    $('#actionExportBtn').addClass('disabled');

            }
        }

        function loadData(link = "{{ route('admin.webuser.get') }}")
        {
            $("#chkCheckAll").prop('checked', false);
            $('#dataInfo').text('Showing 0 to 0 of 0 entries');
            $('#tableData').html(`
                <tr>
                    <td colspan="10">Loading...</td>
                </tr>
            `);
            $('#dataPagination').html('');

            let count = $('#dataCount').val();
            let search = $('#search').val();

            $.ajax({
                url: link,
                type: 'GET',
                data: {
                    count,
                    sort,
                    sortOrder,
                    search
                },
                success: function(res) {
                    tableData = res.data;
                    currentLink = res.path + '?page=' + res.current_page;

                    if(res.data && res.data.length) 
                    {
                        $('#dataInfo').text(`Showing ${res.from} to ${res.to} of ${res.total} entries`);
                        $('#tableData').html('');
                        $.each(res.data, (key, val) => {
                            let row = `
                                <tr id="sid${val.id}">
                                    <td><input type="checkbox" class="checkBoxClass i-checks" value="${val.id ?? '-'}" name="ids[]"></td>
                                    <td>${res.from++}</td>
                                    <td>${val.name ?? '-'}</td>
                                    <td>${val.email ?? '-'}</td>
                                    <td>${val.mobile ?? '-'}</td>
                                    <td>${val.created_at ?? '-'}</td>
                                    <td>
                                        ${val.block==1 ?
                                            `<label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus(${val.id},'0','web')" checked="">
                                                        <span class="slider round"></span>
                                                    </label>` :
                                                    `<label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus(${val.id},'1','web')">
                                                        <span class="slider round"></span>
                                                    </label>`
                                        }
                                        <a class='ml-2 btn btn-xs btn-primary' href="{{ route('admin.webuser.show')}}/${val.id}"><i class=' fa fa-eye'></i></a>
                                        <a class='ml-2 btn btn-xs btn-warning' href="{{ route('admin.webuser.edit')}}/${val.id}"><i class=' fa fa-pencil'></i></a>
                                        <a class='btn btn-xs btn-dark demotest'  href="{{ route('admin.webuser.delete')}}/${val.id}"><i class=' fa fa-trash'></i></a>
                                    </td>
                                </tr>
                            `;
                            
                            $('#tableData').append(row);
                        });

                        $.each(res.links, (key, val) => {
                            let row = `
                                <li class="footable-page ${val.active ? 'active' : ''}">
                                    <a ${val.url ? `onclick="loadData('${val.url}')"` : ''}>${val.label}</a>
                                </li>
                            `;
                            
                            $('#dataPagination').append(row);
                        });
                    }
                    else
                    {
                        $('#tableData').html(`
                            <tr>
                                <td colspan="10">Record not found!</td>
                            </tr>
                        `);
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }

        function sortData(e)
        {
            let iconElement = e.children[0];
            let changableClass = 'fa-sort-down';
            sort = $(iconElement).attr('data-name');
            sortOrder = 'ASC';

            if($(iconElement).hasClass('fa-sort-down'))
            {
                changableClass = 'fa-sort-up';
                sortOrder = 'DESC';
            }

            $('.sortingElement')
            .removeClass('fa-sort')
            .removeClass('fa-sort-up')
            .removeClass('fa-sort-down')
            .addClass('fa-sort');

            $(iconElement).removeClass('fa-sort').addClass(changableClass);

            loadData();
        }

        $('#selectedAction').on('change', function() 
        {
            if(actions.includes($(this).val()))
            {
                $('#actionBtn').removeClass('disabled');
            }
            else
            {
                $('#actionBtn').addClass('disabled');
            }
        });

        $('#selectedExport').on('change', function() 
        {
            if(exportactions.includes($(this).val()))
            {
                $('#actionExportBtn').removeClass('disabled');
            }
            else
            {
                $('#actionExportBtn').addClass('disabled');
            }
        });

        $(document).on('click', '.demotest', function() 
        {
            event.preventDefault();
            const url = $(this).attr('href');

                swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = url;
                        swal("Deleted!", "Your record has been deleted.", "success");
                    } else {
                        swal("Cancelled", "Your record is safe.", "error");
                    }
                });
        });

        function updatestatus(id,value,forval) 
        {
            var id= id;
            var url="{{ url('admin/web-user/status')}}"+'/'+id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    status:value,
                    forval:forval,
                    _token: "{{ csrf_token() }}",
                },
                url: url,
                success: function (data) {
                    if(data.status=='1')
                    {
                        location.reload();
                    }
                    if(data.status=='0')
                    {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Error!", "", "error");
                },
            });
        
        }

    </script>

@endpush
