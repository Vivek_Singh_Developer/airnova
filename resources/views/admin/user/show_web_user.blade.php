@extends('admin.layouts.admin')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2> User Detail</h2>
            
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Manage User</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Website User Detail</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart">
                                    @if( $user->profile_image!=null)
                                        <div class="row">
                                             <div class="col-md-1 "><b> </b></div>
                                            <div class="col-md-11 text-left"><img style="height:200px;width:200px;" src="{{$user->profile_image}}"/></div>
                                        </div>  
                                    @endif
                                    <div class="row">
                                        <div class="col-md-2 "><b>Name </b></div>
                                        <div class="col-md-10 "> {{ $user->name}}</div>
                                    
                                    </div>
                                     @if( $user->email!=null)
                                    <div class="row">
                                        <div class="col-md-2"><b>Email  </b></div>
                                        <div class="col-md-10"> {{ $user->email?$user->email:'N/A'}}</div>
                                    </div>
                                     @endif
                                    @if( $user->mobile!=null)
                                    <div class="row">
                                        <div class="col-md-2"><b>Mobile  </b></div>
                                        <div class="col-md-10"> {{ $user->mobile?$user->mobile:'N/A'}}</div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-2"><b>User Code </b></div>
                                        <div class="col-md-10"> {{ $user->my_refer_code}}</div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-md-2"><b>User Type </b></div>
                                        <div class="col-md-10"> {{ $user->user_type}}</div>
                                    </div>
                                    @if( $user->dob!=null)
                                        <div class="row">
                                            <div class="col-md-2"><b>DOB </b></div>
                                            <div class="col-md-10"> {{ Carbon\Carbon::parse($user->dob)->format('d M Y')}}</div>
                                        </div>  
                                    @endif
                                    @if( $user->address!=null)
                                     <div class="row">
                                        <div class="col-md-2"><b>Address </b></div>
                                        <div class="col-md-10"> {{ $user->address}}</div>
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-2"><b>Status </b></div>
                                        <div class="col-md-10"> {{ $user->block==1?'Active':'Inactive'}}</div>
                                    </div> 
                                     <div class="row">
                                        <div class="col-md-2"><b>User Level </b></div>
                                        <div class="col-md-10"> {{ $user->user_level}}</div>
                                    </div> 
                                    @if( $user->GST_number!=null)
                                        <div class="row">
                                            <div class="col-md-2"><b>GST Number </b></div>
                                            <div class="col-md-10"> {{ $user->GST_number}}</div>
                                        </div>  
                                    @endif
                                    @if( $user->latitude !=null)
                                        <div class="row">
                                            <div class="col-md-2"><b>Latitude </b></div>
                                            <div class="col-md-10"> {{ $user->latitude }}</div>
                                        </div>  
                                    @endif
                                    @if( $user->longitude!=null)
                                        <div class="row">
                                            <div class="col-md-2"><b>Longitude </b></div>
                                            <div class="col-md-10"> {{ $user->longitude}}</div>
                                        </div>  
                                    @endif
                                    <div class="row">
                                        <div class="col-md-2"><b>Date </b></div>
                                        <div class="col-md-10"> {{ Carbon\Carbon::parse($user->created_at)->format('d M Y')}}</div>
                                    </div>                              
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@push('script')
    <script>

        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#1AB394' });

    </script>
@endpush
