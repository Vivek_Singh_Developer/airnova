@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Edit Notification</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Manage Notification</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Notification</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5></h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.notifications.update',$noification->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Key</span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="text" class="form-control" readonly value="{{ $noification->key }}"
                                        name="key" placeholder="" />
                                    @if ($errors->has('key'))
                                        <div class="error">{{ $errors->first('key') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Title </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="text"  class="form-control" value="{{ $noification->title }}" name="title"
                                        placeholder="" />
                                    @if ($errors->has('title'))
                                        <div class="error">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Body</span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="text"  class="form-control" value="{{ $noification->body }}" name="body"
                                        placeholder="" />
                                    @if ($errors->has('body'))
                                        <div class="error">{{ $errors->first('body') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span> Image</span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="file"  class="form-control"  name="image"
                                        placeholder="" />
                                    @if ($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                                @if($noification->image!=null)
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <img src="{{url('storage/notification/'.$noification->image)}}" height="100px" width="100px" />
                                    </div>
                                @endif
                            </div>

                  
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Status </span></b>
                                </div>
                                <div class="col-sm-9 mb-2">
                                    <input type="checkbox" name="status" class="js-switch"
                                        {{ $noification->status == 1 ? 'checked' : '' }}>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    @if ($noification->key == 'custom')
                                        <a class="btn btn-dark btn-md" href="{{route('admin.notifications.create')}}">Send Notification</a>
                                    @endif
                                </div>
                            </div>
                        
                                
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, {
            color: '#f39f01'
        });
    </script>
@endpush
