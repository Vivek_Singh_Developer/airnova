@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Send Notification</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Manage Notification</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Send Notification</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5></h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.notifications.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Choose Type </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <select type="text" name="key_id" class="chosen-select form-control" tabindex="-1">
                                        <option value="0">-- Choose Type--</option>
                                        @foreach ($noification as $item)
                                            <option {{$item->id == old('key_id')? 'selected' : ''}}  value="{{$item->id}}"> {{ucwords($item->key)}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('key_id'))
                                        <div class="error">{{ $errors->first('key_id') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Choose Product </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <select type="text" name="product_id" class="chosen-select form-control" tabindex="-1">
                                        <option value="0">-- Choose Product--</option>
                                        @foreach ($product as $item)
                                            <option {{$item->id == old('product_id')? 'selected' : ''}}  value="{{$item->id}}"> {{ucwords($item->product_name)}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('product_id'))
                                        <div class="error">{{ $errors->first('product_id') }}</div>
                                    @endif
                                </div>
                            </div>
                            

                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Send Notification</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, {
            color: '#f39f01'
        });
    </script>
@endpush
