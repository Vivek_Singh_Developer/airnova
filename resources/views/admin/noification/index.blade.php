@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Notification List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Manage Notification</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Notification List</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5></h5>
                        <div class="ibox-tools">
                            {{-- <a class="btn btn-sm btn-dark" href="{{ route('admin.levels.create') }}"><i
                                    class="fa fa-plus text-light"></i> Add Level</a> --}}
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Key</th>
                                        <th>Title</th>
                                        <th>Body</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($noification as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ ucwords($item->key) }}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->body }}</td>
                                            <td>
                                             @if($item->status==1)
                                                    <label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus({{$item->id}},'0')" checked="">
                                                        <span class="slider round"></span>
                                                    </label>
                                                @else
                                                    <label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus({{$item->id}},'1')">
                                                        <span class="slider round"></span>
                                                    </label>
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-xs text-light btn-warning"
                                                    href="{{ route('admin.notifications.edit', $item->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                                
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
     
       function updatestatus(id,value) 
        {
            var id= id;
            var url="{{ url('admin/notifications/status')}}"+'/'+id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    status:value,
                    _token: "{{ csrf_token() }}",
                },
                url: url,
                success: function (data) {
                    if(data.status=='1')
                    {
                        // location.reload();
                    }
                    if(data.status=='0')
                    {
                        // location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Error!", "", "error");
                },
            });
        
        }
    </script>
@endpush
