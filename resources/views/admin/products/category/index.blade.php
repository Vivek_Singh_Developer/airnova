@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2> Category List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Product</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Category</strong>
                </li>
            </ol>
            
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            
        
            <div class="col-lg-6 col-md-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5></h5>
                        <div class="ibox-tools">
                            {{-- <a class="btn btn-sm btn-warning" style="color:black !important;" href="{{ route('admin.products.index') }}">Go to Product List</a> --}}
                        </div>
                    </div>
                        
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Category Name</th>
                                        <th>Category Code</th>
                                        <th>Action</th>     
                                    </tr>
                                </thead>
                                <tbody >
                                    @forelse ($productscategory as $item)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$item->cat_name}} </td>
                                            <td>{{$item->cat_code}}</td>
                                            <td>
                                                <a class="btn btn-xs text-light btn-warning"  href="{{ route('admin.procategory.edit', $item->id)}}"><i class="fa fa-pencil"></i></a>            
                                                <button  class="btn btn-xs btn-dark" onclick="deletemyTopic({{$item->id}})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Add Product Category</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.procategory.store') }}" method="POST">
                            @csrf                            
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Category Name </span></b>
                                </div>
                                <div class="col-sm-7 mb-2">
                                    <input type="text" class="form-control" value="{{ old('cat_name') }}" name="cat_name" placeholder="" />
                                    @if($errors->has('cat_name'))
                                        <div class="error">{{ $errors->first('cat_name') }}</div>
                                    @endif
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Category Code </span></b>
                                </div>
                                <div class="col-sm-7 mb-2">
                                    <input type="text" class="form-control" value="{{ old('cat_code') }}" name="cat_code" placeholder="" />
                                    @if($errors->has('cat_code'))
                                        <div class="error">{{ $errors->first('cat_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('js/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').dataTable( {
                "columnDefs": [{ "orderable": false, "targets": [3] }]
            });
        });

        function deletemyTopic(id) 
        {
            var id= id;
            var url="{{ url('admin/procategory')}}"+'/'+id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                },
                url: url,
                success: function (data) {
                    if(data.status=='1')
                    {
                        location.reload();
                    }
                    if(data.status=='0')
                    {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Server Error!", "", "error");
                },
            });
        
        }

    </script>
@endpush