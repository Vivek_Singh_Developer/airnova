@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Edit Product</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Product</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Product</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Product</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.products.update', $products->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Choose Category </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <select name="category_id" class="chosen-select form-control" tabindex="-2">
                                        <option value="0"> -- Choose Category-- </option>
                                        @foreach ($productscategory as $productcategory1)
                                            <option
                                                {{ $products->category_id == $productcategory1->id ? 'selected' : '' }}
                                                value="{{ $productcategory1->id }}">{{ $productcategory1->cat_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <div class="error">{{ $errors->first('category_id') }}</div>
                                    @endif
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                  
                                </div>
                                <div class="col-sm-4 mb-2">
                                   
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Code </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ $products->product_code }}"
                                        name="product_code" placeholder="" />
                                    @if ($errors->has('product_code'))
                                        <div class="error">{{ $errors->first('product_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Name </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ $products->product_name }}"
                                        name="product_name" placeholder="" />
                                    @if ($errors->has('product_name'))
                                        <div class="error">{{ $errors->first('product_name') }}</div>
                                    @endif
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                    
                                </div>
                                <div class="col-sm-4 mb-2">
                                    
                                </div> 
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Price (INR)</span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ $products->product_price }}"
                                        name="product_price" placeholder="" />
                                    @if ($errors->has('product_price'))
                                        <div class="error">{{ $errors->first('product_price') }}</div>
                                    @endif
                                </div>
                               
                            </div>
                            
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Thumbnail Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" value="{{ old('image') }}" name="image"
                                        placeholder="" />
                                    @if ($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                                @if ($products->thumbnail_image != null)
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <img height="50px" width="50px"
                                            src="{{ url('storage/product/' . $products->thumbnail_image) }}" />
                                    </div>
                                @endif
                            </div>

                            {{-- <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Product Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" multiple name="product_image[]" />
                                    @if ($errors->has('product_image'))
                                        <div class="error">{{ $errors->first('product_image') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">

                                </div>
                                <div class="col-sm-10 col-sm-offset-2 row">

                                    @if (count($products->getImages) > 0)
                                        @foreach ($products->getImages as $item)
                                            @if ($item->product_image != null)
                                                <div class="col-sm-1 col-sm-offset-2 mt-4">
                                                    <img height="70px" width="70px"
                                                        src="{{ url('storage/product/' . $item->product_image) }}" />
                                                    <a href="{{ route('admin.product.image.delete', $item->id) }}"><i
                                                            class="fa fa-trash text-dark"></i></a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div> --}}
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Product Description </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <textarea  class="form-control ckeditor"  name="product_description">{{$products->product_description}}</textarea>
                                    @if ($errors->has('product_description'))
                                        <div class="error">{{ $errors->first('product_description') }}</div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>


    </script>
@endpush
