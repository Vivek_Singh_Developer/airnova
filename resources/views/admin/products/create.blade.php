@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Add Product</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Product</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Add Product</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Add Product</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Choose Category </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <select name="category_id" class="chosen-select form-control" tabindex="-2">
                                        <option value="0"> -- Choose Category-- </option>
                                        @foreach ($productscategory as $productcategory1)
                                            <option {{ old('category_id') == $productcategory1->id ? 'selected' : '' }}
                                                value="{{ $productcategory1->id }}">{{ $productcategory1->cat_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <div class="error">{{ $errors->first('category_id') }}</div>
                                    @endif
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                   
                                </div>
                                <div class="col-sm-4 mb-2">
                                    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Code </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ old('product_code') }}"
                                        name="product_code" placeholder="" />
                                    @if ($errors->has('product_code'))
                                        <div class="error">{{ $errors->first('product_code') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Name </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ old('product_name') }}"
                                        name="product_name" placeholder="" />
                                    @if ($errors->has('product_name'))
                                        <div class="error">{{ $errors->first('product_name') }}</div>
                                    @endif
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                    
                                </div>
                                <div class="col-sm-4 mb-2">
                                    
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Price (INR)</span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ old('product_price') }}"
                                        name="product_price" placeholder="" />
                                    @if ($errors->has('product_price'))
                                        <div class="error">{{ $errors->first('product_price') }}</div>
                                    @endif
                                </div>

                               
                               
                            </div>

                         
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Thumbnail Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" value="{{ old('image') }}" name="image"
                                        placeholder="" />
                                    @if ($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>

                            </div>

                            {{-- <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Product Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" multiple name="product_image[]" />
                                    @if ($errors->has('product_image'))
                                        <div class="error">{{ $errors->first('product_image') }}</div>
                                    @endif
                                </div>

                            </div> --}}
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Product Description </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <textarea  class="form-control ckeditor"  name="product_description">{{old('product_description')}}</textarea>
                                    @if ($errors->has('product_description'))
                                        <div class="error">{{ $errors->first('product_description') }}</div>
                                    @endif
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>


    </script>
@endpush
