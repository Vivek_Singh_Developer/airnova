@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Service List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Service</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>All Services</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5></h5>
                        <div class="ibox-tools">
                            {{-- <a class="btn btn-sm btn-warning text-light" href="{{ route('admin.protype.index') }}">Product
                                Type</a>
                            <a class="btn btn-sm btn-warning text-light"
                                href="{{ route('admin.procategory.index') }}">Category List</a> --}}
                            <a class="btn btn-sm btn-dark" stype="color:white !important;" href="{{ route('admin.services.create') }}"><i
                                    class="fa fa-plus text-light"></i> Add Service</a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                       
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($products as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><img height="50px" width="50px"
                                                    src="{{ url('storage/service/' . $item->thumbnail_image) }}" /></td>
                                        
                                            <td>
                                                {{ $item->service_name }}
                                            </td>
                                           
                                           <td>
                                                @if($item->status==1)
                                                    <label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus({{$item->id}},'0')" checked="">
                                                        <span class="slider round"></span>
                                                    </label>
                                                @else
                                                    <label class="switch">
                                                        <input type="checkbox" name="status" onchange="updatestatus({{$item->id}},'1')">
                                                        <span class="slider round"></span>
                                                    </label>
                                                @endif
                                                &emsp;
                                            {{-- </td>
                                            <td> --}}
                                                <a class="btn btn-xs text-light btn-warning"
                                                    href="{{ route('admin.services.edit', $item->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                                {{-- <button class="btn btn-xs btn-dark"
                                                    onclick="deletemyTopic({{ $item->id }})"><i
                                                        class="fa fa-trash"></i></button> --}}
                                                <button  class="btn btn-xs btn-dark demotest" data-id="{{$item->id}}"><i class="fa fa-trash"></i></button>

                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').dataTable();
        });

        function deletemyTopic(id) {
            var id = id;
            var url = "{{ url('admin/services') }}" + '/' + id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                },
                url: url,
                success: function(data) {
                    if (data.status == '1') {
                        location.reload();
                    }
                    if (data.status == '0') {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Server Error!", "", "error");
                },
            });

        }

        function updatestatus(id,value)
        {
            var id= id;
            var url="{{ url('admin/services/status')}}"+'/'+id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    status:value,
                    _token: "{{ csrf_token() }}",
                },
                url: url,
                success: function (data) {
                    if(data.status=='1')
                    {
                        // location.reload();
                    }
                    if(data.status=='0')
                    {
                        // location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Error!", "", "error");
                },
            });

        }


        $(document).on('click', '.demotest', function()
        {
            event.preventDefault();
            let id = $(this).attr('data-id');
            // alert(id);
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#233dc7",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        deletemyTopic(id);
                    } else {
                        location.reload();
                    }
            });
        });

    </script>
@endpush
