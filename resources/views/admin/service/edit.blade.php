@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Edit Service</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Service</strong>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Service</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Service</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.services.update', $products->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            

                           
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Name </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{ $products->service_name }}"
                                        name="service_name" placeholder="" />
                                    @if ($errors->has('service_name'))
                                        <div class="error">{{ $errors->first('service_name') }}</div>
                                    @endif
                                </div>
                               
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Description </span></b>
                                </div>
                                <div class="col-sm-10 mb-2">
                                    <textarea type="text" class="form-control ckeditor"
                                        name="description">{{$products->description }}</textarea> 
                                    @if ($errors->has('description'))
                                        <div class="error">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                               
                            </div>
                           
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Sub Heading </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="text" class="form-control" value="{{$products->sub_heading }}"
                                        name="sub_heading" placeholder="" />
                                    @if ($errors->has('sub_heading'))
                                        <div class="error">{{ $errors->first('sub_heading') }}</div>
                                    @endif
                                </div>
                               
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Sub Heading Description </span></b>
                                </div>
                                <div class="col-sm-10 mb-2">
                                    <textarea type="text" class="form-control ckeditor"
                                        name="sub_heading_desc">{{ $products->sub_heading_desc }}</textarea> 
                                    @if ($errors->has('sub_heading_desc'))
                                        <div class="error">{{ $errors->first('sub_heading_desc') }}</div>
                                    @endif
                                </div>
                               
                            </div>
                            
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Thumbnail Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" value="{{ old('image') }}" name="image"
                                        placeholder="" />
                                    @if ($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                                @if ($products->thumbnail_image != null)
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <img height="50px" width="50px"
                                            src="{{ url('storage/service/' . $products->thumbnail_image) }}" />
                                    </div>
                                @endif
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Service Image </span></b>
                                </div>
                                <div class="col-sm-4 mb-2">
                                    <input type="file" class="form-control" multiple name="images[]" />
                                    @if ($errors->has('images'))
                                        <div class="error">{{ $errors->first('images') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">

                                </div>
                                <div class="col-sm-10 col-sm-offset-2 row">
                                    @php 
                                        $img = json_decode($products->images,true);
                                        $aaaa = is_array( $img) ? $img : [];
                                    @endphp
                                    @if (count($img) > 0)
                                        @foreach ($img as $item)
                                            @if ($item != null)
                                                <div class="col-sm-1 col-sm-offset-2 mt-4">
                                                    <img height="70px" width="70px"
                                                        src="{{ url('storage/service/' . $item) }}" />
                                                 
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>


    </script>
@endpush
