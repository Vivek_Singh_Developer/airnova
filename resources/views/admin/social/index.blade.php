@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Social List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Social</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Social</h5>
                        <div class="ibox-tools">
                            {{-- <a class="btn btn-sm btn-dark" href="{{ route('admin.socials.create') }}"><i
                                    class="fa fa-plus text-light"></i></a> --}}
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Icon</th>
                                        <th>Link</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($socials as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->icon_name }}</td>
                                            <td><i class="fa {{ $item->fa_class }}"></i></td>
                                            <td>{{ $item->link }}</td>
                                            <td>
                                                <a class="btn btn-xs text-light btn-warning"
                                                    href="{{ route('admin.socials.edit', $item->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                                {{-- <button class="btn btn-xs btn-dark"
                                                    onclick="deletemyTopic({{ $item->id }})"><i
                                                        class="fa fa-trash"></i></button> --}}
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>

        $(document).ready(function(){
            $('.dataTables-example').dataTable( {
                "columnDefs": [{ "orderable": false, "targets": [2,4] }]
            });
        });

        function deletemyTopic(id) {
            var id = id;
            var url = "{{ url('admin/socials') }}" + '/' + id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                },
                url: url,
                success: function(data) {
                    if (data.status == '1') {
                        location.reload();
                    }
                    if (data.status == '0') {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Server Error!", "", "error");
                },
            });

        }
    </script>
@endpush
