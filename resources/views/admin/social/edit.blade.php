@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Edit Social</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Social</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Social</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.socials.update', $social->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Name </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="text" class="form-control" value="{{ $social->icon_name }}"
                                        name="icon_name" placeholder="Icon Name" />
                                    @if ($errors->has('icon_name'))
                                        <div class="error">{{ $errors->first('icon_name') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Link </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="url" class="form-control" value="{{ $social->link }}" name="link"
                                        placeholder="Social Link" />
                                    @if ($errors->has('link'))
                                        <div class="error">{{ $errors->first('link') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Icon Class </span></b>
                                </div>
                                <div class="col-sm-8 mb-2">
                                    <input type="text" class="form-control" value="{{ $social->fa_class }}"
                                        name="fa_class" placeholder="Icon Class " />
                                    @if ($errors->has('fa_class'))
                                        <div class="error">{{ $errors->first('fa_class') }}</div>
                                    @endif
                                    <span>choose class from <a target="_blank"
                                            href="https://fontawesome.com/v4.7/icons/">here</a></span>
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, {
            color: '#f39f01'
        });
    </script>
@endpush
