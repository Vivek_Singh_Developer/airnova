<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('setting.AppTitle') }}</title>

    <!-- Fonts -->
    <link rel="icon" type="image/x-icon" href="{{asset('assetsweb/images/favicon.png')}}">
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
    <!-- Toastar-->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <!-- CSS -->
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <style>
        .form-control:focus {
            border-color: #f39f01;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(250, 173, 6, 0.6);
        }

        .error {
            color: #f39f01;
            font-weight: bold;
        }

        #toast-container>.toast-success {
            background-color: black ! important;
            color: white !important;
            font-weight: bold;
        }

    </style>

    @stack('style')

</head>

<body>

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">

                    <li class="nav-header">
                        <div class="logo">
                            @if ($settings->logo != null)
                                <img height="70px" width="100%" style="padding:10px;"
                                    src="{{ url('storage/' . $settings->logo) }}">
                            @else
                                <img height="70px" width="100%" style="padding:10px;"
                                    src="{{ asset('images/logo.png') }}">
                            @endif
                            <img height="70px" width="100%" style="padding:10px;"
                                src="{{ asset('images/favlogo.png') }}">
                        </div>
                    </li>

                    <li class=" @if (Request::is('admin/home*')) active @endif">
                        <a href="{{ route('admin.home') }}">
                            <i class="fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="@if (Request::is('admin/products*')  || Request::is('admin/procategory*')) active  @endif">

                        <a href="javascript:;" aria-expanded="false"><i class="fa fa-cart-arrow-down"></i>
                            <span class="nav-label">Manage Product</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a style="@if (Request::is('admin/procategory*')) color:white; @endif" href="{{ route('admin.procategory.index') }}">Category List</a></li>
                            <li><a style="@if (Request::is('admin/products*')) color:white; @endif" href="{{ route('admin.products.index') }}">Product List</a></li>
                        </ul>
                    </li>
                    <li class=" @if (Request::is('admin/services*')) active @endif">
                        <a href="{{ route('admin.services.index') }}">
                            <i class="fa fa-th-large"></i>
                            <span class="nav-label">Manage Services</span>
                        </a>
                    </li>
                    {{-- <li class="@if(Request::is('admin/app-user*') || Request::is('admin/web-user*')) active @endif ">
                        <a href="javascript:;" aria-expanded="false"><i class="fa fa-users"></i>
                            <span class="nav-label">Manage User</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a style="@if (Request::is('admin/web-user*')) color:white; @endif" href="{{ route('admin.webuser.index') }}">Website User List</a></li>
                        </ul>
                    </li> --}}

               
                    <li class="@if (Request::is('admin/feedback*')) active @endif">
                        <a href="{{ route('admin.feedback.index') }}"><i class="fa fa-comments-o"></i> <span
                                class="nav-label">Manage Contact Us</span></a>
                    </li>

                    {{-- <li class="@if (Request::is('admin/content*')) active @endif">
                        <a href="javascript:;" aria-expanded="false"><i class="fa fa-file"></i>
                            <span class="nav-label">Manage Content</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a style="@if (Request::is('admin/content')) color:white; @endif"   href="{{ route('admin.content.index') }}">Website Content</a></li>
                        </ul>
                    </li> --}}

                    <li class="@if (Request::is('admin/newsletter*')) active @endif">
                        <a href="{{ route('admin.newsletter') }}"><i class="fa fa-file-text"></i>
                            <span class="nav-label">Manage Newsletter</span></a>
                    </li>

                    <li class="@if (Request::is('admin/socials*')) active @endif">
                        <a href="{{ route('admin.socials.index') }}"><i class="fa fa-share"></i> <span
                                class="nav-label">Manage Social</span></a>
                    </li>

                    <li class="@if (Request::is('admin/setting*')) active @endif">
                        <a href="{{ route('admin.setting') }}"><i class="fa fa-cogs"></i> <span
                                class="nav-label">Manage Settings</span></a>
                    </li>

                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="light-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-warning" href="#"><i
                                class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li style="padding: 20px">
                            <span class="m-r-sm text-muted welcome-message">Welcome to
                                {{ __('setting.AppName')}}</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i>
                                <span class="label label-warning">0</span>
                            </a>

                            {{-- @if (isset($adminNotification))
                                @if (count($adminNotification) > 0)
                                    <ul class="dropdown-menu dropdown-alerts">
                                        <li>
                                            <a href="{{route('admin.clear.notification')}}" class="btn bg-primary btn-primary dropdown-item">
                                                <div class="text-center  text-light">
                                                    Clear All Notifications
                                                </div>
                                            </a>
                                        </li>
                                        @foreach ($adminNotification as $notify1)
                                            <li class="dropdown-divider"></li>
                                            <li onclick="deletenotification( {{ $notify1->id }})">
                                                <a href="javascript:;" class="dropdown-item">
                                                    <div>
                                                        <i class="fa fa-circle fa-fw"></i> {{ $notify1->title }}
                                                        <span class="float-right text-muted small" >
                                                            <i class="fa fa-times fa-fw"></i>    
                                                        </span>
                                                    </div>
                                                </a>
                                            </li>
                                            
                                        @endforeach
                                    </ul>
                                @else --}}
                                    <ul class="dropdown-menu dropdown-alerts">
                                        <li>
                                            <a href="javascript:void(0)" class="dropdown-item">
                                                <div>
                                                    Notification Not Available
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                {{-- @endif
                            @endif --}}

                        </li>
                        <li>
                            <a href="{{ route('admin.logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                                class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>

                </nav>
            </div>
            {{-- <div class="row">
                <div class="col-lg-12"> --}}
            @yield('content')
            {{-- </div>
            </div> --}}
            <div class="footer">
                <div class="float-right">
                    {!! $settings->copyright ?? '<strong>Copyright</strong> ' . __('setting.AppName') . ' &copy; ' . now()->year !!}
                </div>
             
            </div>
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <!-- Peity -->
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script>
        $(document).ready(function() {
            @if (Session::has('success'))
                setTimeout(function() {
                toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 14000,

                };
                toastr.success('{{ Session::get('success') }}');

                }, 1100);
            @endif

            @if (Session::has('failed'))
                setTimeout(function() {
                toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 14000,

                };
                toastr.danger('{{ Session::get('failed') }}');

                }, 1100);
            @endif
        });

        function deletenotification(id) {
            var id = id;
            var url = "{{ url('admin/delete/notifications') }}" + '/' + id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                },
                url: url,
                success: function(data) {
                    if (data.status == '1') {
                        location.reload();
                    }
                    if (data.status == '0') {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Server Error!", "", "error");
                },
            });

        }
    </script>

    @stack('script')

</body>

</html>
