@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Website Content List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    Content
                </li>
                <li class="breadcrumb-item active">
                    <strong>Website Content</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5></h5>
                        <div class="ibox-tools">
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Key</th>
                                        <th>Value</th>
                                        <th>Image</th>
                                        <th>Video</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($contents as $item)
                                        <tr>
                                            <td>
                                                @if ($item->key == 'about_us')
                                                    About Us
                                                @elseif ($item->key == 'our_vision')
                                                    Our Vision
                                                @elseif ($item->key == 'how_it_works')
                                                    How it Works
                                                @elseif ($item->key == 'refer')
                                                    Refer
                                                @elseif ($item->key == 'weekly_banner')
                                                    Weekly Banner 1
                                                @elseif ($item->key == 'daily_banner')
                                                    Daily Banner 1
                                                @elseif ($item->key == 'weekly_banner_detail')
                                                    Weekly Banner 2
                                                @elseif ($item->key == 'daily_banner_detail')
                                                    Daily Banner 2
                                                @endif
                                            <td>{{ \Illuminate\Support\Str::limit($item->Value, 60) }}</td>
                                            <td>
                                                @if ($item->image != null)
                                                    <img height="50px" width="50px"
                                                        src="{{ url('storage/content/' . $item->image) }}" />
                                                @else
                                                    Not Available
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->video != null)
                                                    <div style="width:300px;height:200px;">
                                                        <video width="100" controls>
                                                            <source src="{{ url('storage/content/' . $item->video) }}"
                                                                type="video/mp4">
                                                        </video>
                                                    </div>
                                                @else
                                                    Not Available
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-xs text-light btn-warning"
                                                    href="{{ route('admin.content.edit', $item->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>
        // $(document).ready(function() {
        //     $('.dataTables-example').DataTable();
        // });
    </script>
@endpush
