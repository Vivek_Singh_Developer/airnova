@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            @if($contents->type=='Website')
            <h2>Edit Website Content</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    Content
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Website Content</strong>
                </li>
            </ol>
            @else
            <h2>Edit Application Content</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item">
                    Content
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Application Content</strong>
                </li>
            </ol>
            @endif

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Content</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="{{ route('admin.content.update', $contents->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" class="form-control" name="key" value="{{ $contents->key }}" />
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Key </span></b>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    @if ($contents->key == 'about_us')
                                        <input type="text" class="form-control" disabled value="About Us" />
                                    @elseif ($contents->key == 'how_it_works')
                                        <input type="text" class="form-control" disabled value="How it Works" />
                                    @elseif ($contents->key == 'our_vision')
                                        <input type="text" class="form-control" disabled value="Our Vision" />
                                    @elseif ($contents->key == 'refer')
                                        <input type="text" class="form-control" disabled value="Refer" />
                                    @elseif ($contents->key == 'weekly_banner')
                                        <input type="text" class="form-control" disabled value="Weekly Banner 1" />
                                    @elseif ($contents->key == 'daily_banner')
                                        <input type="text" class="form-control" disabled value="Daily Banner 1" />
                                    @elseif ($contents->key == 'weekly_banner_detail')
                                        <input type="text" class="form-control" disabled value="Weekly Banner 2" />
                                    @elseif ($contents->key == 'daily_banner_detail')
                                        <input type="text" class="form-control" disabled value="Daily Banner 2" />
                                    @elseif ($contents->key == 'active_banner')
                                        <input type="text" class="form-control" disabled value="Active Banner" />
                                    @elseif ($contents->key == 'help_english')
                                         <input type="text" class="form-control" disabled value="Help & Support (English)"/>
                                    @elseif ($contents->key == 'help_hindi')
                                        <input type="text" class="form-control" disabled value="Help & Support (Hindi)"/>
                                    @elseif ($contents->key == 'privacy_english')
                                         <input type="text" class="form-control" disabled value="Privacy & Security (English)"/>
                                    @elseif ($contents->key == 'privacy_hindi')
                                         <input type="text" class="form-control" disabled value="Privacy & Security (Hindi)"/>
                                    @elseif ($contents->key == 'about_us_english')
                                         <input type="text" class="form-control" disabled value="About Us (English)"/>
                                    @elseif ($contents->key == 'about_us_hindi')
                                        <input type="text" class="form-control" disabled value=" About Us (Hindi)"/>
                                     @elseif ($contents->key == 'term_condition_english')
                                         <input type="text" class="form-control" disabled value="Term & Condition(English)"/>
                                    @elseif ($contents->key == 'term_condition_hindi')
                                        <input type="text" class="form-control" disabled value="  Term & Condition (Hindi)"/>
                                    @elseif ($contents->key == 'leaderboard_banner')
                                        <input type="text" class="form-control" disabled value="Leaderboard Banner"/>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Value </span></b>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <textarea type="text" class="form-control @if($contents->type=='Website') summernote @endif"
                                        name="Value">{{ $contents->Value }}</textarea>
                                    @if ($errors->has('Value'))
                                        <div class="error">{{ $errors->first('Value') }}</div>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">

                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>Image </span></b>
                                </div>
                                <div class="col-sm-6 mb-2">
                                    <input type="file" class="form-control" accept=".png,.jpg,.jpeg" name="image" placeholder="" />
                                    @if ($errors->has('image'))
                                        <div class="error">{{ $errors->first('image') }}</div>
                                    @endif
                                    @if ($contents->type=='Website')
                                        <p>height: 300px and Width:500px</p>
                                    @else
                                        <p>width: 1000px and height:500px</p>
                                    @endif
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                    @if ($contents->image != null)
                                        <img height="50px" width="50px"
                                            src="{{ url('storage/content/' . $contents->image) }}" />
                                        <a class="btn btn-xs text-light btn-warning"
                                            href="{{ route('admin.content.image.delete', $contents->id) }}">Remove
                                            Image</a>
                                    @endif
                                </div>

                            </div>
                            @if($contents->key == 'about_us' || $contents->key =='our_vision' || $contents->key =='refer' || $contents->key =='how_it_works')
                                <div class="form-group row">

                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>Video </span></b>
                                    </div>
                                    <div class="col-sm-6 mb-2">
                                        <input type="file" class="form-control" accept=".mp4" name="video" placeholder="" />
                                        @if ($errors->has('video'))
                                            <div class="error">{{ $errors->first('video') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-2 col-sm-offset-2">
                                        @if ($contents->video != null)
                                            <video width="400" controls>
                                                <source src="{{ url('storage/content/' . $contents->video) }}"
                                                    type="video/mp4">
                                            </video>
                                            <a class="btn btn-xs text-light btn-warning"
                                                href="{{ route('admin.content.video.delete', $contents->id) }}">Remove
                                                Video</a>

                                        @endif
                                    </div>

                                </div>
                            @endif

                            <div class="form-group row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
  <!-- SUMMERNOTE -->
    <script src="{{ asset('js/plugins/summernote/summernote-bs4.js') }}"></script>

    <script>
        $(document).ready(function(){

            $('.summernote').summernote();

       });
    </script>
@endpush
