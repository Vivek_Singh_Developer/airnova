@extends('admin.layouts.admin')

@section('content')
    <style>
            .full-featured-box a {
            display: block;
            }
            .icon-box img {
                    display: block;
                    padding: 40px 0px;
                    width: 130px;
                    height: 130px;
                    margin: 0 auto;
        }
            .icon-box button.mybtn {
                width: 100%;
                border: none;
                color: #000;
                padding: 12px 5px;
                border-radius: 0px 0px 5px 5px;
                cursor: pointer;
            }
                button.mybtn.red {
                    background: #ffaeb9;
                }
                .feature-box {
            background: #fff;
            border-radius: 5px;
                }
                button.mybtn.gold {
                background: #dec465;
            }
            button.mybtn.orange {
                background: #edaf4b;
            }
            button.mybtn.blue {
            background: #869be7;
        }
        .full-featured-box {
            margin-bottom: 30px;
        }
        button.mybtn.pink {
            background: #fbaef9;
        }
        button.mybtn.task {
            background: #65ded8;
        }
        button.mybtn.Community {
            background: #ffd9ad;
        }
        button.mybtn.Feedback {
            background: #b9ffb9;
        }
        button.mybtn.Content {
            background: #dcb6ff;
        }
        button.mybtn.Newsletter {
            background: #feffae;
        }
        button.mybtn.Social {
            background: #72ffff;
        }
        button.mybtn.Settings {
            background: #deff8e;
        }

    </style>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
    
        </div>
    </div>

    
    <div class="wrapper wrapper-content animated fadeInRight">
        

        

        <div class="row">


            <div class="col-lg-4">
                <div class="full-featured-box">
                    <a href="{{ route('admin.feedback.index') }}">
                        <div class="feature-box">
                            <div class="icon-box">
                                <img src="{{asset('images/p-feedback.svg')}}">
                                <button type="button" class="mybtn Feedback">Manage Contact us</button>
                            </div>
                        </div>
                    </a> 
                </div>
            </div>

            

            <div class="col-lg-4">
                <div class="full-featured-box">
                    <a href="{{ route('admin.newsletter') }}">
                        <div class="feature-box">
                            <div class="icon-box">
                                <img src="{{asset('images/p-newsletter.svg')}}">
                                <button type="button" class="mybtn Newsletter">Manage Newsletter</button>
                            </div>
                        </div>
                    </a> 
                </div>
            </div>
            <div class="col-lg-4">
                <div class="full-featured-box">
                    <a href="{{ route('admin.socials.index') }}">
                        <div class="feature-box">
                            <div class="icon-box">
                                <img src="{{asset('images/p-social.svg')}}">
                                <button type="button" class="mybtn Social">Manage Social</button>
                            </div>
                        </div>
                    </a> 
                </div>
            </div>

            <div class="col-lg-4">
                <div class="full-featured-box">
                    <a href="{{ route('admin.setting') }}">
                        <div class="feature-box">
                            <div class="icon-box">
                                <img src="{{asset('images/p-setting.svg')}}">
                                <button type="button" class="mybtn Settings">Manage Settings</button>
                            </div>
                        </div>
                    </a> 
                </div>
            </div>



        </div>
             
       
                
    </div>
    
@endsection
