@extends('admin.auth.layouts.auth')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            @if(isset($settings) && $settings->logo !=null)
                <h1 class="logo-name"> <img height="88px" width="220px" style="background-color:black;padding:10px;" src="{{ url('storage/'.$settings->logo) }}"></h1>
            @else
                <h1 class="logo-name"><img height="88px" width="220px" style="background-color:black;padding:10px;" src="{{ asset('images/logo.png') }}"></h1>
            @endif
        </div>
        @error('login')
        <div class="alert alert-danger text-left alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ $message }}
        </div>
        @enderror 
        <h3>{{ __('setting.AdminLogin') }}</h3>
        <p></p>
        <p></p>
        <form class="m-t text-left" role="form" method="POST" action="{{ route('admin.login.submit') }}">
            @csrf
            
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required autofocus>
                @error('email')
                    *<small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                {{-- <div class="input-group">
                    <input type="password" class="form-control" name="old_password" id="old_password"  placeholder="Old Password" />
                    <span class="input-group-prepend"> 
                        <button onclick="myFunction1('old_password')" type="button" class="btn btn-dark"><i id="oldpass" class="fa fa-eye-slash"></i></button>
                    </span>
                </div>
                @if($errors->has('old_password'))
                    <div class="error">{{ $errors->first('old_password') }}</div>
                @endif --}}

                <div class="input-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
                    <span class="input-group-append"> 
                        <button onclick="showPassword('password')" type="button" class="btn btn-dark">
                            <i id="showPasswordIcon" class="fa fa-eye-slash"></i>
                        </button> 
                    </span>
                </div>
                    @error('password')
                        *<small class="text-danger">{{ $message }}</small>
                    @enderror
                
            </div>
            <button type="submit" class="btn btn-dark block full-width m-b">Login</button>
                <label class="pull-left"> 
                    <input type="checkbox" class="i-checks" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me </label>
            <br>
            <br>
        </form>           
        <p class="m-t"> <small> {!! $settings->copyright ?? "<strong>Copyright</strong> " . __('setting.AppName') . ' &copy; ' . now()->year !!}</small> </p>
    </div>
</div>
@endsection

<script>
    function showPassword(pass)
    {
        let el = $('#showPasswordIcon');

        if(el.hasClass('fa-eye-slash'))
        {
            el.removeClass('fa-eye-slash');
            el.addClass('fa-eye');
            $('#password').attr('type', 'text');
        }
        else
        {
            el.removeClass('fa-eye');
            el.addClass('fa-eye-slash');
            $('#password').attr('type', 'password');
        }
    }
</script>
