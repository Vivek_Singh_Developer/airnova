@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content') 
<div class="mprofile-full-box">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Feedback Details</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Feedback Details</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart">

                                    <div class="row">
                                        <div class="col-md-2"><b>Name </b></div>
                                        <div class="col-md-10"> {{ $feedback->name}}</div>
                                    
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Email  </b></div>
                                        <div class="col-md-10"> {{ $feedback->email?$feedback->email:'N/A'}}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2"><b>Mobile  </b></div>
                                        <div class="col-md-10"> {{ $feedback->mobile?$feedback->mobile:'N/A'}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2"><b>Message </b></div>
                                        <div class="col-md-10"> {{ $feedback->message}}</div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-md-2"><b>Date </b></div>
                                        <div class="col-md-10"> {{ Carbon\Carbon::parse($feedback->created_at)->format('d M Y')}}</div>
                                    </div>                              
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        @if($feedback->email!=null)
            {{-- <div class="row">
                <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5></h5>
                            </div>
                            <div class="ibox-content">
                                <form action="{{route('admin.feedback.reply')}}" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <b><span>Reply </span></b>
                                        </div>
                                        <div class="col-sm-8 mb-2">
                                            <input type="hidden" class="form-control" name="id" value="{{ $feedback->id}}">
                                            <textarea  class="form-control" name="reply" >{{ $feedback->reply}}</textarea>
                                            @if($errors->has('reply'))
                                                <div class="error">{{ $errors->first('reply') }}</div>
                                            @endif
                                        </div>
                                                                                
                                    </div>
                            
                                    <div class="form-group row">
                                        <div class="col-sm-3 col-sm-offset-2">
                                            <button class="btn btn-primary btn-md" type="submit">Reply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div> --}}
        @endif
    </div>
</div>
@endsection

