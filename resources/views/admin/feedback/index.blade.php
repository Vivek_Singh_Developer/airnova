@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contact Us List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Contact Us</strong>
                </li>
            </ol>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Contact Us</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Message</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($feedback as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email?$item->email:'N/A' }}</td>
                                            <td>{{ $item->mobile?$item->mobile:'N/A' }}</td>
                                            <td>{{ \Illuminate\Support\Str::limit($item->message, 50, $end='...') }}</td>
                                            <td>{{ Carbon\Carbon::parse($item->created_at)->format('d M Y') }}</td>
                                            <td>
                                                {{-- <a class="btn btn-xs text-light btn-dark"
                                                    href="{{ route('admin.feedback.delete', $item->id) }}"><i
                                                        class="fa fa-trash"></i></a> --}}
                                                <a class="btn btn-xs text-light btn-warning"
                                                        href="{{ route('admin.feedback.show', $item->id) }}"><i
                                                            class="fa fa-eye"></i></a>
                                                <button style="margin:0 3px;"  class="btn btn-xs btn-dark demotest" title="View"  data-id="{{$item->id}}"><i class="fa fa-trash"></i></button> 
                               
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script>

        $(document).ready(function(){
            $('.dataTables-example').dataTable( {
                dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons:[
                            {
                                    extend: 'excel',
                                    footer: false,
                                    className: 'btn btn-primary',
                                    init: function(api, node, config) {
                                        $(node).removeClass('btn-default')
                                    },
                            },        
                        ]
            });
        });
        
        function mydeleteproduct(id) 
        {
            var id= id;
            var url="{{ url('admin/feedback/delete/')}}"+'/'+id;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                    _method: 'DELETE'
                },
                url: url,
                success: function (data) {
                    if(data.status=='1')
                    {
                        location.reload();
                    }
                    if(data.status=='0')
                    {
                        location.reload();
                    }
                },
                error: (err) => {
                    console.log(err);
                    swal("Error!", "", "error");
                },
            });
        
        }

        $(document).on('click', '.demotest', function() 
        {
            event.preventDefault();
            let id = $(this).attr('data-id');
            // alert(id);
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#233dc7",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        mydeleteproduct(id);
                    } else {
                        location.reload();
                    }
            });
        });

    </script>
@endpush
