@extends('admin.layouts.admin')

@push('style')
    <link href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Settings</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Settings</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Admin Profile</h5>
                        <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        
                            <form action="{{ route('admin.setting.email') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>Admin Email </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $admin_data->id}}" name="id"/>
                                        <input type="text" class="form-control" name="email"  value="{{ $admin_data->email}}" />
                                        @if($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                            <form action="{{ route('admin.setting.password') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>Admin Password </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <div class="input-group">
                                            <input type="hidden" value="{{ $admin_data->id}}" name="id"/>
                                            <span class="input-group-prepend"> 
                                                <button onclick="myFunction1('old_password')" type="button" class="btn btn-dark"><i id="oldpass" class="fa fa-eye-slash"></i></button>
                                            </span>
                                            <input type="password" class="form-control" name="old_password" id="old_password"  placeholder="Old Password" />
                                        </div>
                                        @if($errors->has('old_password'))
                                            <div class="error">{{ $errors->first('old_password') }}</div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2"></div>
                                    <div class="col-sm-7 mb-2">
                                        <div class="input-group">
                                            <span class="input-group-prepend"> 
                                                <button onclick="myFunction2('password')" type="button" class="btn btn-dark"><i id="pass" class="fa fa-eye-slash"></i></button>
                                            </span>
                                            <input type="password" class="form-control" name="password"  id="password" placeholder="New Password" /> 
                                        </div>
                                        @if($errors->has('password'))
                                            <div class="error">{{ $errors->first('password') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>

                                {{-- <div class="form-group row"> --}}
                                    {{-- <div class="col-sm-2 col-sm-offset-2"></div>
                                    <div class="col-sm-7 mb-2">
                                        <div class="input-group">
                                            <span class="input-group-prepend"> 
                                                <button onclick="myFunction3('password-confirm')" type="button" class="btn btn-dark"><i id="conpass" class="fa fa-eye-slash"></i></button>
                                            </span>
                                            <input id="password-confirm" type="password" class="form-control form-control-lg round-40"
                                            name="password_confirmation" placeholder="Confirm Password">
                                        </div>
                                    </div> --}}
                                    
                                {{-- </div> --}}
                            </form>
                    </div>
                </div>
            </div> 
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Website & App Settings</h5>
                    </div>
                    <div class="ibox-content">

                        <form action="{{ route('admin.setting.appname') }}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <b><span>App Name </span></b>
                                </div>
                                <div class="col-sm-7 mb-2">
                                    <input type="hidden" value="{{ $name->id}}" name="id"/>
                                    <input type="hidden" value="{{ $name->key}}" name="key"/>
                                    <input type="text" class="form-control" name="appname"  value="{{ $name->value}}" />
                                    @if($errors->has('appname'))
                                        <div class="error">{{ $errors->first('appname') }}</div>
                                    @endif
                                </div>
                                <div class="col-sm-3 col-sm-offset-2">
                                    <button class="btn btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>


                        <form action="{{ route('admin.setting.copyright') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>Copyright </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $copyright->id}}" name="id"/>
                                        <input type="hidden" value="{{ $copyright->key}}" name="key"/>
                                        <input type="text" class="form-control" name="copyright"  value="{{ $copyright->value}}" />
                                        @if($errors->has('copyright'))
                                            <div class="error">{{ $errors->first('copyright') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                        </form>

                        <form action="{{ route('admin.setting.logo') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>Logo </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $logo->id}}" name="id"/>
                                        <input type="hidden" value="{{ $logo->key}}" name="key"/>
                                        <input type="file" class="form-control" name="logo"  value="{{ $logo->value}}" />
                                        @if($errors->has('logo'))
                                        <div class="error">{{ $errors->first('logo') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        @if ($logo->value)
                                        <img src="{{  URL::to('storage/'.$logo->value) }}" id="img-uploaded" height="80px" width="220px" class="avatar-xl me-3" alt="Logo Image">
                                    @else 
                                        <img src="{{ asset('images/logo.png') }}" id="img-uploaded" height="80px" width="120px" style="background-color:#2f4050;padding:10px;" class="avatar-xl me-3" alt="Logo Image" />
                                    @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                    </div>
                                </div>
                        </form>
                            
                        <form action="{{ route('admin.setting.app_address') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>App Address  </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $app_address->id}}" name="id"/>
                                        <input type="hidden" value="{{ $app_address->key}}" name="key"/>
                                        <input type="text" class="form-control" name="app_address"  value="{{ $app_address->value}}" />
                                        @if($errors->has('app_address'))
                                            <div class="error">{{ $errors->first('app_address') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                        </form>

                        <form action="{{ route('admin.setting.app_email') }}" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>App Email  </span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $app_email->id}}" name="id"/>
                                        <input type="hidden" value="{{ $app_email->key}}" name="key"/>
                                        <input type="text" class="form-control" name="app_email"  value="{{ $app_email->value}}" />
                                        @if($errors->has('app_email'))
                                            <div class="error">{{ $errors->first('app_email') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                        </form>

                        <form action="{{ route('admin.setting.app_mobile') }}" method="POST">
                            @csrf
                                <div class="form-group row">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <b><span>App Contact</span></b>
                                    </div>
                                    <div class="col-sm-7 mb-2">
                                        <input type="hidden" value="{{ $app_mobile->id}}" name="id"/>
                                        <input type="hidden" value="{{ $app_mobile->key}}" name="key"/>
                                        <input type="text" class="form-control" name="app_mobile"  value="{{ $app_mobile->value}}" />
                                        @if($errors->has('app_mobile'))
                                        <div class="error">{{ $errors->first('app_mobile') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 col-sm-offset-2">
                                        <button class="btn btn-dark btn-md" type="submit">Save</button>
                                    </div>
                                </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        
    </div>
   
@endsection

@push('script')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        function myFunction1(id)
        {
            let el = $('#oldpass');

            if(el.hasClass('fa-eye-slash'))
            {
                el.removeClass('fa-eye-slash');
                el.addClass('fa-eye');
                $('#'+id).attr('type', 'text');
            }
            else
            {
                el.removeClass('fa-eye');
                el.addClass('fa-eye-slash');
                $('#'+id).attr('type', 'password');
            }
        }
        function myFunction2(id)
        {
            let el = $('#pass');

            if(el.hasClass('fa-eye-slash'))
            {
                el.removeClass('fa-eye-slash');
                el.addClass('fa-eye');
                $('#'+id).attr('type', 'text');
            }
            else
            {
                el.removeClass('fa-eye');
                el.addClass('fa-eye-slash');
                $('#'+id).attr('type', 'password');
            }
        }
        function myFunction3(id)
        {
            let el = $('#conpass');

            if(el.hasClass('fa-eye-slash'))
            {
                el.removeClass('fa-eye-slash');
                el.addClass('fa-eye');
                $('#'+id).attr('type', 'text');
            }
            else
            {
                el.removeClass('fa-eye');
                el.addClass('fa-eye-slash');
                $('#'+id).attr('type', 'password');
            }
        }
    </script>
@endpush