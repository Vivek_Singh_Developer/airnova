<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <title>{{ __('setting.AppTitle') }}</title>

    <!-- Fav Icon -->
    <link rel="icon" href="{{ asset('assetsweb/images/logo.png') }}" type="image/x-icon">



    <!-- Stylesheets -->
    <link href="{{ asset('assetsweb/css/font-awesome-all.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/owl.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/jquery.fancybox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/color.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/global.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/elpath.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/nice-select.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/woocommerce.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assetsweb/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
</head>


<!-- page wrapper -->

<body>
    <style>
        ol li {
            display: list-item;
            list-style-position: outside;
            list-style: inside;
            margin-left: 15px;
        }
    </style>
    <div class="boxed_wrapper">

        <!-- mouse-pointer -->
        <div class="mouse-pointer" id="mouse-pointer">
            <div class="icon"><i class="far fa-angle-left"></i><i class="far fa-angle-right"></i></div>
        </div>
        <!-- mouse-pointer end -->


        <!-- preloader -->
        {{-- <div class="loader-wrap">
                <div class="preloader">
                    <div class="preloader-close">x</div>
                    <div id="handle-preloader" class="handle-preloader">
                        <div class="animation-preloader">
                            <div class="spinner"></div>
                            <div class="txt-loading">
                                <span data-text-preloader="a" class="letters-loading">
                                    a
                                </span>
                                <span data-text-preloader="i" class="letters-loading">
                                    i
                                </span>
                                <span data-text-preloader="r" class="letters-loading">
                                    r
                                </span>
                                <span data-text-preloader="n" class="letters-loading">
                                    n
                                </span>
                                <span data-text-preloader="o" class="letters-loading">
                                    o
                                </span>
                                <span data-text-preloader="v" class="letters-loading">
                                    v
                                </span>
                                <span data-text-preloader="a " class="letters-loading">
                                    a
                                </span>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <span data-text-preloader="s" class="letters-loading">
                                    s
                                </span>
                                <span data-text-preloader="y" class="letters-loading">
                                    y
                                </span>
                                <span data-text-preloader="s" class="letters-loading">
                                    s
                                </span>
                                <span data-text-preloader="t" class="letters-loading">
                                    t
                                </span>
                                <span data-text-preloader="e" class="letters-loading">
                                    e
                                </span>
                                <span data-text-preloader="m" class="letters-loading">
                                    m
                                </span>
                                <br>

                                <span data-text-preloader="p" class="letters-loading">
                                    p
                                </span>
                                <span data-text-preloader="r" class="letters-loading">
                                    r
                                </span>
                                <span data-text-preloader="i" class="letters-loading">
                                    i
                                </span>
                                <span data-text-preloader="v" class="letters-loading">
                                    v
                                </span>
                                <span data-text-preloader="a" class="letters-loading">
                                    a
                                </span>
                                <span data-text-preloader="t" class="letters-loading">
                                    t
                                </span>
                                <span data-text-preloader="e" class="letters-loading">
                                    e
                                </span>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <span data-text-preloader="l" class="letters-loading">
                                    l
                                </span>
                                <span data-text-preloader="i" class="letters-loading">
                                    i
                                </span>
                                <span data-text-preloader="m" class="letters-loading">
                                    m
                                </span>
                                <span data-text-preloader="i" class="letters-loading">
                                    i
                                </span>
                                <span data-text-preloader="t" class="letters-loading">
                                    t
                                </span>
                                <span data-text-preloader="e" class="letters-loading">
                                    e
                                </span>
                                <span data-text-preloader="d" class="letters-loading">
                                    d
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        <!-- preloader end -->


        <!--Search Popup-->
        <div id="search-popup" class="search-popup">
            <div class="popup-inner">
                <div class="upper-box clearfix">
                    <figure class="logo-box pull-left"><a href="index.html">
                            <img src="{{ asset('assetsweb/images/logo.png') }}" alt=""></a></figure>
                    <div class="close-search pull-right"><span class="icon-179"></span></div>
                </div>
                <div class="overlay-layer"></div>
                <div class="auto-container">
                    <div class="search-form">
                        <form method="post" action="index.html">
                            <div class="form-group">
                                <fieldset>
                                    <input type="search" class="form-control" name="search-input" value=""
                                        placeholder="Type your keyword and hit" required>
                                    <button type="submit"><i class="icon-1"></i></button>
                                </fieldset>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- main header -->
        <header class="main-header">
            <!-- header-top-one -->
            <div class="header-top-one p_relative d_block">
                <div class="auto-container">
                    <div class="top-inner clearfix p_relative">
                        <div class="shape p_absolute t_0"
                            style="background-image: asset(assetsweb/images/shape/shape-1.png);"></div>
                        <div class="top-left pull-left">
                            <ul class="social-links clearfix">
                                <li class="p_relative d_iblock fs_16 float_left mr_25 lh_55">Follow Us:</li>
                                @forelse ($socialsdata as $social)
                                    @if ($social->link)
                                        <li class="p_relative d_iblock fs_16 float_left mr_25 lh_55">
                                            <a href="{!! $social->link !!}" class="p_relative d_iblock fs_16">
                                                <i class="fab {!! $social->fa_class !!}"></i>
                                            </a>
                                        </li>
                                    @endif
                                @empty
                                @endforelse

                            </ul>
                        </div>
                        <div class="top-right pull-right">
                            <ul class="info clearfix">
                                {{-- <li class="search-box-outer search-toggler p_relative d_iblock float_left mr_60 lh_55">
                                    <i class="icon-1"></i>
                                </li> --}}
                                <li class="p_relative d_iblock float_left mr_60 lh_55 pl_25 fs_16">
                                    <i class="icon-2"></i>
                                    <p>Call: <a href="tel:91{{ $settings->app_mobile }}">+91
                                            {{ $settings->app_mobile }}</a></p>
                                </li>
                                <li class="p_relative d_iblock float_left lh_55 pl_25 fs_16">
                                    <i class="icon-3"></i>
                                    <p>Email: <a
                                            href="mailto:{{ $settings->app_email }}">{{ $settings->app_email }}</a>
                                    </p>
                                </li>

                                <li class="p_relative d_iblock float_left lh_55 pl_25 fs_16">
                                   
                                    <p><a
                                            href="#"><b>News & Media</b></a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-lower -->
            <div class="header-lower">
                <div class="auto-container">
                    <div class="outer-box">
                        <div class="logo-box">
                            <figure class="logo">
                                <a href="{{ route('/') }}">
                                    @if ($settings->logo != null)
                                        <img style="max-width: 110px;" src="{{ url('storage/' . $settings->logo) }}">
                                    @else
                                        <img style="max-width: 110px;"src="{{ asset('assetsweb/images/logo.png') }}">
                                    @endif
                                </a>
                            </figure>
                        </div>
                        <div class="menu-area clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler">
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                            </div>
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix home-menu">

                                        {{-- <li class="current dropdown">
                                            <a href="index.html">Home</a>                                         
                                        </li> --}}
                                        <li class="@if (Request::is('about-us') ||
                                            Request::is('who-we-are') ||
                                            Request::is('our-values') ||
                                            Request::is('our-history') ||
                                            Request::is('board-of-director') ||
                                            Request::is('management-team')) current @endif dropdown">
                                            <a href="javascript:;">About Us</a>
                                            <ul>
                                                <li>
                                                    <a @if (Request::is('who-we-are')) style="color:#f42550;" @endif
                                                        href="{{ route('who_we_are') }}">Who We Are</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('our-values')) style="color:#f42550;" @endif
                                                        href="{{ route('our_values') }}">Our Value</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('our-history')) style="color:#f42550;" @endif
                                                        href="{{ route('our_history') }}">Our History</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('board-of-director')) style="color:#f42550;" @endif
                                                        href="{{ route('board_of_director') }}">Board of
                                                        Director</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('management-team')) style="color:#f42550;" @endif
                                                        href="{{ route('management_team') }}">Management Team</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="javascript:;">Services</a>
                                            <div class="megamenu">
                                                <div class="row clearfix">
                                                    @forelse ($services as $item)
                                                        <div class="col-lg-4 column">
                                                            <ul>
                                                                <li><a
                                                                        href="{{ route('service_by_id', $item->id) }}">{{ $item->service_name }}</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    @empty
                                                    @endforelse

                                                </div>
                                            </div>
                                        </li>
                                        <li class=" dropdown">
                                            <a href="javascript:;">Products</a>
                                            <ul>
                                                @forelse ($categories as $item)
                                                    <li>
                                                        <a
                                                            href="{{ route('product_by_id', $item->id) }}">{{ $item->cat_name }}</a>
                                                    </li>
                                                @empty
                                                @endforelse

                                            </ul>
                                        </li>



                                        <li class="@if (Request::is('about-us') ||
                                            Request::is('who-we-are') ||
                                            Request::is('our-values') ||
                                            Request::is('our-history') ||
                                            Request::is('board-of-director') ||
                                            Request::is('management-team')) current @endif dropdown">
                                            <a href="javascript:;">Corporate & Business</a>
                                            <ul>
                                                <li>
                                                    <a @if (Request::is('who-we-are')) style="color:#f42550;" @endif
                                                        href="{{ route('who_we_are') }}">Profile</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('our-values')) style="color:#f42550;" @endif
                                                        href="{{ route('our_values') }}">Business</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('our-history')) style="color:#f42550;" @endif
                                                        href="{{ route('our_history') }}">Research & Development</a>
                                                </li>
                                                <li>
                                                    <a @if (Request::is('board-of-director')) style="color:#f42550;" @endif
                                                        href="{{ route('board_of_director') }}">Integrated Management System Policy </a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li class=""><a
                                                @if (Request::is('contact-us')) style="color:#f42550;" @endif
                                                href="{{ route('contactus') }}">Contact Us</a></li>
                                        <li class="">
                                            <a @if (Request::is('career')) style="color:#f42550;" @endif
                                                href="{{ route('career') }}">Career</a></li>

                                         
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="btn-box">
                            <a href="{{ route('contactus') }}" class="theme-btn theme-btn-one">Get A Quote<i
                                    class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <!--sticky Header-->
            <div class="sticky-header">
                <div class="auto-container">
                    <div class="outer-box">
                        <div class="logo-box">
                            <figure class="logo">
                                <a href="{{ route('/') }}">
                                    @if ($settings->logo != null)
                                        <img style="max-width: 110px;"src="{{ url('storage/' . $settings->logo) }}">
                                    @else
                                        <img style="max-width: 110px;"src="{{ asset('assetsweb/images/logo.png') }}">
                                    @endif
                                </a>
                            </figure>
                        </div>
                        <div class="menu-area clearfix">
                            <nav class="main-menu clearfix">
                                <!--Keep This Empty / Menu will come through Javascript-->
                            </nav>
                        </div>
                        <div class="btn-box">
                            <a href="{{ route('contactus') }}" class="theme-btn theme-btn-one">Get A Quote<i
                                    class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>

            <nav class="menu-box">
                <div class="nav-logo">
                    <a href="{{ route('/') }}">
                        @if ($settings->logo != null)
                            <img style="max-width: 110px;"src="{{ url('storage/' . $settings->logo) }}">
                        @else
                            <img style="max-width: 110px;"src="{{ asset('assetsweb/images/logo.png') }}">
                        @endif
                    </a>
                </div>
                <div class="menu-outer">
                    <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
                </div>
                <div class="contact-info">
                    <h4>Contact Info</h4>
                    <ul>
                        <li>Chicago 12, Melborne City, USA</li>
                        <li><a href="tel:+8801682648101">+88 01682648101</a></li>
                        <li><a href="mailto:info@example.com">info@example.com</a></li>
                    </ul>
                </div>
                <div class="social-links">
                    <ul class="clearfix">
                        @forelse ($socialsdata as $social)
                            @if ($social->link)
                                <li><a href="{!! $social->link !!}"><span
                                            class="fab {!! $social->fa_class !!}"></span></a></li>
                            @endif
                        @empty
                        @endforelse
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->


        @yield('content')


        <!-- footer-one -->
        <footer class="footer-one">
            <div class="pattern-layer">
                <div class="pattern-1 hero-shape-three"></div>
                <div class="pattern-2" style="background-image: asset(assetsweb/images/shape/shape-28.png);"></div>
                <div class="pattern-3 hero-shape-three"></div>
                <div class="pattern-4" style="background-image: asset(assetsweb/images/shape/shape-30.png);"></div>
            </div>
            <div class="footer-top">
                <div class="auto-container">
                    <div class="top-inner clearfix">
                        <figure class="footer-logo pull-left">
                            <a href="{{ route('/') }}">
                                @if ($settings->logo != null)
                                    <img style="max-width: 110px;" src="{{ url('storage/' . $settings->logo) }}">
                                @else
                                    <img style="max-width: 110px;" src="{{ asset('assetsweb/images/logo.png') }}">
                                @endif
                            </a>
                        </figure>
                        <ul class="social-links pull-right clearfix">
                            @forelse ($socialsdata as $social)
                                @if ($social->link)
                                    <li><a href="{!! $social->link !!}"><i
                                                class="fab {!! $social->fa_class !!}"></i></a></li>
                                @endif
                            @empty
                            @endforelse

                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-widget-section">
                <div class="auto-container">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-6 col-sm-12 footer-column">
                            <div class="footer-widget about-widget wow fadeInUp animated" data-wow-delay="00ms"
                                data-wow-duration="1500ms">
                                <div class="widget-title">
                                    <h4>About</h4>
                                </div>
                                <div class="text">
                                    <p>Our biggest strength lies in our people and their zeal to continuously improve upon our own products. We are one of the few companies in the World, those compete with their own products and set standards to supersede by themselves.</p>
                                </div>
                                <div class="subscribe-inner">
                                    <form action="{{ route('newsletter.store') }}" method="post"
                                        class="subscribe-form">
                                        @csrf
                                        <div class="form-group">
                                            <input type="email" name="nemail" placeholder="Your email address">
                                            @if ($errors->has('nemail'))
                                                <div class="error text-danger">{{ $errors->first('nemail') }}
                                                </div>
                                            @endif
                                            <button type="submit"><i class="icon-4"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="footer-widget links-widget ml_80 wow fadeInUp animated" data-wow-delay="200ms"
                                data-wow-duration="1500ms">
                                <div class="widget-title">
                                    <h4>Links</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="links-list clearfix">
                                        <li><a href="{{ route('aboutus') }}">About</a></li>
                                        <li><a href="{{ route('termcondition') }}">Terms & conditions</a></li>
                                        <li><a href="{{ route('privacypolicy') }}">Privacy Policy</a></li>
                                        <li><a href="{{ route('contactus') }}">Contact us</a></li>
                                        <li><a href="{{ route('career') }}">Career</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-12 footer-column">
                            <div class="footer-widget links-widget wow fadeInUp animated" data-wow-delay="400ms"
                                data-wow-duration="1500ms">
                                <div class="widget-title">
                                    <h4>Services</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="links-list clearfix">
                                        @php $count = 0; @endphp
                                        @forelse ($services as $service)
                                            @if ($count==5)
                                                @break
                                            @endif
                                            <li><a href="{{ route('service_by_id', $service->id) }}">{{ $service->service_name }}</a></li>
                                            @php $count++; @endphp
                                        @empty   
                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="footer-widget contact-widget wow fadeInUp animated" data-wow-delay="600ms"
                                data-wow-duration="1500ms">
                                <div class="widget-title">
                                    <h4>Contacts</h4>
                                </div>
                                <div class="widget-content">
                                    <ul class="info-list clearfix">
                                        <li>{{ $settings->app_address }}</li>
                                        <li><a
                                                href="tel:91{{ $settings->app_mobile }}">{{ $settings->app_mobile }}</a>
                                        </li>
                                        <li><a
                                                href="mailto:{{ $settings->app_email }}">{{ $settings->app_email }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="auto-container">
                    <div class="bottom-inner clearfix">
                        <div class="copyright pull-left">
                            <p><a href="{{ route('/') }}">{{ $settings->copyright }}</a></p>
                        </div>
                        <ul class="footer-nav clearfix pull-right">
                            <li><a href="{{ route('termcondition') }}">Terms of Service</a></li>
                            <li><a href="{{ route('privacypolicy') }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer-one end -->


        <!--Scroll to top-->
        <div class="scroll-to-top">
            <div>
                <div class="scroll-top-inner">
                    <div class="scroll-bar">
                        <div class="bar-inner"></div>
                    </div>
                    <div class="scroll-bar-text g_color">Go To Top</div>
                </div>
            </div>
        </div>
        <!-- Scroll to top end -->


    </div>


    <!-- jequery plugins -->
    <script src="{{ asset('assetsweb/js/jquery.js') }}"></script>
    <script src="{{ asset('assetsweb/js/popper.min.js') }}"></script>
    <script src="{{ asset('assetsweb/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assetsweb/js/plugins.js') }}"></script>
    <script src="{{ asset('assetsweb/js/owl.js') }}"></script>
    <script src="{{ asset('assetsweb/js/wow.js') }}"></script>
    <script src="{{ asset('assetsweb/js/validation.js') }}"></script>
    <script src="{{ asset('assetsweb/js/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('assetsweb/js/appear.js') }}"></script>
    <script src="{{ asset('assetsweb/js/scrollbar.js') }}"></script>
    <script src="{{ asset('assetsweb/js/parallax.min.js') }}"></script>
    <script src="{{ asset('assetsweb/js/circle-progress.js') }}"></script>
    <script src="{{ asset('assetsweb/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('assetsweb/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assetsweb/js/parallax-scroll.js') }}"></script>

    <!-- main-js -->
    <script src="{{ asset('assetsweb/js/script.js') }}"></script>

    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.animsition.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        function submitFeedbackForm() {
            $("#feedback_form").submit();
        }

        $(document).ready(function() {
            @if (Session::has('success'))
                swal({
                    title: "Success!",
                    text: "{{ Session::get('success') }}",
                    confirmButtonColor: '#d7218e',
                    timer: 7000
                });
            @endif

            @if (Session::has('failed'))
                swal({
                    title: "Failed!",
                    text: "{{ Session::get('failed') }}",
                    confirmButtonColor: '#000',
                    timer: 7000
                });
            @endif
        });
    </script>

    @stack('script')
</body><!-- End of .page_wrapper -->

</html>
