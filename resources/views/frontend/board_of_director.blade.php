@extends('layouts.app')

@section('content')

<section class="page-title style-two p_relative centred">
    <div class="pattern-layer">
        <div class="shape-1 p_absolute l_120 t_120 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
        <div class="shape-2 p_absolute t_180 r_170 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
    </div>
    <div class="auto-container">
        <div class="content-box">
            <h1 class="d_block fs_60 lh_70 fw_bold mb_10">Board of Directors</h1>
            <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a href="{{route('/')}}">Home</a></li>
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Pages</li>
                <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">Board of Directors</li>
            </ul>
        </div>
    </div>
</section>

<section class="team-five about-page-1 p_relative pt_140 centred pb_140">
    <div class="auto-container">
        <div class="sec-title-three p_relative d_block mb_50">
            <h6 class="d_block fs_17 lh_26 fw_sbold font_family_inter uppercase mb_18">Our Directors </h6>
            <h2 class="d_block fs_40 fw_bold font_family_inter">Meet Our Board of <br>Directors </h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-6 col-sm-12 team-block">
                <div class="team-block-one wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block b_radius_5">
                        <div class="image-box p_relative d_block">
                            <figure class="image p_relative d_block"><img src="{{asset('assetsweb/images/team/team-24.jpg')}}" alt=""></figure>
                            <ul class="social-links-two">
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-twitter"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                        <div class="lower-content p_relative d_block pt_25">
                            <h4 class="p_relative d_iblock fs_20 lh_30 fw_sbold"><a href="javascript:;">Roger Jones</a></h4>
                            <span class="designation fs_16 p_relative d_block font_family_poppins">Designer</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 team-block">
                <div class="team-block-one wow fadeInUp animated animated" data-wow-delay="200ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 200ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block b_radius_5">
                        <div class="image-box p_relative d_block">
                            <figure class="image p_relative d_block"><img src="{{asset('assetsweb/images/team/team-25.jpg')}}" alt=""></figure>
                            <ul class="social-links-two">
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-twitter"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                        <div class="lower-content p_relative d_block pt_25">
                            <h4 class="p_relative d_iblock fs_20 lh_30 fw_sbold"><a href="javascript:;">Ann Dowson</a></h4>
                            <span class="designation fs_16 p_relative d_block font_family_poppins">Designer</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 team-block">
                <div class="team-block-one wow fadeInUp animated animated" data-wow-delay="400ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 400ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block b_radius_5">
                        <div class="image-box p_relative d_block">
                            <figure class="image p_relative d_block"><img src="{{asset('assetsweb/images/team/team-26.jpg')}}" alt=""></figure>
                            <ul class="social-links-two">
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-twitter"></i></a></li>
                                <li class="p_relative d_iblock"><a href="javascript:;" class="w_40 h_40 lh_40 fs_17 centred"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                        <div class="lower-content p_relative d_block pt_25">
                            <h4 class="p_relative d_iblock fs_20 lh_30 fw_sbold"><a href="javascript:;">Nicolas Lawson</a></h4>
                            <span class="designation fs_16 p_relative d_block font_family_poppins">Designer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection