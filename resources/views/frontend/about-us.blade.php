@extends('layouts.app')

@section('content')

<section class="page-title style-two p_relative centred">
    <div class="pattern-layer">
        <div class="shape-1 p_absolute l_120 t_120 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
        <div class="shape-2 p_absolute t_180 r_170 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
    </div>
    <div class="auto-container">
        <div class="content-box">
            <h1 class="d_block fs_60 lh_70 fw_bold mb_10">About Us</h1>
            <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a href="index.html">Home</a></li>
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Pages</li>
                <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">About Us </li>
            </ul>
        </div>
    </div>
</section>


<section class="about-17 about-page-2 p_relative pb_150 pt_40">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div class="content_block_25">
                    <div class="content-box">
                        <div class="sec-title-12 p_relative d_block mb_35">
                            <h5 class="d_block fs_17 lh_26 fw_sbold font_family_inter uppercase mb_19">About</h5>
                            <h2 class="d_block fs_40 lh_50 fw_bold font_family_inter">We create brand new identities</h2>
                        </div>
                        <div class="text p_relative d_block mb_45">
                            <p class="fs_17 font_family_poppins lh_28">Excepteur sint occaecat cupidatat non proident sunt in culpa  officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="inner-box">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 single-column">
                                    <div class="single-item">
                                        <div class="icon-box p_relative text fs_45 mb_19">
                                            <div class="icon"><i class="icon-11"></i></div>
                                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-106.png')}}" alt=""></div>
                                        </div>
                                        <h4 class="d_block fs_20 lh_30 fw_sbold mb_8">Our Mission</h4>
                                        <p class="fs_16 font_family_poppins">Enim ad minim veniam quis nostrud exercitation lab</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 single-column">
                                    <div class="single-item">
                                        <div class="icon-box p_relative text fs_45 mb_19">
                                            <div class="icon"><i class="icon-14"></i></div>
                                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-107.png')}}" alt=""></div>
                                        </div>
                                        <h4 class="d_block fs_20 lh_30 fw_sbold mb_8">Our Vision</h4>
                                        <p class="fs_16 font_family_poppins">Enim ad minim veniam quis nostrud exercitation lab</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div class="image-box p_relative d_block ml_70 pr_180 pb_120">
                    <div class="shape">
                        <div class="shape-1 p_absolute rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                        <div class="shape-2 p_absolute w_80 h_80 r_20 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-177.png);"></div>
                    </div>
                    <figure class="image image-1 p_relative d_block b_radius_50"><img src="{{asset('assetsweb/images/resource/about-9.jpg')}}" alt=""></figure>
                    <figure class="image image-2 p_absolute r_0 b_0 b_radius_50"><img src="{{asset('assetsweb/images/resource/about-10.jpg')}}" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="process-five p_relative sec-pad centred">
    <div class="shape p_absolute l_160 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
    <div class="auto-container">
        <div class="sec-title-12 p_relative d_block mb_100">
            <h5 class="d_block fs_17 lh_26 fw_sbold font_family_inter uppercase mb_19">Process</h5>
            <h2 class="d_block fs_40 lh_50 fw_bold font_family_inter mb_30">Always Work Done by The <br>Following Process</h2>
            <p class="fs_17 font_family_poppins lh_28">Amet consectur adipiscing elit sed eiusmod ex tempor incidid <br>unt labore dolore aliquaenim minim veniam. </p>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-6 col-sm-12 processing-block">
                <div class="processing-block-four">
                    <div class="inner-box p_relative d_block pl_30 pr_30">
                        <div class="line-shape p_absolute tran_5 t_20" style="background-image: url(assetsweb/images/shape/shape-178.png);"></div>
                        <div class="count-box p_relative d_iblock w_70 h_70 lh_70 centred b_radius_50 fs_18 fw_sbold font_family_inter z_1 tran_5 mb_80 counted">
                            <span class="p_relative tran_5">1</span>
                            <div class="overlay-icon p_absolute l_0 t_0 r_0 w_70 h_70 lh_70 centred fs_25 tran_5"><i class="icon-16"></i></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 fw_sbold lh_30 mb_16">Make Schedule</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit amet adipelit sed eiusmtempor dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 processing-block">
                <div class="processing-block-four">
                    <div class="inner-box p_relative d_block pl_30 pr_30">
                        <div class="line-shape p_absolute tran_5 t_20" style="background-image: url(assetsweb/images/shape/shape-178.png);"></div>
                        <div class="count-box p_relative d_iblock w_70 h_70 lh_70 centred b_radius_50 fs_18 fw_sbold font_family_inter z_1 tran_5 mb_80 counted">
                            <span class="p_relative tran_5">2</span>
                            <div class="overlay-icon p_absolute l_0 t_0 r_0 w_70 h_70 lh_70 centred fs_25 tran_5"><i class="icon-16"></i></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 fw_sbold lh_30 mb_16">Start Discussion</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit amet adipelit sed eiusmtempor dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 processing-block">
                <div class="processing-block-four">
                    <div class="inner-box p_relative d_block pl_30 pr_30">
                        <div class="count-box p_relative d_iblock w_70 h_70 lh_70 centred b_radius_50 fs_18 fw_sbold font_family_inter z_1 tran_5 mb_80 counted">
                            <span class="p_relative tran_5">3</span>
                            <div class="overlay-icon p_absolute l_0 t_0 r_0 w_70 h_70 lh_70 centred fs_25 tran_5"><i class="icon-16"></i></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 fw_sbold lh_30 mb_16">Enjoy Plan</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit amet adipelit sed eiusmtempor dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="faq-two p_relative sec-pad">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div class="image_block_one">
                    <div data-animation-box="" class="image-box p_relative d_block mr_30 pr_170 pb_130 animated">
                        <div class="shape">
                            <div class="shape-3 p_absolute b_35 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                            <div class="shape-4 p_absolute r_85 t_150 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                        </div>
                        <figure data-animation-text="" class="overlay-anim-black-bg image image-1 p_relative d_block  animated overlay-animation" data-animation="overlay-animation" style="transform: translateY(23px); transition: transform 0.6s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform;"><img src="assetsweb/images/resource/about-11.jpg" alt=""></figure>
                        <div class="video-inner p_absolute b_0 r_0 text-center pt_85 pb_85 z_1 " style="background-image: url(&quot;assetsweb/images/resource/about-12.jpg&quot;); transform: translateY(-37px); transition: transform 0.6s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform;">
                            <div class="video-btn">
                                <a href="https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s" class="lightbox-image video-btn p_relative d_iblock w_80 h_80 lh_85 text-center b_radius_50" data-caption=""><i class="icon-10"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div class="content_block_five">
                    <div class="content-box p_relative d_block mr_30">
                        <div class="sec-title-12 p_relative d_block mb_35">
                            <h5 class="d_block fs_17 lh_26 fw_sbold font_family_inter uppercase mb_19">Frequently Asked question</h5>
                            <h2 class="d_block fs_40 lh_50 fw_bold font_family_inter">Innovation and Countless Opportunities.</h2>
                        </div>
                        <ul class="accordion-box">
                            <li class="accordion block active-block p_relative d_block mb_30">
                                <div class="acc-btn active p_relative d_block tran_5 pt_16 pr_80 pb_16 pl_30">
                                    <div class="icon-outer p_absolute fs_10 tran_5 z_1"><i class="icon-29"></i></div>
                                    <h4 class="p_relative d_block fs_20 lh_30 fw_medium">Is this theme support powerfull options?</h4>
                                </div>
                                <div class="acc-content current p_relative pt_25 pr_50 pl_30">
                                    <div class="text p_relative d_block">
                                        <p class="font_family_poppins">Lorem ipsum dolor sit elit consectur sed eiusmod tempor aliquat enim minim veniam exercitation ulamco laboris nis aliquip commodo.</p>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block p_relative d_block mb_30">
                                <div class="acc-btn p_relative d_block tran_5 pt_16 pr_80 pb_16 pl_30">
                                    <div class="icon-outer p_absolute fs_10 tran_5 z_1"><i class="icon-29"></i></div>
                                    <h4 class="p_relative d_block fs_20 lh_30 fw_medium">How do i install this theme?</h4>
                                </div>
                                <div class="acc-content p_relative pt_25 pr_50 pl_30">
                                    <div class="text p_relative d_block">
                                        <p class="font_family_poppins">Lorem ipsum dolor sit elit consectur sed eiusmod tempor aliquat enim minim veniam exercitation ulamco laboris nis aliquip commodo.</p>
                                    </div>
                                </div>
                            </li>
                            <li class="accordion block p_relative d_block">
                                <div class="acc-btn p_relative d_block tran_5 pt_16 pr_80 pb_16 pl_30">
                                    <div class="icon-outer p_absolute fs_10 tran_5 z_1"><i class="icon-29"></i></div>
                                    <h4 class="p_relative d_block fs_20 lh_30 fw_medium">Where can i found the collor options? </h4>
                                </div>
                                <div class="acc-content p_relative pt_25 pr_50 pl_30">
                                    <div class="text p_relative d_block">
                                        <p class="font_family_poppins">Lorem ipsum dolor sit elit consectur sed eiusmod tempor aliquat enim minim veniam exercitation ulamco laboris nis aliquip commodo.</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
