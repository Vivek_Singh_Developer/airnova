@extends('layouts.app')

@section('content')

    <div class="container top-banner body-height">
        <div class="row privacy-box">
            <div class="col-12 pt-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
            </div>

            <div class="col-12 text-center pb-2">
                <h1 class="py-2">PRIVACY POLICY</h1>
            </div>

            <div class="row">
                <div class="my-4">
                    <div class="col-12">
                        <p>To see our documented Privacy Policy, peer down.
                            Play Pause Continue Technology Private Limited thinks often about your security. Therefore, we gather and utilize individual data just on a case-by-case basis to convey our items, administrations, sites and versatile applications and to speak with you about the same, or as you have referenced.</p>
                        <p>Your own data incorporates data, for example,</p>
                       
                        <p>Our Privacy Policy clarifies how and why we utilize the individual data that we gather and how you can access, update, or in any case assume responsibility for your own data. We've additionally made a contact centre that offers replies to your most normal inquiries; speedy connects to get to your Account Settings, guidelines on the best way to practice specific privileges that may be accessible to you and definitions to key terms and ideas noted in this Privacy Policy.</p>
                        <p>If whenever you have inquiries regarding our practices or any of your freedoms depicted beneath, you might arrive at our Data Protection Officer ("DPO") and our devoted group that upholds this office by reaching us at <b style="font-size: 20px;">privacy@airnovagroup.com</b> . This inbox is effectively checked and figured out how to convey an encounter that you can unquestionably trust.</p>
                        <p>What data we gather, how we gather it, and why
                            A lot of what you probably consider individual data is gathered straightforwardly from you when you:</p>
                        <ul>
                            <li> Make a record or buy any of our Services (ex: charging data, including name, address, Visa number, government distinguishing proof).</li>
                            <li> Demand help from our honour winning client care group (ex: telephone number).</li>
                            <li> Complete contact structures or solicitation bulletins or other data from us (ex: email); or</li>
                            <li> Partake in challenges and reviews, go after a position, or in any case take part in exercises we advance that require data about you.</li>
                        </ul>
                        <p>Notwithstanding, we likewise gather extra data while conveying our Services to you to guarantee fundamental and ideal execution. These strategies for assortment may not be as evident to you, so we thought we'd feature and clarify a touch more with regards to what these may be (as they differ every once in a while):</p>
                        <p>Cookies and comparative innovations on our sites and portable applications permit us to follow your perusing conduct, for example, joins clicked, pages saw, and things bought. In particular, the information gathered connects with your cooperation’s with our website elements or outsider highlights, for example, web-based media stopping, held inside the Services; Internet Protocol (IP) address (for instance, to decide your language inclination; program type and settings; the date and time the Services were utilized; gadget type and settings; working framework utilized; application IDs, exceptional gadget identifiers; and blunder information). </p>
                        <p> These advancements likewise naturally gather information to gauge site execution and further develop our frameworks, including metadata, log documents, page load time, server reaction time to streamline DNS goal, network steering and server arrangements.</p>
                        <p>Enhanced Data might be gotten with regards to you from different sources, for instance, from freely accessible data sets, web-based media stages, or outsiders from whom we have bought information, in which case we might consolidate this information with data we as of now have about you so we can refresh, extend and examine the precision of our records, survey the capabilities of a contender for work, distinguish new clients, and give items and administrations that might bear some significance with you. Assuming you give us individual data about others or give us your data, we will just involve that data for the particular justification behind which it was given to us.</p>
                        <h3>How we use data:</h3>
                        <p>We firmly trust in both limiting the information we gather and restricting its utilization and reason to just that</p>
                        <ul>
                            <li>Which we have been given consent,</li>
                            <li>As important to convey the Services you buy or associate with, or</li>
                            <li>As we may be required or allowed for legitimate consistence or other legal purposes:</li>

                        </ul>
                        <p>Conveying, improving, refreshing and upgrading our Services. We gather different information connecting with your buy, use and associations with our Services.</p>
                        <h3>We use this data to:</h3>
                        <ul>
                            <li>Improve and enhance the activity and execution of our Services (once more, including our sites and portable applications)</li>
                            <li>Determine issues to have and distinguish any security and consistence dangers, blunders, or required upgrades to the Services</li>
                            <li>Identify and forestall misrepresentation and maltreatment of our Services and frameworks</li>

                        </ul>
                        <p>A significant part of the information gathered is totalled or measurable information regarding how people use our Services and are not connected to individual data. </p>
                        <h3>Advertisements</h3>
                        <p>We need to serve you promotions that are generally important and valuable to you, so we might utilize the individual data you gave us in Account Settings or those gathered through treat innovations to customize advertisements and further develop your general involvement in us on our Site different locales. We call this advertisement personalization (previously interest-based promoting) on the grounds that the advertisements you see depend on recently gathered or authentic information to figure out which promotion will be generally applicable to you, including information, for example, past search questions, action, visits to destinations or applications, segment data, or area. The following time you explore to our publicizing accomplice's foundation or site, they might perceive that you are a (P<sup>²</sup>C) client by means of a matched email and utilize that information to assist us with showing an advertisement about our most recent advancement for site security. On the other hand, assuming you previously bought our site security item, then, at that point, we might bar you from future promotion lobbies for this item since it wouldn't be applicable to you. Other promotion personalization could be movement based.
                            For instance, utilizing information about your online visits on our Site just to decide if an advertisement would bear some significance with you or not. The data we share with our publicizing channel accomplices to convey you customized advertisements is done safely and can't be utilized by them for some other reason. We don't offer your information to any outsiders. Also, the information we share is hashed and encoded, which implies that it doesn't straightforwardly distinguish you. We don't offer your information to our accomplices or other outsiders in any capacity. To deal with your inclinations for crowd-based promotion personalization, kindly sign into your Account and visit "Record Settings." To deal with your inclinations for movement-based advertisement personalization, assuming no one really minds, see our Cookie Policy. Also go to "Oversee Settings." If you quit promotion personalization, you might keep on getting advertisements, yet those advertisements might be less applicable to you. </p>
                         <h3>Changes to this policy</h3>
                        <p>We keep up with whatever authority is expected to change this Privacy Policy at whatever point. Expect we decide to change our Privacy Policy. All things considered, we will present those progressions on this Privacy Policy and some other spots we consider suitable with the goal that you know about what data we gather, how we use it. Under what conditions expecting to be any, we uncover it. Assume we roll out material improvements to this Privacy Policy. All things considered, we will inform you here, by email, or utilizing a notification on our landing page, no less than thirty (30) days prior to carrying out the changes. </p>

                     </div>

                </div>
            </div>
        </div>
    </div>
@endsection
