@extends('layouts.app')

@section('content')
    <!-- Page Title -->
    <style>
         .sec-pad {
            padding: 15px 0px;
        }
        .pt_140 {
            padding-top: 14px;
        }
        .shapeabc {
            background-image: url('../../assetsweb/images/shape/shape-176.png');
        }

        .shapeabcd {
            background-image: url('../../assetsweb/images/shape/shape-56.png');
        }

        .shapeabcde {
            background-image: url('../../assetsweb/images/shape/shape-189.png');
        }
    </style>
    <!-- Page Title -->
    <section class="page-title about-page-5 style-two p_relative centred">
        <div class="pattern-layer">
            <div class="shapeabc shape-1 p_absolute l_120 t_120 rotate-me"></div>
            <div class="shapeabcd shape-2 p_absolute t_180 r_170 float-bob-y"></div>
            <div class="shapeabcde shape-3 p_absolute l_0 b_0"></div>
        </div>
        <div class="auto-container">
            <div class="content-box">
                <h1 class="d_block fs_60 lh_70 fw_bold mb_10">Product Details</h1>
                <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a
                            href="{{ url('/') }}">Home</a></li>
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Product</li>
                    <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">
                        {{ $product->getCategory->cat_name }}</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Page Title -->


    <!-- shop-details -->
    <section class="shop-details p_relative pt_140 pb_80">
        <div class="auto-container">
            <div class="product-details-content p_relative d_block mb_100">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div class="image-box p_relative d_block">
                            
                            <figure class="image"><img src="{{ url('storage/product/' . $product->thumbnail_image) }}" alt=""></figure>
                            <div class="preview-link p_absolute t_20 r_20"><a href="{{ url('storage/product/' . $product->thumbnail_image) }}"
                                    class="lightbox-image p_relative d_iblock fs_20 centred z_1 w_50 h_50 color_black lh_50"
                                    data-fancybox="gallery"><i class="far fa-search-plus"></i></a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div class="product-details p_relative d_block ml_20">
                            <h2 class="d_block fs_30 lh_40 fw_sbold font_family_inter mb_5">{{ $product->product_name }}</h2>
                           
                            <span
                                class="price p_relative d_block fs_20 lh_30 fw_medium color_black mb_25 font_family_inter">&#8377;  {{ $product->product_price }}</span>
                            <div class="text p_relative d_block mb_30">
                                <p class="font_family_poppins mb_25">{!! $product->product_description !!}</p>
                            </div>
                            <div class="other-option">
                                <ul class="list">
                                    <li class="p_relative d_block fs_16 font_family_poppins mb_5"><span
                                            class="fw_medium color_black">Product ID:</span> {{ $product->product_code }}</li>
                                    <li class="p_relative d_block fs_16 font_family_poppins mb_5"><span
                                            class="fw_medium color_black">Category</span> {{ $product->getCategory->cat_name }}</li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="related-product shop-page-2 shop-page-section">
                <div class="title-text mb_25">
                    <h2 class="d_block fs_30 lh_40 fw_sbold">Related Products</h2>
                </div>
                <div class="row clearfix">
                    @forelse ($productdata as $item)
                        <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                            <div class="shop-block-one">
                                <div class="inner-box p_relative d_block tran_5 mb_30">
                                    <div class="image-box p_relative d_block">
                                        <figure class="image p_relative d_block">
                                            <img src="{{ url('storage/product/' . $item->thumbnail_image) }}" alt=""></figure>
                                       
                                    </div>
                                    <div class="lower-content p_relative d_block pt_25 pb_30">
                                        <h6 class="d_block fs_15 lh_20 mb_4"><a href="{{route('productDetail',$item->id)}}"
                                                class="d_iblock color_black">{{$item->product_name}}</a></h6>
                        
                                        <span
                                            class="price p_relative d_block fs_15 fw_medium font_family_inter color_black mb_2"> &#8377;  {{$item->product_price}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <!-- shop-details end -->
@endsection
