@extends('layouts.app')

@section('content')
    <!-- Page Title -->
    <style>
        .sec-pad {
            padding: 15px 0px;
        }
        .shapeabc {
            background-image: url('../assetsweb/images/shape/shape-176.png');
        }

        .shapeabcd {
            background-image: url('../assetsweb/images/shape/shape-56.png');
        }

        .shapeabcde {
            background-image: url('../assetsweb/images/shape/shape-189.png');
        }
    </style>
    <!-- Page Title -->
    <section class="page-title about-page-5 style-two p_relative centred">
        <div class="pattern-layer">
            <div class="shapeabc shape-1 p_absolute l_120 t_120 rotate-me"></div>
            <div class="shapeabcd shape-2 p_absolute t_180 r_170 float-bob-y"></div>
            <div class="shapeabcde shape-3 p_absolute l_0 b_0"></div>
        </div>
        <div class="auto-container">
            <div class="content-box">
                <h1 class="d_block fs_60 lh_70 fw_bold mb_10">{{$servicedata->service_name}}</h1>
                <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a
                            href="{{ url('/') }}">Home</a></li>
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Service</li>
                    {{-- <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">
                        {{ $servicedata->getCategory->cat_name }}</li> --}}
                </ul>
            </div>
        </div>
    </section>
    <!-- End Page Title -->
 <!-- service-details -->
 <section class="service-details service-details-1 p_relative sec-pad">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="service-details-content">
                    <div data-animation-box class="content-one p_relative d_block mb_55">
                        <figure data-animation-text class="overlay-anim-black-bg image p_relative d_block b_radius_10 mb_60" data-animation="overlay-animation">
                            <img src="{{ url('storage/service/' . $servicedata->thumbnail_image) }}" alt=""></figure>
                        <div class="text">
                            <h2 class="d_block fs_40 fw_sbold lh_50 mb_25"> {{$servicedata->service_name}}</h2><br />
                            <p class="font_family_poppins mb_25">{!!$servicedata->description!!}</p>
                        </div>
                    </div>
                    <div class="content-two p_relative d_block mb_55">
                        <h3 class="d_block fs_30 lh_40 fw_sbold mb_25">{{$servicedata->sub_heading}}</h3>
                        <p class="font_family_poppins mb_25">{!!$servicedata->sub_heading_desc!!}</p>
                        
                    </div>
                    <div data-animation-box class="two-column p_relative d_block mb_60">

                        <div class="row clearfix">
                            @php 
                                $img = json_decode($servicedata->images,true);
                                $aaaa = is_array( $img) ? $img : [];
                            @endphp
                            @if (count($img) > 0)
                            @foreach ($img as $item)
                                @if ($item != null)
                                    <div class="col-lg-6 col-md-6 col-sm-12 image-column">
                                        <figure data-animation-text class="overlay-anim-black-bg image p_relative d_block b_radius_10" data-animation="overlay-animation"><img src="{{ url('storage/service/' . $item) }}" alt="{{ url('storage/service/' . $item) }}"></figure>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                <div class="service-sidebar p_relative d_block ml_30">
                    <div class="category-widget p_relative d_block mb_60">
                        <ul class="category-list clearfix">
                            @forelse ($allservice as $item)
                                <li class="p_relative d_block mb_10">
                                    <a href="{{route('service_by_id',$item->id)}}" class="p_relative d_block fs_16 fw_sbold font_family_inter {{$item->id==$servicedata->id ? 'current' : ''}} ">
                                        <i class="icon-11"></i>{{$item->service_name}}</a></li>
                                
                            @empty
                                
                            @endforelse
                         </ul>
                    </div>
                   
                    <div class="contact-widget sidebar-widget p_relative d_block pt_40 pr_30 pb_40 pl_30 b_radius_5">
                        <div class="widget-title p_relative d_block mb_20">
                            <h3 class="d_block fs_24 lh_30 fw_sbold">Contact Us</h3>
                        </div>
                        <ul class="info-list clearfix"> 
                            <li class="p_relative d_block fs_15 font_family_inter color_black pl_30 mb_10"><i class="icon-2"></i>Call: <a href="tel:{{ $settings->app_mobile }}" class="d_iblock color_black">+91 {{ $settings->app_mobile }}</a></li>
                            <li class="p_relative d_block fs_15 font_family_inter color_black pl_30 mb_10"><i class="icon-137"></i>Address:  {{ $settings->app_address }}e</li>
                            <li class="p_relative d_block fs_15 font_family_inter color_black pl_30"><i class="icon-3"></i>Email: <a href="mailto:{{ $settings->app_email }}" class="d_iblock color_black">{{ $settings->app_email }}</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service-details -->

@endsection
