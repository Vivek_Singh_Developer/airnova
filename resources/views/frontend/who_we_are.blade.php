@extends('layouts.app')

@section('content')

<section class="page-title about-page-5 style-two p_relative centred">
    <div class="pattern-layer">
        <div class="shape-1 p_absolute l_120 t_120 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
        <div class="shape-2 p_absolute t_180 r_170 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
        <div class="shape-3 p_absolute l_0 b_0" style="background-image: url(assetsweb/images/shape/shape-189.png);"></div>
    </div>
    <div class="auto-container">
        <div class="content-box">
            <h1 class="d_block fs_60 lh_70 fw_bold mb_10">Who We Are</h1>
            <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a href="{{route('/')}}">Home</a></li>
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Pages</li>
                <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">Who We Are</li>
            </ul>
        </div>
    </div>
</section>

<section class="about-20 about-page-5 p_relative sec-pad">
    <div class="auto-container">
        <div class="title-box p_relative d_block mb_70 pb_60">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-12 col-sm-12 title-column">
                    <div class="sec-title-six p_relative d_block text-right mr_40 ml_30">
                        <h2 class="d_block fs_40 lh_52 fw_bold">The Best Solutions for Best Business</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 text-column">
                    <div class="text p_relative ml_40">
                        <p class="font_family_poppins color_black">Lorem ipsum dolor sit amet cons etur adipisicing elit sed do eiusm aut tempor incididunt labore dolore magna aliqua quis nostrud ex ertation lamcolab oris aliquip.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-box p_relative d_block mb_100">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 feature-block">
                    <div class="feature-block-nine wow fadeInUp animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block pl_100 pt_6">
                            <div class="icon-box p_absolute l_0 t_0 d_iblock w_80 h_80 lh_80 text-center b_radius_50 fs_50 tran_5 z_1">
                                <div class="icon g_color_2"><i class="icon-113"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-117.png')}}" alt=""></div>
                            </div>
                            <div class="text">
                                <h4 class="d_block fs_20 lh_30 fw_sbold mb_11">Company Vision</h4>
                                <p class="font_family_poppins">Lorem ipsum dolor sit elit con sectur sed eiusmod tempor labore set aliquat.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-block">
                    <div class="feature-block-nine wow fadeInUp animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block pl_100 pt_6">
                            <div class="icon-box p_absolute l_0 t_0 d_iblock w_80 h_80 lh_80 text-center b_radius_50 fs_50 tran_5 z_1">
                                <div class="icon g_color_2"><i class="icon-117"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-118.png')}}" alt=""></div>
                            </div>
                            <div class="text">
                                <h4 class="d_block fs_20 lh_30 fw_sbold mb_11">Strategy Monitoring</h4>
                                <p class="font_family_poppins">Lorem ipsum dolor sit elit con sectur sed eiusmod tempor labore set aliquat.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 feature-block">
                    <div class="feature-block-nine wow fadeInUp animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block pl_100 pt_6">
                            <div class="icon-box p_absolute l_0 t_0 d_iblock w_80 h_80 lh_80 text-center b_radius_50 fs_50 tran_5 z_1">
                                <div class="icon g_color_2"><i class="icon-119"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-119.png')}}" alt=""></div>
                            </div>
                            <div class="text">
                                <h4 class="d_block fs_20 lh_30 fw_sbold mb_11">Advanced Reporting</h4>
                                <p class="font_family_poppins">Lorem ipsum dolor sit elit con sectur sed eiusmod tempor labore set aliquat.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="image-box p_relative">
            <div class="image-shape">
                <div class="shape-1 p_absolute b_radius_50"></div>
                <div class="shape-2 p_absolute b_radius_50"></div>
                <div class="shape-3 p_absolute b_55 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-36.png);"></div>
                <div class="shape-4 p_absolute float-bob-x" style="background-image: url(assetsweb/images/shape/shape-36.png);"></div>
            </div>
            <div data-animation-box="" class="row clearfix animated">
                <div class="col-lg-6 col-md-6 col-sm-12 image-column">
                    <figure data-animation-text="" class="overlay-anim-black-bg image p_relative d_block b_radius_5 animated overlay-animation" data-animation="overlay-animation"><img src="{{asset('assetsweb/images/resource/about-16.jpg')}}" alt=""></figure>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 image-column">
                    <figure data-animation-text="" class="overlay-anim-black-bg image p_relative d_block b_radius_5 animated overlay-animation" data-animation="overlay-animation"><img src="{{asset('assetsweb/images/resource/about-17.jpg')}}" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection