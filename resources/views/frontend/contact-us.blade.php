@extends('layouts.app')

@section('content')
    <section class="page-title about-page-5 style-two p_relative centred">
        <div class="pattern-layer">
            <div class="shape-1 p_absolute l_120 t_120 rotate-me"
                style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
            <div class="shape-2 p_absolute t_180 r_170 float-bob-y"
                style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
            <div class="shape-3 p_absolute l_0 b_0" style="background-image: url(assetsweb/images/shape/shape-189.png);">
            </div>
        </div>
        <div class="auto-container">
            <div class="content-box">
                <h1 class="d_block fs_60 lh_70 fw_bold mb_10">Contact Us</h1>
                <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a
                            href="{{ route('/') }}">Home</a></li>
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Pages</li>
                    <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">Contact Us</li>
                </ul>
            </div>
        </div>
    </section>


    <section class="contact-five p_relative pt_140 pb_150">
        <div class="shape">
            <div class="shape-1 p_absolute t_0 l_90" style="background-image: url(assetsweb/images/shape/shape-208.png);">
            </div>
            <div class="shape-2 p_absolute r_130 b_0" style="background-image: url(assetsweb/images/shape/shape-209.png);">
            </div>
        </div>
        <div class="shape">
            <div class="shape-1 p_absolute l_90" style="background-image: url(assetsweb/images/shape/shape-208.png);"></div>
            <div class="shape-2 p_absolute r_130 b_0" style="background-image: url(assetsweb/images/shape/shape-209.png);">
            </div>
        </div>
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 map-column">
                    <div class="map-inner p_relative d_block">
                        <iframe style="width: 580px;height: 522px;border-radius: 2%;"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112184.4604916183!2d77.33112535381963!3d28.51673559180199!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce5a43173357b%3A0x37ffce30c87cc03f!2sNoida%2C%20Uttar%20Pradesh!5e0!3m2!1sen!2sin!4v1668366178504!5m2!1sen!2sin"
                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>

                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div class="form-inner p_relative pt_45 pr_50 pb_50 pl_50 b_radius_10 b_shadow_6">
                        <div class="text p_relative d_block mb_35">
                            <h3 class="d_block fs_30 lh_40 fw_bold">Send a Message</h3>
                        </div>
                        <form method="post" action="{{ route('feedback.store') }}" id="contact-form" novalidate="novalidate">
                            @csrf
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="text" name="name" placeholder="Your Name" value="{{ old('name') }}" required=""
                                        aria-required="true">
                                        @if ($errors->has('name'))
                                        <div class="error text-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <input type="email" name="email" placeholder="Email Address" value="{{old('email')}}" required=""
                                        aria-required="true">
                                        @if ($errors->has('email'))
                                        <div class="error text-danger">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="mobile" pattern="[56789][0-9]{9}" required="" value="{{old('mobile')}}" placeholder="Phone Number"
                                        aria-required="true">
                                        @if ($errors->has('mobile'))
                                        <div class="error text-danger">{{ $errors->first('mobile') }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 form-group">
                                    <input type="text" name="subject" required="" placeholder="Subject" value="{{old('subject')}}"
                                        aria-required="true">
                                        @if ($errors->has('subject'))
                                            <div class="error text-danger">{{ $errors->first('subject') }}</div>
                                        @endif
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <textarea name="message" placeholder="Leave A Comment">{{old('message')}}</textarea>
                                    @if ($errors->has('message'))
                                            <div class="error text-danger">{{ $errors->first('message') }}</div>
                                        @endif
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group message-btn">
                                    <button class="theme-btn theme-btn-eight" type="submit" name="submit-form">Send
                                        Message <i class="icon-4"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contactinfo-one contact-page-4 p_relative sec-pad centred">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="info-block-one wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block b_radius_10 b_shadow_6 pt_40 pr_40 pb_35 pl_40">
                            <div class="icon-box p_relative d_iblock w_80 h_80 lh_80 b_radius_50 text-center fs_45 z_1 mb_25 tran_5">
                                <div class="icon-shape t_0 p_absolute" style="background-image: url(assetsweb/images/shape/shape-212.png);"></div>
                                <div class="icon p_relative d_iblock"><i class="icon-180"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-133.png')}}" alt=""></div>
                            </div>
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_7">Office Location</h4>
                            <p class="font_family_poppins">{{$settings->app_address}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="info-block-one wow fadeInUp animated animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block b_radius_10 b_shadow_6 pt_40 pr_40 pb_35 pl_40">
                            <div class="icon-box p_relative d_iblock w_80 h_80 lh_80 b_radius_50 text-center fs_45 z_1 mb_25 tran_5">
                                <div class="icon-shape t_0 p_absolute" style="background-image: url(assetsweb/images/shape/shape-212.png);"></div>
                                <div class="icon p_relative d_iblock"><i class="icon-181"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-134.png')}}" alt=""></div>
                            </div>
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_7">Company Email</h4>
                            <p class="font_family_poppins"><a href="mailto:{{$settings->app_email}}">{{$settings->app_email}}</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 info-column">
                    <div class="info-block-one wow fadeInUp animated animated" data-wow-delay="600ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                        <div class="inner-box p_relative d_block b_radius_10 b_shadow_6 pt_40 pr_40 pb_35 pl_40">
                            <div class="icon-box p_relative d_iblock w_80 h_80 lh_80 b_radius_50 text-center fs_45 z_1 mb_25 tran_5">
                                <div class="icon-shape t_0 p_absolute" style="background-image: url(assetsweb/images/shape/shape-212.png);"></div>
                                <div class="icon p_relative d_iblock"><i class="icon-182"></i></div>
                                <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-135.png')}}" alt=""></div>
                            </div>
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_7">Contact Us</h4>
                            <p class="font_family_poppins"><a href="tel:91{{$settings->app_mobile}}">+91 {{$settings->app_mobile}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
