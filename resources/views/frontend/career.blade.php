@extends('layouts.app')

@section('content')

<section class="career-section p_relative pt_80 pb_60">
    <div class="pattern-layer">
        <div class="pattern-1 p_absolute l_0 t_0" style="background-image: url(assetsweb/images/shape/shape-201.png);"></div>
        <div class="pattern-2 p_absolute l_10 t_140 rotate-me" style="background-image: url(assetsweb/images/shape/shape-202.png);"></div>
    </div>
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div class="content-box p_relative d_block pt_75">
                    <h2 class="d_block fs_60 lh_70 fw_bold mb_20">Looking for a New Career?Join Us</h2>
                    <p class="font_family_poppins mb_30 lh_28">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="index-javascript:;" class="theme-btn theme-btn-five">See Open Position <i class="icon-4"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div class="image-box p_relative d_block pl_30 pb_140 pr_120">
                    <div class="shape">
                        <div class="shape-1 p_absolute float-bob-y" style="background-image: url(assetsweb/images/shape/shape-200.png);"></div>
                        <div class="shape-2 p_absolute t_170 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                    </div>
                    <figure class="image image-1 p_relative d_block"><img src="assetsweb/images/resource/career-1.png" alt=""></figure>
                    <figure class="image image-2 p_absolute d_block r_0 b_0"><img src="assetsweb/images/resource/career-2.png" alt=""></figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- career-section end -->


<!-- job-category -->
<section class="job-category p_relative pt_130 pb_140">
    <div class="auto-container">
        <div class="sec-title centred mb_50">
            <h2 class="d_block fs_40 fw_bold">Job Category</h2>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 mb_30 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-120"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-127.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Finance & Accounting</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(25)</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 mb_30 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-121"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-128.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Project Management</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(10)</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 mb_30 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-122"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-129.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Business Strategy</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(40)</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-170"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-130.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Marketing & Sales</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(37)</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-14"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-131.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Business Accountant</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(52)</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                <div class="single-item p_relative d_block pl_120 pt_55 pr_20 pb_55 b_radius_5 b_shadow_6 wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="icon-box p_absolute l_30 t_50 w_70 h_70 lh_70 centred fs_40 b_radius_50 d_iblock z_1 tran_5">
                        <div class="icon"><i class="icon-12"></i></div>
                        <div class="icon-img hidden-icon"><img src="assetsweb/images/icons/hid-icon-132.png" alt=""></div>
                    </div>
                    <h4 class="d_block fs_20 lh_30 fw_sbold tran_5">Marketing Analytics</h4>
                    <span class="p_relative d_block fs_16 font_family_poppins tran_5">(27)</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- job-category end -->


<!-- positions-section -->
<section class="positions-section p_relative pb_150">
    <div class="pattern-layer">
        <div class="pattern-1 p_absolute l_100 t_20" style="background-image: url(assetsweb/images/shape/shape-203.png);"></div>
        <div class="pattern-2 p_absolute b_75 rotate-me" style="background-image: url(assetsweb/images/shape/shape-202.png);"></div>
    </div>
    <div class="auto-container">
        <div class="sec-title centred mb_50">
            <h2 class="d_block fs_40 fw_bold mb_25">Open Positions</h2>
            <p class="font_family_poppins">Adipisicing elit, sed do eiusmod tempor incididunt labore et dolore <br />magna aliqua enim ad minim veniam.</p>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-12 positions-block">
                <div class="positions-block-one wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box p_relative d_block pt_45 pr_50 pb_50 pl_50 b_radius_10 b_shadow_6 mb_30">
                        <h3 class="d_block fs_24 lh_30 fw_bold mb_17">Lead Backend Developer</h3>
                        <p class="font_family_poppins mb_25">Lorem ipsum dolor sit elit consectur sed eiusmod tempor enim minim veniam exercitation lamco ex laboris aliquip comodo consequat duis aute.</p>
                        <h4 class="d_block fs_20 lh_30 mb_16">Job Responsibilities</h4>
                        <ul class="list clearfix p_relative d_block mb_25">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Vestibulum nec dolor auctor consequat magna vitae.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Tempor nulla sed scelerisque sollicitudin.</li>
                        </ul>
                        <h4 class="d_block fs_20 lh_30 mb_16">Qualifications</h4>
                        <ul class="list clearfix p_relative d_block mb_30">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Vestibulum nec dolor auctor conseq.</li>
                        </ul>
                        <div class="btn-box">
                            <a href="javascript:;" class="theme-btn theme-btn-six">Apply Now <i class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 positions-block">
                <div class="positions-block-one wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box p_relative d_block pt_45 pr_50 pb_50 pl_50 b_radius_10 b_shadow_6 mb_30">
                        <h3 class="d_block fs_24 lh_30 fw_bold mb_17">Senior Project Manager</h3>
                        <p class="font_family_poppins mb_25">Lorem ipsum dolor sit elit consectur sed eiusmod tempor enim minim veniam exercitation lamco ex laboris aliquip comodo consequat duis aute.</p>
                        <h4 class="d_block fs_20 lh_30 mb_16">Job Responsibilities</h4>
                        <ul class="list clearfix p_relative d_block mb_25">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Vestibulum nec dolor auctor consequat magna vitae.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Tempor nulla sed scelerisque sollicitudin.</li>
                        </ul>
                        <h4 class="d_block fs_20 lh_30 mb_16">Qualifications</h4>
                        <ul class="list clearfix p_relative d_block mb_30">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Vestibulum nec dolor auctor conseq.</li>
                        </ul>
                        <div class="btn-box">
                            <a href="javascript:;" class="theme-btn theme-btn-six">Apply Now <i class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 positions-block">
                <div class="positions-block-one wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box p_relative d_block pt_45 pr_50 pb_50 pl_50 b_radius_10 b_shadow_6">
                        <h3 class="d_block fs_24 lh_30 fw_bold mb_17">Lead UI/UX Designer</h3>
                        <p class="font_family_poppins mb_25">Lorem ipsum dolor sit elit consectur sed eiusmod tempor enim minim veniam exercitation lamco ex laboris aliquip comodo consequat duis aute.</p>
                        <h4 class="d_block fs_20 lh_30 mb_16">Job Responsibilities</h4>
                        <ul class="list clearfix p_relative d_block mb_25">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Vestibulum nec dolor auctor consequat magna vitae.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Tempor nulla sed scelerisque sollicitudin.</li>
                        </ul>
                        <h4 class="d_block fs_20 lh_30 mb_16">Qualifications</h4>
                        <ul class="list clearfix p_relative d_block mb_30">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Vestibulum nec dolor auctor conseq.</li>
                        </ul>
                        <div class="btn-box">
                            <a href="javascript:;" class="theme-btn theme-btn-six">Apply Now <i class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 positions-block">
                <div class="positions-block-one wow fadeInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box p_relative d_block pt_45 pr_50 pb_50 pl_50 b_radius_10 b_shadow_6">
                        <h3 class="d_block fs_24 lh_30 fw_bold mb_17">Lead Frontend Developer</h3>
                        <p class="font_family_poppins mb_25">Lorem ipsum dolor sit elit consectur sed eiusmod tempor enim minim veniam exercitation lamco ex laboris aliquip comodo consequat duis aute.</p>
                        <h4 class="d_block fs_20 lh_30 mb_16">Job Responsibilities</h4>
                        <ul class="list clearfix p_relative d_block mb_25">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Vestibulum nec dolor auctor consequat magna vitae.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Tempor nulla sed scelerisque sollicitudin.</li>
                        </ul>
                        <h4 class="d_block fs_20 lh_30 mb_16">Qualifications</h4>
                        <ul class="list clearfix p_relative d_block mb_30">
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25 mb_7">Integer elementum neque non finibus ultrices.</li>
                            <li class="p_relative d_block fs_16 font_family_poppins pl_25">Vestibulum nec dolor auctor conseq.</li>
                        </ul>
                        <div class="btn-box">
                            <a href="javascript:;" class="theme-btn theme-btn-six">Apply Now <i class="icon-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection