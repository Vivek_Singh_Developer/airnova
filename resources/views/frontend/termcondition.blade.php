@extends('layouts.app')

@section('content')

    <div class="container top-banner body-height">
        <div class="row privacy-box">
            <div class="col-12 pt-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Terms and Conditions</li>
                    </ol>
                </nav>
            </div>

            <div class="col-12 text-center pb-2">
                <h1 class="py-2">Terms and Conditions</h1>
            </div>

            <div class="row">
                <div class="my-4">
                    <div class="col">
                        <h5 class="text-warning">OVERVIEW</h5>
                        <p> This Agreement governs your use of the Site and the products and services purchased or accessed through the Site (individually and collectively, the "Services"). Certain Services are governed by separate arrangements and strategies that are notwithstanding (not in lieu of) this Agreement. In the event of a conflict between the terms of a Services Agreement and the conditions, the applicable Services Agreement's requirements shall prevail. </p>
                        <p>The terms  <b style="font-size: 20px;">"P<sup>²</sup>C," "we," "us," or "our" </b>refer to a third party. The terms  <b style="font-size: 20px;"> "you," "your," "User," or "customer"</b> refer to any individual or entity that accepts this Agreement, gains access to your Account, or utilises the Services.</p>
                        <p>Nothing in this Agreement shall be construed as conferring any rights or benefits on a third party. </p>
                        <h5 class="text-warning">MODIFICATION OF AGREEMENT, SITE OR SERVICES</h5>
<p>We may change or modify this Agreement and any policies or agreements incorporated herein at any time in our sole and absolute discretion, and such changes or modifications shall take effect immediately upon posting to this Site. Your proceeds with utilization of this Site or the Services following such changes or alterations will comprise your acknowledgment of this Agreement in its latest correction. If you do not agree to be bound by this Agreement as revised, you must immediately cease using (or continuing to use) this Site or the Services. Additionally, we may notify you via email of changes or modifications to this Agreement on a periodic basis. As a result, it is critical that you maintain current information regarding your account <b style="font-size: 20px;">("P<sup>²</sup>C Account")</b>. We will not be liable or responsible if you do not receive an email warning because of a mistaken email address. Additionally, we may suspend or terminate your use of the Services if you violate or breach any of the terms of this Agreement. <b style="font-size: 20px;">(P<sup>²</sup>C)</b> reserves the right, at any time, to modify, change, or discontinue any aspect of this site or its services, including without limitation, prices and fees associated with the same. </p>

                        <h5 class="text-warning">ELIGIBILITY & AUTHORITY</h5>
          <p>This Site and the Services are available only to individuals or entities ("Users") who, under applicable law, are capable of entering into legally binding contracts.</p>
                        <p>By accessing or using this Site or the Services, you represent and warrant that you are at least the following: </p>
                        <ul>
                            <li>At least eighteen (18) years old.</li>
                            <li>Otherwise recognised by applicable law as having the capacity to enter into legally binding contracts.</li>
                            <li>Isn't it illegal for a person to purchase or receive the Services found on this website under the laws of the United States or another applicable jurisdiction?</li>

                        </ul>
 <p>Assume you are entering this Agreement on behalf of a busin</p>
 <p>In that case, you represent and warrant that you have the legal authority to bind such corporate entity to the terms and conditions of this Agreement, and the terms "you", "your", "User", or "client" will allude to such corporate substance. If we discover that you lack the legal authority to bind such corporate entity following your electronic acceptance of this Agreement, you will be personally liable for the obligations contained in this Agreement, including, yet not restricted to, the instalment commitments. </p>
 <p> Play Pause Continue Technology Private Limited shall not be liable for any loss or damage incurred as a result of <b style="font-size: 20px;">(P<sup>²</sup>C)'s </b> on any instruction, notice, document, or verse that we reasonably believe is true and originated from an authorised representative of your corporate entity.
    On the off chance that there is sensible uncertainty about the unwavering quality of any such guidance, notice, archive, or correspondence, we hold the right (however expect no commitment) to look for extra validation from you. </p>
 <p>Additionally, you agree to be bound by the terms of this Agreement for all transactions entered into by you, anyone acting on your behalf, and anyone who uses your records or the administrations, whether or not approved by you. </p>

 <h5 class="text-warning">YOUR ACCOUNT</h5>
 <p>To access certain features of this Site or to make use of certain Services, you must first create an <b style="font-size: 20px;">P<sup>²</sup>C account</b>.</p>
 <p>You represent and warrant to <b style="font-size: 20px;">(P<sup>²</sup>C)</b> that all information you submit when creating your Account is accurate, current, and complete, and that you will maintain the accuracy, currency, and completeness of your Account information. You are solely responsible for all activity on your Account, whether authorised or not, and you are responsible for maintaining the security of your Account information, which includes, however, isn't restricted to, your client number/login, password, Payment Method(s) (as characterized beneath), and customer PIN. You must immediately notify <b style="font-size: 20px;">(P<sup>²</sup>C)</b> of any breach of security or unauthorised use of your Account. <b style="font-size: 20px;">(P<sup>²</sup>C)</b> will not be liable for any loss you incur as a result of someone else using your Account without your permission. </p>

 <h5 class="text-warning">GENERAL RULES OF CONDUCT</h5>


<ul>
    <li>You agree that your use of this Site and the Services, including any content you contribute, will be in accordance with this Agreement, any applicable Services Agreement or policy that may apply to your administrations, and all appropriate neighbourhoods, state, public, and global laws, rules and guidelines.</li>
    <li>You won't gather or collect any User Content (as characterized underneath), just as any non-public or by and by recognizable data about another User or some other individual or substance. You won't permit any other individual to do as such except if they have given you formal authorization.</li>
    <li>You will not use this Site or the Services provided on this Site in a way that: is illegal, promotes or encourages illegal activity; promotes, encourages, or engages in illegal intimidation, brutality against individuals, creatures, or property; advances, energizes, or participates in psychological warfare, Spam or other spontaneous mass email, just as PC or organization hacking or breaking, are for the most part models. You will not take any action that places an unreasonable or abnormally big burden on our infrastructure, as determined by us;</li>
    <li>You will not, in any form, copy or distribute any part of this Site or Services. Except as expressly allowed by <b style="font-size: 20px;">(P<sup>²</sup>C)</b></li>
    <li>You will not alter or modify any component of this Site, its Services, or the technology that is linked to it. </li>
    <li> You will not use any technology or means other than this Site or as we may specify to access <b style="font-size: 20px;">(P<sup>²</sup>C)</b> Content or User Content. </li>
    <li>You agree to back up all of your User Content so that you can access and use it at any time.
        Play Pause Continue Technology Private Limited makes no promise that any Account or User Content will be backed up, and you agree to accept the risk of losing any User Content.
        You agree to back up all of your user content so that you can access and use it at any time.</li>
    <li>Unless you receive written agreement from the Partnership's customers, you will not resell or supply the Services for a commercial purpose, including any <b style="font-size: 20px;">(P<sup>²</sup>C)</b>  related technologies. </li>
    <li>You will not disable, circumvent, or otherwise tamper with this Site's or its Services' security features.
        You will not use these features to prevent or restrict the use or copying of any <b style="font-size: 20px;">(P<sup>²</sup>C)</b> Content, or to enforce site-wide policies.</li>
    <li>For access or usage of any computer resource, you will not host, display, upload, edit, publish, transmit, save, update, or distribute any content that breaches the Digital Media Ethics Code. <b style="font-size: 20px;">(P<sup>²</sup>C)'s</b> regulations, privacy policies, and user agreements.</li>
    <li><b style="font-size: 20px;">(P<sup>²</sup>C)</b> reserves the right to instantly terminate any computer resource's access or usage rights, as described in the Code.
        The user's rights to access or use a computer resource may be revoked if they do not comply with the Code or <b style="font-size: 20px;">(P<sup>²</sup>C)'s</b> regulations, privacy policies, and user agreements. </li>
    <li>You agree to submit government-issued picture identification as well as government-issued business identification where identity verification is required.</li>
    <li> <b style="font-size: 20px;">(P<sup>²</sup>C)</b>  explicitly reserves the right to refuse, cancel, terminate, suspend, or restrict future access to this Site or any Services to any User I whose Account or Services have been cancelled or suspended before.
        (ii) Anyone who has used the Site in an improper or illegal manner.</li>
    <li><b style="font-size: 20px;">(P<sup>²</sup>C)</b> maintains the right to terminate any service linked to your name, email address, or Account, as well as any related accounts.
        <b style="font-size: 20px;">(P<sup>²</sup>C)</b> may close your account if your purchase or account behaviour shows signs of fraud, abuse, or suspicious behaviour.
        You could be held accountable for any financial losses incurred by <b style="font-size: 20px;">(P<sup>²</sup>C)</b> , including legal fees and damages.
        <b style="font-size: 20px;">(P<sup>²</sup>C)</b> reserves the right to take any legal action it considers appropriate.
        If you disagree with the termination of Services, your account may be frozen or closed.</li>

</ul>
<h5 class="text-warning">USER CONTENT</h5>

  <p>(a) Users may be able to access, post, publish, distribute, save, or control "User Content" using some of the features of this Site or the Services, including those provided by <b style="font-size: 20px;">(P<sup>²</sup>C)</b> .
    Client Content contains (a) gathering posts, Content submitted related to a challenge, item audits or proposals, or pictures to be incorporated into a web-based media occasion or movement ("User Submissions"), or (b) photographs to be joined into a web-based media occasion or action
    (b) Any scholarly, imaginative, melodic, or other substance, including however not restricted to photos and recordings (along with User Submissions, "Client Content").
    Client Content alludes to all substance submitted through your record. You represent and warrant to <b style="font-size: 20px;">(P<sup>²</sup>C)</b>  that by submitting or publishing User Content to this Site or to or via the Services, you have the authority to do so.
    Users may be able to access, post, publish, distribute, save, or control "User Content" using some of the features of this Site or the Services, including those provided by <b style="font-size: 20px;">(P<sup>²</sup>C)</b> .
    You have all necessary rights to convey User Content by means of this Site or through the Services, either on the grounds that you are the writer of the User Content and reserve the option to disseminate it, or on the grounds that you have the proper appropriation privileges, licenses, assents, and authorizations to use, recorded as a hard copy, from the copyright or other owner of the client content.
    (ii) the User Content does not infringe on any third-party rights.
    You are completely responsible for any of your User Content or User Content submitted through your Account, as well as the repercussions and restrictions associated with its distribution.</p>


    <h5 class="text-warning">User Submissions</h5>

    <p>You recognize and concur that your User Submissions are totally intentional, don't make a classified relationship with <b style="font-size: 20px;">(P<sup>²</sup>C)</b>, and don't commit <b style="font-size: 20px;">(P<sup>²</sup>C)</b> to regard your User Submissions as private or secret, and that <b style="font-size: 20px;">(P<sup>²</sup>C)</b> has no express or inferred commitment to create or utilize your User Submissions... You or any other individual won't be made up for any expected or inadvertent utilization of your User Submissions. <b style="font-size: 20px;">(P<sup>²</sup>C)</b> may as of now be acquainted with such Content from different sources, may look to produce this (or comparative) Content all alone, or may have done/will make another move. Play Pause Continue Technology Private Limited will possess elite privileges to any User Submissions presented on this Site, including all protected innovation and other exclusive freedoms, and will be qualified for the unlimited use and dispersal of any User Submissions presented on this Site for any reason, business or in any case, without affirmation or pay to you or any other person.</p>

    <h5 class="text-warning">User Content Other Than User Submissions</h5>
    <p>By posting or distributing User Content to this Site or through the Services, you approve <b style="font-size: 20px;">(P<sup>²</sup>C)</b> to involve the licensed innovation and other restrictive freedoms in and to your User Content to empower incorporation and utilization of the User Content in the way considered by this Site and this Agreement. You as of now award <b style="font-size: 20px;">(P<sup>²</sup>C)</b> an around the world, non-select, sovereignty free, sub licensable (through numerous levels), and adaptable permit to utilize, replicate, appropriate, get ready subsidiary works of, consolidate with different works, show, and play out your User Content regarding this Site, the Services and (<b style="font-size: 20px;">(P<sup>²</sup>C)'s</b>(and (P<sup>²</sup>C)'s associates') business(s), including without impediment for advancing and rearranging all or part of this Site in any media designs and through any media channels without limitations of any sort and instalment or other thought of any sort, or authorization or warning, to you or any outsider. You additionally, as of now, award every User of this Site a non-selective permit to get to your User Content (aside from User Content that you assign "private" or "secret key secured") through this Site and to utilize, replicate, convey, get ready subordinate works of, consolidate with different works, show, and play out your User Content as allowed under this Agreement in accordance with the usefulness of this site. The above licenses conceded by you in your User Content end inside a financially sensible time after you eliminate or erase your User Content from this Site. Notwithstanding, you comprehend and concur that (P<sup>²</sup>C) may hold (yet not disseminate, show, or perform) server duplicates of your User Content that have been eliminated or erased. The above licenses conceded by you in your User Content are unending and permanent. In any case anything actually contained thus, (P<sup>²</sup>C) will not utilize any User Content that has been assigned "private" or "secret word ensured" by you to advance this Site or <b style="font-size: 20px;">(P<sup>²</sup>C)'s</b> (or (P<sup>²</sup>C)'s members') business(s). In the event that you have a site or other substance facilitated by <b style="font-size: 20px;">(P<sup>²</sup>C)</b>, you will hold all your proprietorship or authorized freedoms in User Content.</p>


    <h5 class="text-warning">Limitation of liability: </h5>
    <p>In no occasion will go to (P<sup>²</sup>C), its officials, chiefs, workers, specialists, and all outsider specialist organizations, be responsible to you or some other individual or element for any immediate, circuitous, accidental, exceptional, reformatory, or noteworthy harms at all, including any that might result from </p>
   <ul>
       <li>The exactness, culmination, or content of this site.</li>
       <li>The precision, fulfilment, or content of any locales connected (through hyperlinks, flag publicizing or in any case) to this site.</li>
       <li>The administrations found at this site or any destinations connected (through  hyperlinks, pennant promoting or in any case) to this site.</li>
       <li> individual injury or property harm of any nature at all,</li>
       <li>Outsider direct of any nature at all</li>
       <li>any unapproved admittance to or utilization of our servers as well as all substance, individual data, monetary data or other data and information put away in that, </li>
       <li>any interference or discontinuance of administrations to or from this site or any locales connected (through hyperlinks, pennant publicizing or in any case) to this site,</li>
       <li>any infections, worms, bugs, Trojan ponies, or something like that, which might be sent to or from this site or any locales connected (through hyperlinks, standard promoting or in any case) to this site,</li>
       <li>any client content or content that is disparaging, irritating, oppressive, destructive to minors or any secured class, explicit, "x-appraised", profane or in any case questionable, and additionally</li>
        <li>any misfortune or harm of any sort caused because of your utilization of this site or the administrations found at this site, regardless of whether dependent on guarantee, agreement, misdeed, or some other lawful or fair hypothesis, and whether or not <b style="font-size: 20px;">(P<sup>²</sup>C)</b> is informed with respect to the chance of such harms.</li>
   </ul>

    <p>Furthermore, you explicitly recognize and concur that in no occasion will <b style="font-size: 20px;">(P<sup>²</sup>C)'s</b> complete total obligation surpass $ 10,000.00 U.S. dollars.
        The previous restriction of obligation will apply to the furthest reaches allowed by law and will endure any end or lapse of this understanding or your utilization of this site or the administrations found at this site. </p>

        <h5 class="text-warning">Third-Party Entities</h5>
        <p>You are responsible for using non-<b style="font-size: 20px;">(P<sup>²</sup>C)</b> gaming servers and services.
            Some of our Services may allow you to play on servers that are not owned or controlled by <b style="font-size: 20px;">(P<sup>²</sup>C)</b>. We have no control over those services and are not liable for any of our Service you use on or via them. Additional or differing conditions and restrictions may apply to these third-party services.
            Links to third-party websites may be included in our Services. These websites may gather data or ask you for personal information. We have no control over those sites and is not responsible for their content, data collection, use, or disclosure.</p>
            <h5 class="text-warning">Informal Discussions</h5>
            <p>Before pursuing arbitration, you and <b style="font-size: 20px;">(P<sup>²</sup>C)</b> must attempt to resolve any Dispute informally for at least 30 days. When one party sends the other a written notice, the informal negotiations begin ("Notice of Dispute").</p>
       <ul>
           <li>The complaining party's full name and contact information must be included in the Notice of Dispute.</li>
           <li>Describe the claim or dispute's nature and basis.</li>
           <li>Specify the type of relief sought.
            </li>
       </ul>
            <p>Your billing or email address will be used to send <b style="font-size: 20px;">(P<sup>²</sup>C)</b>'s Notice of Dispute. (Dispute Resolutions by Binding Arbitration, According to Indian laws for using the website of software Development Company)
</p>








                    </div>
                </div>
            </div>

            {{-- @if($term)
                {!!$term->Value!!}
            @endif --}}
        </div>
    </div>
@endsection
