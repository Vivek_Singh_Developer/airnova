@extends('layouts.app')

@section('content')

<section class="page-title about-page-5 style-two p_relative centred">
    <div class="pattern-layer">
        <div class="shape-1 p_absolute l_120 t_120 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
        <div class="shape-2 p_absolute t_180 r_170 float-bob-y" style="background-image: url(assetsweb/images/shape/shape-56.png);"></div>
        <div class="shape-3 p_absolute l_0 b_0" style="background-image: url(assetsweb/images/shape/shape-189.png);"></div>
    </div>
    <div class="auto-container">
        <div class="content-box">
            <h1 class="d_block fs_60 lh_70 fw_bold mb_10">Our Values</h1>
            <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a href="{{route('/')}}">Home</a></li>
                <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Pages</li>
                <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">Our Values</li>
            </ul>
        </div>
    </div>
</section>

<section class="about-18 p_relative sec-pad">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div class="image_block_21">
                    <div data-animation-box="" class="image-box p_relative d_block pb_140 mr_30 animated">
                        <div class="shape">
                            <div class="shape-1 p_absolute b_80 rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                            <div class="shape-2 t_100 p_absolute rotate-me" style="background-image: url(assetsweb/images/shape/shape-176.png);"></div>
                        </div>
                        <figure data-animation-text="" class="overlay-anim-black-bg image image-1 p_relative d_block  animated overlay-animation" data-animation="overlay-animation" style="transform: translateY(39px); transition: transform 0.6s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform;"><img src="assetsweb/images/resource/about-13.jpg" alt=""></figure>
                        <figure data-animation-text="" class="overlay-anim-black-bg image image-2 p_absolute r_0 b_0  animated overlay-animation" data-animation="overlay-animation" style="transform: translateY(-54px); transition: transform 0.6s cubic-bezier(0, 0, 0, 1) 0s; will-change: transform;"><img src="assetsweb/images/resource/about-14.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div class="content_block_11">
                    <div class="content-box p_relative d_block ml_30">
                        <div class="sec-title-13 p_relative d_block mb_25">
                            <h5 class="p_relative d_iblock fs_17 lh_30 fw_sbold mb_11 uppercase">About Us</h5><br>
                            <h2 class="d_block fs_40 fw_bold lh_50">We Conceive and Translate  Dreams into Reality.</h2>
                        </div>
                        <div class="text p_relative d_block mb_25">
                            <p class="font_family_poppins">Lorem ipsum dolor sit amet consectur adipiscing elit sed eiusmod ex tempor incididunt labore dolore magna aliquaenim ad minim veniam quis nostrud exercitation laboris.</p>
                        </div>
                        <ul class="list clearfix">
                            <li class="p_relative d_block fs_16 fw_bold font_family_oxygen pull-left mb_18 pl_40">Clients Focused</li>
                            <li class="p_relative d_block fs_16 fw_bold font_family_oxygen pull-left mb_18 pl_40">Oil Change</li>
                            <li class="p_relative d_block fs_16 fw_bold font_family_oxygen pull-left mb_18 pl_40">We Can Save You Money.</li>
                            <li class="p_relative d_block fs_16 fw_bold font_family_oxygen pull-left mb_18 pl_40">Engine Cooling System</li>
                            <li class="p_relative d_block fs_16 fw_bold font_family_oxygen pull-left pl_40">Sertified Repair</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="feature-13 p_relative pb_150 centred">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                <div class="feature-block-eight wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block pt_50 pr_30 pb_45 pl_30">
                        <div class="icon-box p_relative d_iblock mb_25">
                            <div class="icon p_relative d_iblock fs_50 tran_5 z_1"><i class="icon-113"></i></div>
                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-108.png')}}" alt=""></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_18">Company Vision</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit elit sectur sed eiusm.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                <div class="feature-block-eight wow fadeInUp animated animated" data-wow-delay="200ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 200ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block pt_50 pr_30 pb_45 pl_30">
                        <div class="icon-box p_relative d_iblock mb_25">
                            <div class="icon p_relative d_iblock fs_50 tran_5 z_1"><i class="icon-117"></i></div>
                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-109.png')}}" alt=""></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_18">Strategy Monitoring</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit elit sectur sed eiusm.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                <div class="feature-block-eight wow fadeInUp animated animated" data-wow-delay="400ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 400ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block pt_50 pr_30 pb_45 pl_30">
                        <div class="icon-box p_relative d_iblock mb_25">
                            <div class="icon p_relative d_iblock fs_50 tran_5 z_1"><i class="icon-119"></i></div>
                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-110.png')}}" alt=""></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_18">Advanced Reporting</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit elit sectur sed eiusm.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                <div class="feature-block-eight wow fadeInUp animated animated" data-wow-delay="600ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                    <div class="inner-box p_relative d_block pt_50 pr_30 pb_45 pl_30">
                        <div class="icon-box p_relative d_iblock mb_25">
                            <div class="icon p_relative d_iblock fs_50 tran_5 z_1"><i class="icon-69"></i></div>
                            <div class="icon-img hidden-icon"><img src="{{asset('assetsweb/images/icons/hid-icon-111.png')}}" alt=""></div>
                        </div>
                        <div class="text">
                            <h4 class="d_block fs_20 lh_30 fw_sbold mb_18">User Experience</h4>
                            <p class="font_family_poppins">Lorem ipsum dolor sit elit sectur sed eiusm.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection