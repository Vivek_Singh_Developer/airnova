@extends('layouts.app')

@section('content')
    <!-- Page Title -->
    <style>
         .sec-pad {
            padding: 15px 0px;
        }
        .shapeabc {
            background-image: url('../assetsweb/images/shape/shape-176.png');
        }

        .shapeabcd {
            background-image: url('../assetsweb/images/shape/shape-56.png');
        }

        .shapeabcde {
            background-image: url('../assetsweb/images/shape/shape-189.png');
        }
    </style>
    <section class="page-title about-page-5 style-two p_relative centred">
        <div class="pattern-layer">
            <div class="shapeabc shape-1 p_absolute l_120 t_120 rotate-me"></div>
            <div class="shapeabcd shape-2 p_absolute t_180 r_170 float-bob-y"></div>
            <div class="shapeabcde shape-3 p_absolute l_0 b_0">
            </div>
        </div>
        <div class="auto-container">
            <div class="content-box">
                <h1 class="d_block fs_60 lh_70 fw_bold mb_10">{{ $catdata->cat_name }}</h1>
                <ul class="bread-crumb p_relative d_block mb_8 clearfix">
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20"><a
                            href="{{ url('/') }}">Home</a></li>
                    <li class="p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte mr_20">Products</li>
                    <li class="current p_relative d_iblock fs_16 lh_25 fw_sbold font_family_inte">{{ $catdata->cat_name }}
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Page Title -->


    <!-- shop-page-section -->
    <section class="shop-page-section p_relative sec-pad">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 cols-sm-12 content-side">
                    <div class="our-shop">
                        <div class="item-shorting p_relative d_block mb_50 clearfix">
                            <div class="left-column pull-left clearfix">
                                <div class="btn-box float_left p_relative clearfix mr_30">
                                    <button class="grid-view on p_relative d_iblock fs_20 b_radius_5 mr_2 centred"><i
                                            class="icon-177"></i></button>
                                    <button class="list-view p_relative d_iblock fs_20 b_radius_5 centred"><i
                                            class="icon-178"></i></button>
                                </div>
                                <div class="text float_left">
                                    <p class="fs_16 font_family_poppins">Showing <span
                                            class="color_black">{{ $productdata->lastPage() }} –
                                            {{ $productdata->perPage() }}</span> of
                                        <span class="color_black"> {{ $productdata->total() }}</span> Results
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper grid">
                            <div class="shop-grid-content">
                                <div class="row clearfix">
                                    @forelse ($productdata as $item)
                                        <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                                            <div class="shop-block-one">
                                                <div class="inner-box p_relative d_block tran_5 mb_30">
                                                    <div class="image-box p_relative d_block">

                                                        <figure class="image p_relative d_block"><img
                                                                src="{{ url('storage/product/' . $item->thumbnail_image) }}"
                                                                alt=""></figure>
                                                        {{-- <ul class="option-list clearfix">
                                                            <li><a href="{{ url('storage/product/' . $item->thumbnail_image) }}"
                                                                    class="lightbox-image"><i class="icon-156"></i></a></li>
                                                            <li><a href="{{route('product_by_id',$catdata->id)}}"><i class="icon-155"></i></a>
                                                            </li>
                                                            <li><a href="{{route('product_by_id',$catdata->id)}}"><i class="icon-154"></i></a></li>
                                                            <li><a href="{{route('product_by_id',$catdata->id)}}"><i class="icon-140"></i></a>
                                                            </li>
                                                        </ul> --}}
                                                    </div>
                                                    <div class="lower-content p_relative d_block pt_3 pr_20 pb_20 pl_20">
                                                        <h6 class="d_block fs_15 lh_20 mb_4"><a href="{{route('productDetail',$item->id)}}"
                                                                class="d_iblock color_black">{{ $item->product_name }}</a>
                                                        </h6>
                                                        <span
                                                            class="price p_relative d_block fs_15 fw_medium font_family_inter color_black mb_2"> &#8377; {{ $item->product_price }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                    @endforelse

                                </div>
                            </div>
                            <div class="shop-list-content">
                                <div class="row clearfix">
                                    @forelse ($productdata as $item)
                                        <div class="col-lg-4 col-md-6 col-sm-12 shop-block">
                                            <div class="shop-block-one">
                                                <div class="inner-box p_relative d_block tran_5 mb_30">
                                                    <div class="image-box p_relative d_block">
                                                        <figure class="image p_relative d_block"><img
                                                                src="{{ url('storage/product/' . $item->thumbnail_image) }}"
                                                                alt=""></figure>
                                                        {{-- <ul class="option-list clearfix">
                                                            <li><a href="{{ url('storage/product/' . $item->thumbnail_image) }}"
                                                                    class="lightbox-image"><i class="icon-156"></i></a></li>
                                                            <li><a href="{{route('product_by_id',$catdata->id)}}"><i class="icon-155"></i></a>
                                                            </li>
                                                            <li><a href="shop.html"><i class="icon-154"></i></a></li>
                                                            <li><a href="{{route('product_by_id',$catdata->id)}}"><i class="icon-140"></i></a>
                                                            </li>
                                                        </ul> --}}
                                                    </div>

                                                    <div class="lower-content p_relative d_block pt_3 pr_20 pb_20 pl_20">
                                                        <h6 class="d_block fs_15 lh_20 mb_4"><a href="{{route('productDetail',$item->id)}}"
                                                                class="d_iblock color_black">{{ $item->product_name }}</a>
                                                        </h6>
                                                        <span
                                                            class="price p_relative d_block fs_15 fw_medium font_family_inter color_black mb_2"> &#8377;  {{ $item->product_price }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        </div>

                        <div class="pagination-wrapper centred mt_40">
                            @if ($productdata->hasPages())
                                <ul class="pagination clearfix">
                                    @if ($productdata->onFirstPage())
                                        <li>
                                            <a href="javascript:;"><i class="icon-4"></i></a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ $productdata->previousPageUrl() }}"><i class="icon-4"></i></a>
                                        </li>
                                    @endif
                                    @for ($i = max($productdata->currentPage() - 2, 1); $i <= min(max($productdata->currentPage() - 2, 1) + 4, $productdata->lastPage()); $i++)
                                        <li>
                                            <a class="{{ $productdata->currentPage() == $i ? ' current' : '' }}"
                                                href="{{ $productdata->url($i) }}">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    @if ($productdata->hasMorePages())
                                        <li>
                                            <a href="{{ $productdata->nextPageUrl() }}" rel="next"><i
                                                    class="icon-4"></i>
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="javascript:;"><i class="icon-4"></i></a>
                                        </li>
                                    @endif
                                </ul>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- shop-page-section end -->
@endsection
