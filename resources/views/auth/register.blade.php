@extends('layouts.app')

@section('content')
    <style>
        .modal-content1 {
            border: 2px solid #f39f01 !important;
        }

        .main-form-header {
            display: flex;
            justify-content: space-between;
        }

        #signUpForm1 .custom-input input,
        #signUpForm1 .custom-dropdown-input .custom-dropdown-btn {
            border-left: 0;
            border-top: 0;
            border-right: 0;
            padding-left: 0;
            padding-right: 0;
            font-size: 18px;
            color: #f39f01;
            border-bottom: 1px solid #dfe1ea;
            box-shadow: 0 1px 0 0 #dfe1ea;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            box-sizing: border-box;
            margin: 0px;
        }

        #signUpForm1 input[type=text],
        #signUpForm1 input[type=password],
        #signUpForm1 input[type=email],
        #signUpForm1 input[type=url],
        #signUpForm1 input[type=time],
        #signUpForm1 input[type=date],
        #signUpForm1 input[type=datetime-local],
        #signUpForm1 input[type=tel],
        #signUpForm1 input[type=number],
        #signUpForm1 input[type=search-md],
        #signUpForm1 input[type=search],
        #signUpForm1 textarea.md-textarea {
            background-color: transparent;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0;
            outline: none;
            width: 100%;
            font-size: 0.9rem;
            box-shadow: none;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            transition: all .3s;
        }

        #signUpForm1 #signinSubmitBtn {
            margin-top: 20px !important;
        }

        #signUpForm1 .button {
            min-width: 174px;
            height: 48px;
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 8px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            cursor: pointer;
            transition: all .15s ease-in-out;
        }


        .invalid-feedback {
            display: none;
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #error-mobile,
        #error-email,
        #sent-otp {
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #signUpForm1 .form-control.is-invalid,
        #signUpForm1 .was-validated .form-control:invalid {
            border-color: #f39f01;
        }

        #signUpForm1 .get-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right:25px;
            top: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #signUpForm1 .verify-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            margin-bottom: 47px;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right: 10px;
            bottom: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #signUpForm1 .custom-input.otp-visible .get-otp-button {
            display: block;
        }

        #signUpForm1 .form-control:focus {
            color: #f39f01;
            background-color: transparent;
            border-bottom: 1px solid #ccc;
            outline: 0;
            box-shadow: none;
        }

        #signUpForm1 .custom-input.otp-visible {
            position: relative;
        }

        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }

        #otp-field {
            display: none;
        }

        #signUpForm1 .custom-input.otp-visible .verify-otp-button {
            display: block;
        }

        .input-group-btn {
            position: absolute;
            top: 15px;
            right: 30px;
            color: #333;
        }

        .dark-gray {
            color: #999;
        }

    </style>
    <div class="container mt-5 top-banner body-height">

        <div class="row  justify-content-center row-cols-1 row-cols-sm-6">
            <div class="col-sm-6 bg-dark p-3 pt-3 modal-content1 ">
                <div class="main-form-header px-5 pt-4">
                    <h4 class="header-title text-light">Sign Up</h4>
                    <span class="header-subtitle text-light">Already have an account? <a href="{{ route('login') }}">Sign
                            In</a></span>
                </div>
                <div class="tab-content px-5 pb-3 pt-4" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form method="POST" action="{{ route('register') }}" autocomplete="off" id="signUpForm1">
                            @csrf
                            <div class="mb-3 custom-input">
                                <input type="text" class="form-control login-input @if ($errors->has('name')) is-invalid @endif" name="name"
                                    value="{{ old('name') }}" placeholder="Your Name" required autocomplete="off">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="mb-3 custom-input">
                                <input type="email" class="form-control login-input @if ($errors->has('email')) is-invalid @endif" name="email"
                                    value="{{ old('email') }}" id="signUpForm1Email" placeholder="Email Address" required autocomplete="off">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <div id="error-email"></div>
                            </div>
                            <div class="mb-3 custom-input" id="otp-box">
                                <input type="number" pattern="[6789][0-9]{9}" required
                                    class="form-control login-input @if ($errors->has('mobile')) is-invalid @endif" name="mobile"
                                    value="{{ old('mobile') }}" id="signUpForm1MobileNumber" maxLength="10"
                                    placeholder="Mobile Number" autocomplete="off"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">

                                
                                <div id="error-mobile"></div>
                                <div id="getOtpButton" class="get-otp-button">Get OTP</div>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="custom-input" id="otp-field">
                                <input type="number" maxLength="4" class="form-control helper-input" name="otp"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                    id="confirmotp" placeholder="Enter Your OTP" autocomplete="off">
                                <div id="getVerifyButton" class="verify-otp-button">Verify OTP</div>
                                <div id="sent-otp"></div>
                                <div class="col-auto ml-auto" id="signUpFormResendOtp">
                                    <a id="resend-otp" href="javascript:void(0)" class="semibold">Resend OTP</a>
                                </div>

                            </div>
                            <div class="mb-3 custom-input position-relative">
                                <input type="password" required class="form-control login-input @if ($errors->has('password'))  @endif"
                                    name="password" id="register_password" value="{{ old('password') }}"
                                    placeholder="Password" autocomplete="off">
                                <span class="input-group-btn text-light" id="eyeSlash">
                                    <i class="dark-gray  fa fa-eye-slash" onclick="visibility4()" aria-hidden="true"></i>
                                </span>
                                <span class="input-group-btn text-light" id="eyeShow" style="display: none;">
                                    <i onclick="visibility4()" class="dark-gray  fa fa-eye" aria-hidden="true"></i>
                                </span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <button type="submit" disabled="true" id="signinSubmitBtn"
                                class="button button-warning mx-auto d-block my-4 formSubmitBtn">Sign Up</button>
                            <p class="mute resetFeedback" id="errorFeedback"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script>

        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                event.preventDefault();
                return false;
                }
            });
        });
        function visibility4() {
            var x = document.getElementById('register_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#eyeShow').show();
                $('#eyeSlash').hide();
            } else {
                x.type = "password";
                $('#eyeShow').hide();
                $('#eyeSlash').show();
            }
        }

        $('#signUpForm1Email').on('keyup', function() {
            var email = $("#signUpForm1Email").val();
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (email.match(mailformat)) {
                $.ajax({
                    url: "{{ route('validateemail') }}",
                    type: 'post',
                    data: {
                        _token: "{{ csrf_token() }}",
                        email: email,
                    },
                    success: (res) => {

                        if (res.status == 1) {
                            $("#error-email").html(`<strong>${res.result}</strong>`);
                        } else {
                            $("#error-email").html(`<strong>${res.result}</strong>`);
                            // setTimeout(function() {
                            //     $('#error-email').hide();
                            // }, 5000);
                            // $("#error-email").css('display', 'none');
                        }
                    },
                    error: (err) => {
                        console.log(err);
                    },
                });
            } else {

                // if (email != null) {
                $("#error-email").html(`<strong>Enter Valid Email</strong>`);
                //     setTimeout(function() {
                //         $('#error-email').hide();
                //     }, 5000);
                // }
            }
        });
        $('#signUpForm1MobileNumber').on('keyup', function() {
            var mobileNumber = $("#signUpForm1MobileNumber").val();

            if (mobileNumber.length == 10) {
                $("#otp-box").addClass('otp-visible');
                $("#error-mobile").html(`<strong>Please enter on get otp button.</strong>`);
            } else {
                $("#otp-box").removeClass('otp-visible');
                $("#error-mobile").html(``);
            }
        });
 
        $('#confirmotp').on('keyup', function() {
            var otp = $("#confirmotp").val();

            if (otp.length == 4) {
                $("#otp-field").addClass('otp-visible');
                $("#sent-otp").html(`<strong>Please enter on verify otp button.</strong>`);
            } else {
                $("#otp-field").removeClass('otp-visible');

            }
        });

        function sendotp(mobile) {
            $.ajax({
                url: "{{ route('validatemobile') }}",
                type: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    mobile: mobile,
                },
                success: (res) => {

                    if (res.status == 1) {
                        $("#error-mobile").html(`<strong>${res.result}</strong>`);
                    } else {
                        $("#otp-box").removeClass('otp-visible');
                        $("#otp-field").css('display', 'block');
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                        // setTimeout(function() {
                        //     $('#sent-otp').hide();
                        // }, 8000);
                        setTimeout(function() {
                            $('#error-mobile').hide();
                        }, 5000);
                    }
                },
                error: (err) => {
                    console.log(err);
                },
            });
        }

        $("#getOtpButton, #signUpFormResendOtp").click(function() {
            var mobileNumber = $("#signUpForm1MobileNumber").val();
            sendotp(mobileNumber);
        });
        $("#getVerifyButton").click(function() {
            var mobileNumber = $("#signUpForm1MobileNumber").val();
            var otp = $("#confirmotp").val();
            verifyotp(mobileNumber, otp);
        });


        function verifyotp(mobile, otp) {
            $.ajax({
                url: "{{ route('verifyotp') }}",
                type: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    mobile: mobile,
                    otp: otp,
                },
                success: (res) => {

                    if (res.status == 1) {
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    } else {

                        $("#getVerifyButton").css('display', 'none');
                        $("#signUpFormResendOtp").css('display', 'none');
                        $("#otp-field").removeClass('otp-visible');
                        $("#otp-field").css('display', 'block');
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").css('color', 'green');
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                        $(":submit").removeAttr("disabled");
                        $("#signUpFormResendOtp").css('display', 'none');
                        setTimeout(function() {
                            $('#sent-otp').hide();
                        }, 8000);
                        setTimeout(function() {
                            $('#error-mobile').hide();
                        }, 5000);
                    }
                },
                error: (err) => {
                    console.log(err);
                },
            });
        }
    </script>
@endpush
