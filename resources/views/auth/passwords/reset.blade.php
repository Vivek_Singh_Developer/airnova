@extends('layouts.app')

@section('content')
    <style>
        .modal-content1 {
            border: 2px solid #f39f01 !important;
        }

        .main-form-header {
            text-align: center;
        }

        #signUpForm1 .custom-input input,
        #signUpForm1 .custom-dropdown-input .custom-dropdown-btn {
            border-left: 0;
            border-top: 0;
            border-right: 0;
            padding-left: 0;
            padding-right: 0;
            font-size: 18px;
            color: #f39f01;
            border-bottom: 1px solid #dfe1ea;
            box-shadow: 0 1px 0 0 #dfe1ea;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            box-sizing: border-box;
            margin: 0px;
        }

        #signUpForm1 input[type=text],
        #signUpForm1 input[type=password],
        #signUpForm1 input[type=email],
        #signUpForm1 input[type=url],
        #signUpForm1 input[type=time],
        #signUpForm1 input[type=date],
        #signUpForm1 input[type=datetime-local],
        #signUpForm1 input[type=tel],
        #signUpForm1 input[type=number],
        #signUpForm1 input[type=search-md],
        #signUpForm1 input[type=search],
        #signUpForm1 textarea.md-textarea {
            background-color: transparent;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0;
            outline: none;
            width: 100%;
            font-size: 0.9rem;
            box-shadow: none;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            transition: all .3s;
        }

        #signUpForm1 #signinSubmitBtn {
            margin-top: 20px !important;
        }

        #signUpForm1 .button {
            min-width: 174px;
            height: 48px;
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 18px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            cursor: pointer;
            transition: all .15s ease-in-out;
        }


        .invalid-feedback {
            display: none;
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #error-mobile,
        #error-email,
        #sent-otp,
        #confirm-password {
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #signUpForm1 .form-control.is-invalid,
        #signUpForm1 .was-validated .form-control:invalid {
            border-color: #f39f01;
        }

        #signUpForm1 .get-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right: 10px;
            bottom: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #signUpForm1 .verify-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            margin-bottom: 47px;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right: 10px;
            bottom: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #signUpForm1 .custom-input.otp-visible .get-otp-button {
            display: block;
        }

        #signUpForm1 .form-control:focus {
            color: #f39f01;
            background-color: transparent;
            border-bottom: 1px solid #ccc;
            outline: 0;
            box-shadow: none;
        }

        #signUpForm1 .custom-input.otp-visible {
            position: relative;
        }

        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
  /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
        #otp-field {
            display: none;
        }

        #signUpForm1 .custom-input.otp-visible .verify-otp-button {
            display: block;
        }

    </style>
    <div class="container mt-5 top-banner body-height">

        <div class="row  justify-content-center row-cols-1 row-cols-sm-6">
            <div class="col-sm-6 bg-dark p-3 pt-3 modal-content1 ">
                <div class="main-form-header pt-4">
                    <h4 style="font-size:19px;" class="header-title text-light  ">Reset Password</h4>

                </div>

                <div class="tab-content px-5 pb-3 pt-4" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form method="POST" action="{{ route('password.update') }}" autocomplete="off" id="signUpForm1">
                            @csrf

                            <input type="hidden" class="form-control login-input @if ($errors->has('mobile')) is-invalid @endif" name="mobile"
                                id="signUpForm1MobileNumber" maxLength="10" value="{{ $mobile }}" autocomplete="off"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                placeholder="Mobile Number">


                            <div class="mb-3 custom-input">
                                <input type="number" class="form-control login-input @if ($errors->has('otp')) is-invalid @endif" name="otp"
                                    maxLength="4" id="loginotp" autocomplete="off"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                    placeholder="Enter OTP">

                                @if ($errors->has('otp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('otp') }}</strong>
                                    </span>
                                @endif
                                <div id="sent-otp"></div>
                            </div>

                            <div class="mb-3 custom-input">
                                <input type="password" required class="form-control login-input @if ($errors->has('password')) is-invalid @endif"
                                autocomplete="off" name="password" placeholder="New Password" id="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="mb-3 custom-input">
                                <input type="password" autocomplete="off" required class="form-control login-input @if ($errors->has('confirm-password')) is-invalid @endif"
                                    name="confirm-password" placeholder="Confirm password" id="confirmpassword">
                                @if ($errors->has('confirm-password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('confirm-password') }}</strong>
                                    </span>
                                @endif
                                <div id="confirm-password"></div>
                            </div>

                            <button type="submit" disabled="true" id="signinSubmitBtn"
                                class="button button-warning mx-auto d-block my-4 formSubmitBtn">Reset Password</button>
                            <p class="mute resetFeedback" id="errorFeedback"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script>
        $('#confirmpassword').on('keyup', function() {
            var password = $("#password").val();
            var confirmpassword = $("#confirmpassword").val();
            if (confirmpassword.length == password.length && password == confirmpassword) {
                $("#confirm-password").css('display', 'none');
            } else {
                $("#confirm-password").css('display', 'block');
                $("#confirm-password").html(`<strong>Password does not match.</strong>`);
            }
        });

        $('#loginotp').on('keyup', function() {
            var mobile = $("#signUpForm1MobileNumber").val();
            var otp = $("#loginotp").val();
            // alert(otp);
            if (otp.length == 4) {
                $("#sent-otp").css('display', 'none');
                verifyotp(mobile, otp);
            } else {
                $("#sent-otp").html(`<strong>Please enter a 4-digit otp .</strong>`);
            }
        });

        function verifyotp(mobile, otp) {
            $.ajax({
                url: "{{ route('verifyotp') }}",
                type: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    mobile: mobile,
                    otp: otp,
                },
                success: (res) => {

                    if (res.status == 1) {
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    } else {
                        $("#signinSubmitBtn").removeAttr('disabled');
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    }
                },
                error: (err) => {
                    console.log(err);
                },
            });
        }
    </script>
@endpush
