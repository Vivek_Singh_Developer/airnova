@extends('layouts.app')

@section('content')
    <style>
        .modal-content1 {
            border: 2px solid #f39f01 !important;
        }

        .main-form-header {
            display: flex;
            justify-content: space-between;
        }

        #loginform .custom-input input,
        #loginform .custom-dropdown-input .custom-dropdown-btn {
            border-left: 0;
            border-top: 0;
            border-right: 0;
            padding-left: 0;
            padding-right: 0;
            font-size: 18px;
            color: #f39f01;
            border-bottom: 1px solid #dfe1ea;
            box-shadow: 0 1px 0 0 #dfe1ea;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            box-sizing: border-box;
            margin: 0px;
        }

        #loginform input[type=text],
        #loginform input[type=password],
        #loginform input[type=email],
        #loginform input[type=url],
        #loginform input[type=time],
        #loginform input[type=date],
        #loginform input[type=datetime-local],
        #loginform input[type=tel],
        #loginform input[type=number],
        #loginform input[type=search-md],
        #loginform input[type=search],
        #loginform textarea.md-textarea {
            background-color: transparent;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0;
            outline: none;
            width: 100%;
            font-size: 0.9rem;
            box-shadow: none;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            transition: all .3s;
        }

        #loginform #signinSubmitBtn {
            margin-top: 20px !important;
        }

        #loginform .button {
            min-width: 174px;
            height: 48px;
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 8px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            cursor: pointer;
            transition: all .15s ease-in-out;
        }


        .invalid-feedback {
            display: none;
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #error-mobile,
        #error-user,
        #sent-otp {
            width: 100%;
            margin-top: 0.25rem;
            font-size: .875em;
            color: #f39f01;
        }

        #loginform .form-control.is-invalid,
        #loginform .was-validated .form-control:invalid {
            border-color: #f39f01;
        }

        #loginform .get-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right: 10px;
            bottom: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #loginform .verify-otp-button {
            width: 100px;
            height: 30px;
            text-align: center;
            line-height: 27px;
            box-shadow: 0 4px 8px 0 rgb(246 103 68 / 50%);
            background-color: #f39f01;
            border: 2px solid #f39f01;
            margin-bottom: 47px;
            border-radius: 22px;
            color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            right: 10px;
            bottom: 5px;
            position: absolute;
            cursor: pointer;
            display: none;
        }

        #loginform .custom-input.otp-visible .get-otp-button {
            display: block;
        }

        #loginform #loginwithotp {
            color: #f39f01;
        }

        #loginform .form-control:focus {
            color: #f39f01;
            background-color: transparent;
            border-bottom: 1px solid #ccc;
            outline: 0;
            box-shadow: none;
        }

        #loginform .custom-input.otp-visible {
            position: relative;
        }

        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        #otp-field {
            display: none;
        }

        #loginform .custom-input.otp-visible .verify-otp-button {
            display: block;
        }

        .input-group-btn {
            position: absolute;
            top: 15px;
            right: 30px;
            color: #333;
        }

        .dark-gray {
            color: #999;
        }

    </style>
    <div class="container mt-5 top-banner body-height" >
        <div class="row  justify-content-center row-cols-1 row-cols-sm-6">
            <div class="col-sm-6 bg-dark p-3 pt-3 modal-content1 ">
                <div class="main-form-header px-5 pt-4">
                    <h4 class="header-title text-light">Sign In</h4>
                    <span class="header-subtitle text-light">Don't have an account? <a href="{{ route('register') }}">Sign
                            Up</a></span>
                </div>
                <div class="tab-content px-5 pb-3 pt-4" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form method="POST" action="{{ route('login') }}" autocomplete="off" id="loginform">
                            @csrf
                            <input type="hidden" id="logintype" name="logintype" value="password">
                            <div class="mb-3 custom-input">
                                <input type="text" name="user" value="{{ old('user') }}" id="loginuser"
                                    class="form-control login-input @if ($errors->has('user')) is-invalid @endif"
                                    placeholder="Email Address / Mobile Number" required>

                                @if ($errors->has('user'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                @endif
                                <div id="error-user"></div>
                            </div>
                            <div class="mb-3 custom-input position-relative" id="password-box">
                                <input type="password" name="password"
                                    class="form-control login-input @if ($errors->has('password')) is-invalid @endif" id="login_password"
                                    placeholder="Password">
                                <span class="input-group-btn text-light" id="eyeSlash">
                                    <i class="dark-gray  fa fa-eye-slash" onclick="visibility3()" aria-hidden="true"></i>
                                </span>
                                <span class="input-group-btn text-light" id="eyeShow" style="display: none;">
                                    <i onclick="visibility3()" class="dark-gray  fa fa-eye" aria-hidden="true"></i>
                                </span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="mb-3 custom-input" id="otp-field">
                                <input type="text" name="otp"
                                    class="form-control login-input @error('otp') is-invalid @enderror" id="loginotp"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                    maxlength="4" placeholder="Enter OTP">
                                @error('otp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div id="getOtpButton" class="get-otp-button">Get OTP</div>
                                <div id="sent-otp"></div>
                                <div class="col-auto ml-auto" id="signUpFormResendOtp">
                                    <a id="resend-otp" href="javascript:void(0)" class="semibold">Resend OTP</a>
                                </div>
                            </div>

                            <div class="main-form-header mb-3">
                                <span class="header-title text-light">
                                    <div class="custom-checkbox-input">
                                        {{-- <input class="custom-checkbox" name="tos" type="checkbox" value="remember_user"
                                            id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-checkbox-label" for="rememberMeCheckbox"></label>
                                        <label class="custom-checkbox-text" for="rememberMeCheckbox"> Remember me</label> --}}
                                    </div>
                                </span>
                                @if (Route::has('password.request'))
                                    <span class="header-subtitle text-light"><a
                                            href="{{ route('password.request') }}">Forgot password?</a></span>
                                @endif

                            </div>
                            <button type="submit" id="signinSubmitBtn"
                                class="button button-warning mx-auto d-block my-4 formSubmitBtn">Sign In</button>
                            <div class="mb-3 text-center">
                                <label class="form-label" id="loginwithotp">
                                    <a href="javascript:;">Login with OTP</a></label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>
        function visibility3() {
            var x = document.getElementById('login_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#eyeShow').show();
                $('#eyeSlash').hide();
            } else {
                x.type = "password";
                $('#eyeShow').hide();
                $('#eyeSlash').show();
            }
        }

        $("#loginwithotp,#signUpFormResendOtp").click(function() {
            var user = $("#loginuser").val();
            var mobileformate = /^(0|[+91]{3})?[7-9][0-9]{9}$/;
            if (user.length == 10 && user.match(mobileformate)) {
                sendotp(user);
            } else {
                $("#error-user").html(`Enter Mobile Number`);
            }
        });


        function sendotp(mobile) {
            $.ajax({
                url: "{{ route('sendotptomobile') }}",
                type: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    mobile: mobile,
                },
                success: (res) => {

                    if (res.status == 1) {
                        $("#error-user").html(`<strong>${res.result}</strong>`);
                    } else {
                        $("#password-box").css('display', 'none');
                        $("#otp-field").css('display', 'block');
                        $('#logintype').val("otp");
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    }
                },
                error: (err) => {
                    console.log(err);
                },
            });
        }


        $('#loginotp').on('keyup', function() {
            var mobile = $("#loginuser").val();
            var otp = $("#loginotp").val();
            // alert(otp);
            if (otp.length == 4) {
                $("#sent-otp").css('display', 'none');
                verifyotp(mobile, otp);
            } else {
                $("#sent-otp").html(`<strong>Please enter a 4-digit otp .</strong>`);
            }
        });

        function verifyotp(mobile, otp) {
            $.ajax({
                url: "{{ route('verifyotp') }}",
                type: 'post',
                data: {
                    _token: "{{ csrf_token() }}",
                    mobile: mobile,
                    otp: otp,
                },
                success: (res) => {

                    if (res.status == 1) {
                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    } else {
                        $("#sent-otp").css('display', 'block');
                        $("#sent-otp").css('color', 'green');
                        $("#signUpFormResendOtp").css('display', 'none');

                        $("#sent-otp").html(`<strong>${res.result}</strong>`);
                    }
                },
                error: (err) => {
                    console.log(err);
                },
            });
        }

        $('#loginuser').on('keyup', function() {
            var user = $("#loginuser").val();
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var mobileformate = /^[6-9][0-9]{9}$/;
            if (user.match(mailformat) || (user.length == 10 && user.match(mobileformate))) {
                $.ajax({
                    url: "{{ route('validateuser') }}",
                    type: 'post',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user: user,
                    },
                    success: (res) => {

                        if (res.status == 1) {
                            $("#error-user").html(`<strong>${res.result}</strong>`);
                        } else {
                            $("#error-user").html(`<strong>${res.result}</strong>`);
                        }
                    },
                    error: (err) => {
                        console.log(err);
                    },
                });
            } else {
                $("#error-user").html(`<strong>Please enter a valid Email ID or Phone no.</strong>`);
            }
        });
    </script>
@endpush
