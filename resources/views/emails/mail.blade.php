
@if($data['subject']) 
    <h1>{{ $data['subject'] }}</h1>  
@endif

@if($data['message']) 
    <p>{{ $data['message'] }}</p>  
@endif