<?php

use App\Mail\TestMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/fcm/{message}/{token}', function ($message, $token)
// {
//    dd(fcm($token,'user', 'Play Pause Continue', $message, []));
// });

Route::get('/welcome', function () {
    return view('welcome');
}); 
Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('/');
// Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('index');
Route::get('/contact-us', [App\Http\Controllers\IndexController::class, 'contactUs'])->name('contactus');
Route::get('/career', [App\Http\Controllers\IndexController::class, 'career'])->name('career');
Route::get('/about-us', [App\Http\Controllers\IndexController::class, 'aboutUs'])->name('aboutus');
Route::get('/privacy-policy', [App\Http\Controllers\IndexController::class, 'privacypolicy'])->name('privacypolicy');
Route::get('/term-condition', [App\Http\Controllers\IndexController::class, 'termcondition'])->name('termcondition');
Route::get('/services', [App\Http\Controllers\IndexController::class, 'services'])->name('services');
Route::get('/services/{id}', [App\Http\Controllers\IndexController::class, 'serviceById'])->name('service_by_id');
Route::get('/who-we-are', [App\Http\Controllers\IndexController::class, 'who_we_are'])->name('who_we_are');
Route::get('/our-values', [App\Http\Controllers\IndexController::class, 'our_values'])->name('our_values');
Route::get('/our-history', [App\Http\Controllers\IndexController::class, 'our_history'])->name('our_history');
Route::get('/board-of-director', [App\Http\Controllers\IndexController::class, 'board_of_director'])->name('board_of_director');
Route::get('/management-team', [App\Http\Controllers\IndexController::class, 'management_team'])->name('management_team');
Route::get('/products', [App\Http\Controllers\IndexController::class, 'products'])->name('products');
Route::get('/products/{cat_id}', [App\Http\Controllers\IndexController::class, 'productByCategory'])->name('product_by_id');
Route::get('/products/detail/{product_id}', [App\Http\Controllers\IndexController::class, 'productDetail'])->name('productDetail');

Auth::routes();

Route::post('/feedback/store', [App\Http\Controllers\IndexController::class, 'feedback'])->name('feedback.store');
Route::post('/newsletter/store', [App\Http\Controllers\IndexController::class, 'newsletter'])->name('newsletter.store');
Route::post('/validatemobile', [App\Http\Controllers\IndexController::class, 'validatemobile'])->name('validatemobile');
Route::post('/validateemail', [App\Http\Controllers\IndexController::class, 'validateemail'])->name('validateemail');
Route::post('/validateuser', [App\Http\Controllers\IndexController::class, 'validateuser'])->name('validateuser');
Route::post('/sendotptomobile', [App\Http\Controllers\IndexController::class, 'sendotptomobile'])->name('sendotptomobile');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/mobile', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.mobile');

Route::post('/verifyotp', [App\Http\Controllers\IndexController::class, 'verifyotp'])->name('verifyotp');
Route::get('/resend-otp/{mobile}', [App\Http\Controllers\IndexController::class, 'resendotp'])->name('resendotp');

Route::middleware(['auth:customer'])->group(function()
{
    Route::get('/test', [App\Http\Controllers\IndexController::class, 'test']);
    Route::get('/home', [App\Http\Controllers\IndexController::class, 'index'])->name('home');
    Route::post('/save-token', [App\Http\Controllers\IndexController::class, 'saveToken'])->name('save-token');


    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
   
});

