<?php

use Illuminate\Support\Facades\Route;

/////////////// Admin Routes ////////////////
Route::name('admin.')->namespace('Auth')->middleware('guest:admin')->group(function()
{
    Route::get('/', function() {
        return redirect(route('admin.login'));
    }); 

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login.submit');

});

Route::name('admin.')->middleware(['auth:admin'])
->group(function() {

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::post('/newsletter/store', 'HomeController@newsletterstore')->name('newsletter.store');

    Route::group(['prefix' => 'setting'], function() {

        Route::get('/', 'SettingController@index')->name('setting');
        Route::post('/appname', 'SettingController@updateappname')->name('setting.appname');
        Route::post('/logo', 'SettingController@updatelogo')->name('setting.logo');
        Route::post('/copyright', 'SettingController@updatecopyright')->name('setting.copyright');
        Route::post('/email', 'SettingController@updateadminemail')->name('setting.email');
        Route::post('/password', 'SettingController@updateadminpassword')->name('setting.password');

        Route::post('/app_address', 'SettingController@updatecompanyaddress')->name('setting.app_address');
        Route::post('/app_email', 'SettingController@updatecompanyemail')->name('setting.app_email');
        Route::post('/app_mobile', 'SettingController@updatecompanymobile')->name('setting.app_mobile');
   
    });

    Route::resource('socials', SocialController::class);
    Route::resource('products', ProductController::class);
    Route::resource('procategory', ProductCategoryController::class);
    Route::resource('services', ServiceController::class);
    
    Route::get('/product/image/{id}', 'ProductController@produtimagedelete')->name('product.image.delete');
    Route::post('/products/status/{id}', 'ProductController@updatestatus')->name('product.status');
    Route::post('/services/status/{id}', 'ServiceController@updatestatus')->name('services.status');

    Route::get('/feedback', 'FeedBackController@index')->name('feedback.index');
    Route::delete('/feedback/delete/{id}', 'FeedBackController@destroy')->name('feedback.delete');
    Route::get('/feedback/{id}/show', 'FeedBackController@feedbackshow')->name('feedback.show');
    Route::post('/feedback/reply', 'FeedBackController@feedbackreply')->name('feedback.reply');


    Route::get('/content', 'ContentController@index')->name('content.index');
    Route::get('/content/{id}', 'ContentController@edit')->name('content.edit');
    Route::put('/content/{id}', 'ContentController@update')->name('content.update');
    Route::get('/content/image/{id}', 'ContentController@deleteImage')->name('content.image.delete');
    Route::get('/content/video/{id}', 'ContentController@deletevideo')->name('content.video.delete');


    Route::group(['prefix' => 'web-user'], function() {

        Route::get('/', 'WebUserController@index')->name('webuser.index');
        Route::get('/get', 'WebUserController@get')->name('webuser.get');

        Route::get('/add', 'WebUserController@addUser')->name('webuser.add');
        Route::post('/store', 'WebUserController@storeUser')->name('webuser.store');
        Route::get('/edit/{id?}', 'WebUserController@editUser')->name('webuser.edit');
        Route::get('/show/{id?}', 'WebUserController@showUser')->name('webuser.show');
        Route::post('/update', 'WebUserController@updateUser')->name('webuser.update');
        Route::get('/delete/{id?}', 'WebUserController@userdelete')->name('webuser.delete');

        Route::post('/bulk-action', 'WebUserController@bulkAction')->name('webuser.bulkAction');
        Route::post('export-excel', 'WebUserController@excel_export')->name('webuser.export.excel');
        Route::post('/status/{id}', 'WebUserController@updatestatus')->name('webuser.status');
    });

});
