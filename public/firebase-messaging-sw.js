/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
 apiKey: "AIzaSyBdSHT_XCBS8HAlwOgKUlPetQhrKQ9vyGY",

    authDomain: "playpause-dbe96.firebaseapp.com",

    projectId: "playpause-dbe96",

    storageBucket: "playpause-dbe96.appspot.com",

    messagingSenderId: "85183737581",

    appId: "1:85183737581:web:a8a2087c0273a82f4666e4",

    measurementId: "G-5RJEQB78KY"


});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log(
    "[firebase-messaging-sw.js] Received background message ",
    payload,
  );
  /* Customize notification here */
  const notificationTitle = "Background Message Title";
  const notificationOptions = {
    body: "Background Message body.",
    icon: "/itwonders-web-logo.png",
  };

  return self.registration.showNotification(
    notificationTitle,
    notificationOptions,
  );
});
