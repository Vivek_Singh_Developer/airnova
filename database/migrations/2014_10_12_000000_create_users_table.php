<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('my_refer_code')->unique();
            $table->enum('user_type',['User','Shopkeeper'])->default('User');
            $table->string('name');
            $table->string('email')->nullable()->unique();
            $table->string('mobile')->nullable()->unique();
            $table->string('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('GST_number')->nullable();
            $table->string('refferal_code')->nullable();
            $table->string('profile_image')->nullable();
            $table->integer('user_level')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('block')->default(1);
            $table->string('send_password');
          
            $table->string('fcm_token')->nullable();
            $table->enum('lang',['hin','en'])->default('en');
            $table->enum('type',['ios','app'])->default('app');
            $table->rememberToken();
            $table->timestamps();


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
