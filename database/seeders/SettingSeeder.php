<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key'    =>  'name',
                'value'  =>  env('APP_NAME'),
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'logo',
                'value'  =>  null,
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'copyright',
                'value'  =>  'Copyright © 2021 ,'.env('APP_NAME').'. All Right Reserved.',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'app_address',
                'value'  =>  'Noida',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'app_email',
                'value'  =>  'technogigz@gmail.com',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'app_mobile',
                'value'  =>  '76435435',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
       
        ]);
    }
}
