<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('socials')->insert([
            [
                'icon_name'     => 'Facebook',
                'link'          => 'https://www.facebook.com/',
                'fa_class'      => 'fa fa-facebook',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'icon_name'     => 'Instagram',
                'link'          => 'https://www.instagram.com/',
                'fa_class'      => 'fa fa-instagram',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'icon_name'     => 'Linkdin',
                'link'          => 'https://www.linkedin.com',
                'fa_class'      => 'fa fa-linkedin',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'icon_name'     => 'Twitter',
                'link'          => 'https://twitter.com/',
                'fa_class'      => 'fa fa-twitter',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'icon_name'     => 'Pinterest',
                'link'          => 'https://in.pinterest.com/',
                'fa_class'      => 'fa fa-pinterest-p',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ]
        ]);
    }
}
