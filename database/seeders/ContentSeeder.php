<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [
                'key'    =>  'how_it_works',
                'value'  =>  'Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.',
                'type'  =>  'Website',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'about_us',
                'value'  =>  'Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.',
                'type'  =>  'Website',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            
            [
                'key'    =>  'privacy_english',
                'value'  =>  'Privacy & Security',
                'type'  =>  'Mobile',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            [
                'key'    =>  'term_condition_english',
                'value'  =>  'Term & Condition',
                'type'  =>  'Mobile',
                'created_at'    =>  now(),
                'updated_at'    =>  now(),
            ],
            

        ]);
    }
}
